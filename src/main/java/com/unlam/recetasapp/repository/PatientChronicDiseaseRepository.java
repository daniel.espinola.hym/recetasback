package com.unlam.recetasapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.PatientChronicDisease;

public interface PatientChronicDiseaseRepository extends JpaRepository<PatientChronicDisease, Long>{
	
	List<PatientChronicDisease> findByPatientId(Long patientId);
	List<PatientChronicDisease> findByDoctorIdAndHospitalId(Long doctorId, Long hospitalId);
}

package com.unlam.recetasapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.Locality;

public interface LocalityRepository extends JpaRepository<Locality, Long>{

}

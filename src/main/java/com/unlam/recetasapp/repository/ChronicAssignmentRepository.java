package com.unlam.recetasapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.unlam.recetasapp.entity.ChronicAssignment;

public interface ChronicAssignmentRepository extends JpaRepository<ChronicAssignment, Long> {
	
	@Query("select ca "
			+ "from ChronicAssignment ca "
			+ "inner join PatientChronicDisease pcd on pcd.id = ca.patientChronicDiseaseId "
			+ "where pcd.patient.id = ?1")
	public List<ChronicAssignment> findChronicAssignmentByPatientId(Long patientId);
	
	@Query("select ca "
			+ "from ChronicAssignment ca "
			+ "where ca.quantity > ca.triggeredQuantity and ca.frequency='m'")
	public List<ChronicAssignment> findAllChronicAssignmentToWork();
	
	public List<ChronicAssignment> findChronicAssignmentByPatientChronicDiseaseIdId(Long patientChronicDiseaseId);
}

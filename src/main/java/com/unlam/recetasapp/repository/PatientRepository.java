package com.unlam.recetasapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.Patient;

public interface PatientRepository  extends JpaRepository<Patient, Long>{
	
	Patient findByDni(Integer dni);
	
}

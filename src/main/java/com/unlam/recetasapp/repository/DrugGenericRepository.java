package com.unlam.recetasapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.unlam.recetasapp.entity.DrugGeneric;
import com.unlam.recetasapp.entity.Strength;
import com.unlam.recetasapp.model.PharmaceuticalFormModel;
import com.unlam.recetasapp.model.StrengthModel;

public interface DrugGenericRepository extends JpaRepository<DrugGeneric, Long>{
	public List<DrugGeneric> findAllByOrderByNameAsc();
	
	@Query("select new com.unlam.recetasapp.model.StrengthModel(s.id, s.number, u.description) from DrugGeneric dg inner join Strength s on dg.strength = s.id inner join Unity u ON u.id = s.unityId where dg.name = ?1")
	public List<StrengthModel> findStrengthByName(String name);

	@Query("select new com.unlam.recetasapp.model.PharmaceuticalFormModel(p.id, p.description) from DrugGeneric dg inner join DrugComercialCompose dcc on dg.id = dcc.drugGeneric inner join DrugComercial dc on dc.id = dcc.drugComercial inner join com.unlam.recetasapp.entity.PharmaceuticalForm p on p.id = dc.pharmaceuticalForm where dg.name = ?1")
	public List<PharmaceuticalFormModel> findPharmaceuticalFormByName(String name);

	public List<DrugGeneric> findByName(String name);
	
	public DrugGeneric findByNameAndStrength(String name, Strength strength);
}	


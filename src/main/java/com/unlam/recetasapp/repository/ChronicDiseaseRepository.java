package com.unlam.recetasapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.ChronicDisease;

public interface ChronicDiseaseRepository extends JpaRepository<ChronicDisease, Long> {
	
	public ChronicDisease findByDiagnosisId(Long diagnosisId);
}

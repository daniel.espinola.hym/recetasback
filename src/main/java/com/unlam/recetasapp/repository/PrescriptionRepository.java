package com.unlam.recetasapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.unlam.recetasapp.entity.Prescription;

public interface PrescriptionRepository  extends JpaRepository<Prescription, Long>{

	
	@Query("select p from Prescription p "
			+ "inner join MedicalConsultation mc on mc.id = p.medicalConsultation "
			+ "where (p.prescriptionStatus.id=1 or p.prescriptionStatus.id=7) and mc.doctorId = ?1")
	List<Prescription> findPrescriptionPendingByDoctorId(Long doctorId);
	
	@Query("select p.id from Prescription p "
			+ "where p.chronicAssignment.id = ?1")
	Long findPrescriptionIdChronicAssignmentId(Long chronicAssignmentId);
}

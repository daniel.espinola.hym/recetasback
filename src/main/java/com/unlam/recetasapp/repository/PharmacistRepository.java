package com.unlam.recetasapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.unlam.recetasapp.entity.Pharmacist;

public interface PharmacistRepository extends JpaRepository<Pharmacist, Long>{

	public Pharmacist findByDni(Integer dni);
	
}

package com.unlam.recetasapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.unlam.recetasapp.entity.MedicalConsultation;

public interface MedicalConsultationRepository extends JpaRepository<MedicalConsultation, Long> {

	List<MedicalConsultation> findByPatientIdOrderByConsultationDateDesc(Long patientId);

	List<MedicalConsultation> findByPatientId(Long patientId);

	List<MedicalConsultation> findAllByDoctorId(Long doctorId);
	
	List<MedicalConsultation> findByHospitalId(Long HospitalId);

	List<MedicalConsultation> findByPatientIdAndHospitalId(Long patientId, Long hospitalId);
	
	@Query("select distinct mc from MedicalConsultation mc\n" + 
			"inner join Prescription p on p.medicalConsultation = mc.id\n" + 
			"inner join PrescriptionItem pi on pi.prescription = p.id\n" + 
			"inner join PrescriptionItemDiagnosis pid on pid.prescriptionItem = pi.id\n" + 
			"inner join ChronicDisease cd on cd.diagnosis = pid.diagnosis\n" + 
			"where mc.patientId = ?1 and mc.hospitalId = ?2 order by mc.consultationDate desc")
	List<MedicalConsultation> findAllMCChronicByPatientAndHospital(Long patientId, Long hospitalId);
}

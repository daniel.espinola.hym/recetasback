package com.unlam.recetasapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.Doctor;
import com.unlam.recetasapp.entity.DoctorHospital;

public interface DoctorHospitalRepository extends JpaRepository<DoctorHospital, Long>{
	
	public List<DoctorHospital> findByHospitalId(Long hospitalId);
}

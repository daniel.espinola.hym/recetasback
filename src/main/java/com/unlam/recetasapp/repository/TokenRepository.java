package com.unlam.recetasapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unlam.recetasapp.entity.SecurityToken;
import com.unlam.recetasapp.model.TokenStatus;

@Repository
public interface TokenRepository extends JpaRepository<SecurityToken, Long> {
	
	SecurityToken findByDniAndOtpCode(Integer dni, Integer otpcode);

	SecurityToken findByDniAndTokenStatus(Integer dni, TokenStatus status);
	
}

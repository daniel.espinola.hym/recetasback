package com.unlam.recetasapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.Unity;

public interface UnityRepository extends JpaRepository<Unity, Long>{

}

package com.unlam.recetasapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.unlam.recetasapp.entity.Doctor;
import com.unlam.recetasapp.entity.Patient;
import com.unlam.recetasapp.entity.Pharmacist;
import com.unlam.recetasapp.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {
	User findByUsername(String username);
	
	@Query("SELECT p FROM User u INNER JOIN Patient p ON u.patient = p.id WHERE u.username=:dni")
	Patient findPatientByDni(@Param("dni") String dni);
	
	@Query("SELECT d FROM User u INNER JOIN Doctor d ON u.doctor = d.id WHERE u.username=:dni")
	Doctor findDoctorByDni(@Param("dni") String dni);
	
	@Query("SELECT p FROM User u INNER JOIN Pharmacist p ON u.pharmacist = p.id WHERE u.username=:dni")
	Pharmacist findPharmacistByDni(@Param("dni") String dni);
	
}

package com.unlam.recetasapp.repository;

import java.util.Date;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.Hospital;
import com.unlam.recetasapp.entity.WaitingRoom;

public interface WaitingRoomRepository extends JpaRepository<WaitingRoom, Long> {
		WaitingRoom findByDniAndHospitalAndEntranceDateBetween(Long dni, Hospital hospital, Date dateFrom, Date dateTo);
}

package com.unlam.recetasapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.DrugComercial;
import com.unlam.recetasapp.entity.PharmaceuticalForm;

public interface DrugComercialRepository extends JpaRepository<DrugComercial, Long>{
	
	public List<DrugComercial> findAllByOrderByNameAsc();
	
	public List<PharmaceuticalForm> findPharmaceuticalFormByIdIn(List<Long> drugComercialIds);
	
}

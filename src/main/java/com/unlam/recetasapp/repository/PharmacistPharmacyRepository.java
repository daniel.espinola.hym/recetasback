package com.unlam.recetasapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import com.unlam.recetasapp.entity.PharmacistPharmacy;
import com.unlam.recetasapp.entity.Pharmacist;

public interface PharmacistPharmacyRepository extends JpaRepository<PharmacistPharmacy, Pharmacist>{

	public PharmacistPharmacy findByPharmacist(Pharmacist pharmacist);
	public List<PharmacistPharmacy> findByPharmacyId(Long pharmacyId);
	
}

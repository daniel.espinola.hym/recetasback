package com.unlam.recetasapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.Hospital;

public interface HospitalRepository extends JpaRepository<Hospital, Long> {

}
package com.unlam.recetasapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.unlam.recetasapp.entity.Province;

@Repository
public interface ProvinceRepository  extends JpaRepository<Province, Long>{
	

}

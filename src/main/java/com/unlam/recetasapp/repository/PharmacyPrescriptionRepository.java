package com.unlam.recetasapp.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.MedicalConsultation;
import com.unlam.recetasapp.entity.PharmacyPrescription;

public interface PharmacyPrescriptionRepository extends JpaRepository<PharmacyPrescription, Long>{
	
	List<PharmacyPrescription> findByPharmacyIdOrderByInsertdateDesc(Long pharmacyId);
	
	List<PharmacyPrescription> findAllByPharmacistId(Long pharmacistId);
	
	List<PharmacyPrescription> findByPharmacyId(Long pharmacyId);
}

package com.unlam.recetasapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.Diagnosis;

public interface DiagnosisRepository extends JpaRepository<Diagnosis, Long>{
	
}

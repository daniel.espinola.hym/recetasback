package com.unlam.recetasapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.PharmaceuticalForm;

public interface PharmaceuticalFormRepository extends JpaRepository<PharmaceuticalForm, Long> {
	public PharmaceuticalForm findByDescription(String description);
}

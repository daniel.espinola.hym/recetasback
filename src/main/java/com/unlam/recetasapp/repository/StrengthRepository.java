package com.unlam.recetasapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.Strength;

public interface StrengthRepository extends JpaRepository<Strength, Long>{

}

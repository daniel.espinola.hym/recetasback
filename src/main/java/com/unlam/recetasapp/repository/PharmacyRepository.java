package com.unlam.recetasapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.Pharmacy;

public interface PharmacyRepository extends JpaRepository<Pharmacy, Long>{

}

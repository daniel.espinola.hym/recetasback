package com.unlam.recetasapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.Doctor;

public interface DoctorRepository extends JpaRepository<Doctor, Long>{

	public Doctor findByDni(Integer dni);
	
}

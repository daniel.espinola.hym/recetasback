package com.unlam.recetasapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unlam.recetasapp.entity.DrugComercialCompose;

public interface DrugComercialComposeRepository extends JpaRepository<DrugComercialCompose, Long>{

}	


package com.unlam.recetasapp.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unlam.recetasapp.entity.Patient;
import com.unlam.recetasapp.model.PatientModel;
import com.unlam.recetasapp.model.WaitingRoomPostPackage;
import com.unlam.recetasapp.security.jwt.JwtUserDetails;
import com.unlam.recetasapp.service.MedicalConsultationService;
import com.unlam.recetasapp.service.PatientService;
import com.unlam.recetasapp.util.ResponseWrapper;

@RestController
@RequestMapping(value = "/patient")
public class PatientController {
	
	Logger logger = LoggerFactory.getLogger(PatientController.class);

	@Autowired
	PatientService patientService;

	@Autowired
	MedicalConsultationService medicalConsultationService;

	@CrossOrigin
	@Secured({"ROLE_DOCTOR"})
	@RequestMapping(value = "/{dni}", method = RequestMethod.GET)
	public ResponseWrapper getPatientByDNI(@PathVariable(required = true) Integer dni, @RequestParam(required = true) Integer idHospital) {
		
		ResponseWrapper r = new ResponseWrapper();
		Patient patient = patientService.getPatientByDni(dni);
		
		if(patient == null) {
			r.setError("El paciente ingresado no está dado de alta en el sistema");
			return r;
		}
		
		PatientModel patientModel = new PatientModel(patient);
		
		if(patientService.patientIsInWaitingRoom(dni.longValue(), idHospital.longValue())) {
			r.setResponse(patientModel);			
		}else {
			r.setError("El paciente " + patientModel.getFullName() + " no fue dado de alta en la sala de espera");
		}
		
		return r;

	}
	

	@CrossOrigin
	@Secured({"ROLE_PATIENT"})
	@RequestMapping(value = "/{dni}/prescription/preview", method = RequestMethod.GET)
	public ResponseWrapper getPrecriptionPreviewByDNI(@PathVariable(required = true) Integer dni) {

		Patient patient = patientService.getPatientByDni(dni);
		JwtUserDetails userDetails = (JwtUserDetails) currentUserDetails();

		if(!userDetails.getClaims().get("dni").equals(dni)) {
			System.out.println("El usuario no tiene permisos para ver las recetas del dni " + dni);
		}
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(medicalConsultationService.getPrescriptionPreviewPackageByPatient(patient.getId()));
		return r;

	}
	
	public UserDetails currentUserDetails(){
	    SecurityContext securityContext = SecurityContextHolder.getContext();
	    Authentication authentication = securityContext.getAuthentication();
	    if (authentication != null) {
	        Object principal = authentication.getPrincipal();
	        return principal instanceof UserDetails ? (UserDetails) principal : null;
	    }
	    return null;
	}
	

	@CrossOrigin
	@Secured({"ROLE_PATIENT"})
	@RequestMapping(value = "/{dni}/prescription/{idPrescription}", method = RequestMethod.GET)
	public ResponseWrapper getPrecriptionDetailByID(@PathVariable(required = true) Integer dni,
			@PathVariable(required = true) Long idPrescription) {

		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(medicalConsultationService.getPrescriptionDetailById(idPrescription));
		return r;

	}
	
	@CrossOrigin
	@Secured({"ROLE_HOSPITAL_ADMIN"})
	@RequestMapping(value = "/waitingRoom", method = RequestMethod.POST)
	public ResponseWrapper addPatientToWaitingRoom( @RequestBody WaitingRoomPostPackage waitingRoomPackage) {

		ResponseWrapper r = new ResponseWrapper();
		try {
			r.setResponse(patientService.addPatientToWaitingRoom(waitingRoomPackage));
		} catch (Exception e) {
			logger.error(e.getMessage());
			r.setError(e.getMessage());
		}
		return r;
	}
	
	@CrossOrigin
	@RequestMapping(value = "/medicalConsultation/{idMedicalConsultation}", method = RequestMethod.GET)
	public ResponseWrapper getPrecriptionDetailByMedicalConsultationID(@PathVariable(required = true) Long idMedicalConsultation) {

		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(medicalConsultationService.getPrescriptionDetailByMedicalConsultationId(idMedicalConsultation));
		return r;

	}


}

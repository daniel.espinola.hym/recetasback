package com.unlam.recetasapp.controller;

import java.util.Base64;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.unlam.recetasapp.model.RequestFilePrescription;
import com.unlam.recetasapp.service.MedicalConsultationService;

@RestController
@RequestMapping(value = "/prescription")
public class PrescriptionController {

	@Autowired
	private MedicalConsultationService medicalConsultationService;

	@CrossOrigin
	@GetMapping("/downloads/{prescriptionId}")
	public ResponseEntity<byte[]> downloadFile(@PathVariable Long prescriptionId, HttpServletRequest request, HttpServletResponse response) {
		String prescriptionFile = medicalConsultationService.getPrescription(prescriptionId).getFile();

		String contentType = "application/octet-stream";

		String prescriptionName = "ReMeDi-Receta-" + prescriptionId + ".pdf";
		
	    response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");
	    
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(contentType))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + prescriptionName + "")
				.body(Base64.getDecoder().decode(prescriptionFile));
	}

}

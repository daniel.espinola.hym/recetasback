package com.unlam.recetasapp.controller;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unlam.recetasapp.entity.Pharmacist;
import com.unlam.recetasapp.entity.PharmacistPharmacy;
import com.unlam.recetasapp.entity.PharmacyPrescription;
import com.unlam.recetasapp.entity.Prescription;
import com.unlam.recetasapp.repository.PharmacyPrescriptionRepository;
import com.unlam.recetasapp.service.PharmacistPharmacyService;
import com.unlam.recetasapp.service.PharmacistService;
import com.unlam.recetasapp.service.PrescriptionService;
import com.unlam.recetasapp.util.ResponseWrapper;


@RestController
@RequestMapping(value = "/token")
//@CrossOrigin(origins={ "http://192.168.1.105:3000", "http://localhost:4200" })
public class TokenController {
	
	Logger logger = LoggerFactory.getLogger(TokenController.class);
	
	@Autowired
	private PrescriptionService prescriptionService;
	
	@Autowired
	private PharmacistService pharmacistService;
	
	@Autowired
	private PharmacistPharmacyService pharmacistPharmacyService;
	
	@Autowired
	private PharmacyPrescriptionRepository pharmacyPrescriptionRepository;

	//Utilizado por el paciente para generar y obtener un token
	@CrossOrigin
	@Secured({"ROLE_PATIENT"})
	@RequestMapping(value="/{dni}/{prescriptionId}", method = RequestMethod.GET)
	public ResponseWrapper getSecurityToken(@PathVariable(required = true) Integer dni, @PathVariable(required = true) Long prescriptionId) {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(prescriptionService.generateReadToken(dni, prescriptionId));
		return r;
	}
	
	//Utilizado por el farmacéutico, quien envía el DNI del paciente y el token para obtener la receta
	@CrossOrigin(origins = {"*"})
	@Secured({"ROLE_PHARMACIST"})
	@RequestMapping(value="/{dni}", method = RequestMethod.GET)
	public ResponseWrapper getPrescriptions(@PathVariable(required = true) Integer dni, @RequestParam(required = true) Integer otpCode) {
		ResponseWrapper r = new ResponseWrapper();
		try {
			r.setResponse(prescriptionService.getPrescriptionsByDniAndCode(dni, otpCode));			
		}catch(Exception e) {
			logger.error(e.getMessage());
			r.setError(e.getMessage());
		}
		return r;
	}
	
	//Secuencia de pasos para la confirmación del uso de la receta
	
	//El farmacéutico oprime el botón de confirmar, y la receta pasa al estado "PRESENTADA"
	@CrossOrigin
	@Secured({"ROLE_PHARMACIST"})
	@RequestMapping(value="/confirmation-flow/step-one/{dni}/{prescriptionId}", method = RequestMethod.GET)
	public ResponseWrapper prescriptionConfirmationFlow_Step1(@PathVariable(required = true) Integer dni, @PathVariable(required = true) Integer prescriptionId) {
		ResponseWrapper r = new ResponseWrapper();
		try {
			//Asignamos el estado "PRESENTADA" a la receta
			prescriptionService.updatePrescriptionStatus(prescriptionId, 3);
			r.setResponse(true);
		}catch(Exception e) {
			logger.error(e.getMessage());
			r.setError(e.getMessage());
		}
		return r;
	}
	
	//En paralelo, desde la aplicación de paciente se empieza a pollear para detectar el momento en que la receta
	//pasa a estar en el estado "PRESENTADA"
	@CrossOrigin
	@Secured({"ROLE_PATIENT"})
	@RequestMapping(value="/{dni}/patient/confirmation-request/{prescriptionId}", method = RequestMethod.GET)
	public ResponseWrapper prescriptionConfirmationFlow_Step2(@PathVariable(required = true) Integer dni, @PathVariable(required = true) Integer prescriptionId) {
		ResponseWrapper r = new ResponseWrapper();
		try {
			//r.setResponse(prescriptionService.getPrescriptionsByDniAndCode(dni, otpCode));
			Long requestedPrescriptionStatus = prescriptionService.getById((long)prescriptionId).getPrescriptionStatus().getId();
			if(requestedPrescriptionStatus == 3) {
				r.setResponse(true);
			} else {
				r.setResponse(false);
			}
		}catch(Exception e) {
			logger.error(e.getMessage());
			r.setError(e.getMessage());
		}
		return r;
	}
	

	//Cuando desde la aplicación de paciente detectamos el cambio de estado, podemos confirmar el uso de la receta o rechazar y hacer rollback.
	//Este método es para el caso en donde el paciente confirma, de manera que el estado pasa a ser "UTILIZADA"
	@CrossOrigin
	@Secured({"ROLE_PATIENT"})
	@RequestMapping(value="/{dni}/patient/confirmation-response/{prescriptionId}", method = RequestMethod.GET)
	public ResponseWrapper prescriptionConfirmationFlow_Step3a(@PathVariable(required = true) Integer dni, @PathVariable(required = true) Integer prescriptionId) {
		ResponseWrapper r = new ResponseWrapper();
		try {
			//Cambiamos la receta al estado "UTILIZADA"
			prescriptionService.updatePrescriptionStatus(prescriptionId, 4);
			r.setResponse(true);
		}catch(Exception e) {
			logger.error(e.getMessage());
			r.setError(e.getMessage());
		}
		return r;
	}
	
	//Este método es para el caso en donde el paciente rechaza, de manera que el estado vuelve a ser "FIRMADA/DISPONIBLE"
	@CrossOrigin
	@Secured({"ROLE_PATIENT"})
	@RequestMapping(value="/{dni}/patient/status-reset/{prescriptionId}", method = RequestMethod.GET)
	public ResponseWrapper prescriptionConfirmationFlow_Step3b(@PathVariable(required = true) Integer dni, @PathVariable(required = true) Integer prescriptionId) {
		ResponseWrapper r = new ResponseWrapper();
		try {
			//Cambiamos la receta al estado "FIRMADA/DISPONILE"
			prescriptionService.updatePrescriptionStatus(prescriptionId, 2);
			r.setResponse(true);
		}catch(Exception e) {
			logger.error(e.getMessage());
			r.setError(e.getMessage());
		}
		return r;
	}
	
	
	//En paralelo, desde la aplicación de farmacia se empieza a pollear para detectar el momento en que la receta
	//pasa a estar en el estado "UTILIZADA", ya que ahí habilitamos el botón de descargar PDF
	@CrossOrigin
	@Secured({"ROLE_PHARMACIST"})
	@RequestMapping(value="/confirmation-flow/step-four/{prescriptionId}", method = RequestMethod.GET)
	public ResponseWrapper prescriptionConfirmationFlow_Step4(@PathVariable(required = true) Integer prescriptionId) {
		ResponseWrapper r = new ResponseWrapper();
		try {
			Long requestedPrescriptionStatus = prescriptionService.getById((long)prescriptionId).getPrescriptionStatus().getId();
			if(requestedPrescriptionStatus == 4) {
				r.setResponse(true);
			} else {
				r.setResponse(false);
			}
		}catch(Exception e) {
			logger.error(e.getMessage());
			r.setError(e.getMessage());
		}
		return r;
	}
	
	//El farmacéutico oprime el botón de confirmar, y la receta pasa al estado "PRESENTADA"
	@CrossOrigin
	@Secured({"ROLE_PHARMACIST"})
	@RequestMapping(value="/confirmation-flow/step-five/{dni}/{prescriptionId}", method = RequestMethod.GET)
	public ResponseWrapper prescriptionConfirmationFlow_Step5(@PathVariable(required = true) Integer dni, @PathVariable(required = true) Integer prescriptionId) {
		ResponseWrapper r = new ResponseWrapper();
		try {
			
			//Buscamos el objeto farmacéutico a partir del DNI
			Pharmacist pharmacist = pharmacistService.getPharmacist(dni);
			//Buscamos la relación entre el farmacéutico y la farmacia en donde trabaja
			PharmacistPharmacy pharmacistPharmacy = pharmacistPharmacyService.getPharmacistPharmacy(pharmacist);
			//Buscamos la receta presentada a partir del prescriptionId
			Prescription prescription = prescriptionService.getById((long)prescriptionId);
			
			//Generamos una nueva relación Farmacia/Prescription y la guardamos
			//TODO: meter estas acciones en un servicio
			PharmacyPrescription pp = new PharmacyPrescription();
			pp.setPharmacist(pharmacist);
			pp.setPharmacy(pharmacistPharmacy.getPharmacy());
			pp.setPrescription(prescription);
			pp.setInsertdate(new Date());
			pharmacyPrescriptionRepository.save(pp);
			
			r.setResponse(true);
		}catch(Exception e) {
			logger.error(e.getMessage());
			r.setError(e.getMessage());
		}
		return r;
	}
	
}

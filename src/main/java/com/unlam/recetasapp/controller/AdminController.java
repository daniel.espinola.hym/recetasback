package com.unlam.recetasapp.controller;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.unlam.recetasapp.model.DiagnosisModel;
import com.unlam.recetasapp.model.DoctorPostPackage;
import com.unlam.recetasapp.model.PatientPostPackage;
import com.unlam.recetasapp.model.PharmacistPostPackage;
import com.unlam.recetasapp.model.PharmacyPostPackage;
import com.unlam.recetasapp.service.DiagnosisService;
import com.unlam.recetasapp.model.DoctorPostPackage;
import com.unlam.recetasapp.model.HospitalPostPackage;
import com.unlam.recetasapp.model.PrescriptionView;
import com.unlam.recetasapp.service.DoctorService;
import com.unlam.recetasapp.service.DrugService;
import com.unlam.recetasapp.service.HospitalService;
import com.unlam.recetasapp.service.MedicalConsultationService;
import com.unlam.recetasapp.service.PatientService;
import com.unlam.recetasapp.service.PharmacistService;
import com.unlam.recetasapp.service.PharmacyService;
import com.unlam.recetasapp.util.ResponseWrapper;

///////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////

@RestController
@RequestMapping(value = "/admin")
public class AdminController {

	@Autowired
	private DoctorService doctorService;

	@Autowired
	private PharmacistService pharmacistService;

	@Autowired
	private PatientService patientService;

	@Autowired
	private PharmacyService pharmacyService;

	@Autowired
	private HospitalService hospitalService;

	@Autowired
	private DrugService drugService;

	@Autowired
	private DiagnosisService diagnosisService;
	
	@Autowired
	private MedicalConsultationService mcService;

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/doctor", method = RequestMethod.GET)
	public ResponseWrapper getListOfDoctors() {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(doctorService.getAllDoctors());
		return r;
	}

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/doctor", method = RequestMethod.POST)
	public ResponseWrapper saveNewDoctor2(@RequestBody DoctorPostPackage doctorPostPackage) throws Exception {
		ResponseWrapper r = new ResponseWrapper();

		try {
			r.setResponse(doctorService.saveNewDoctor(doctorPostPackage));
		} catch (IllegalArgumentException e) {
			r.setError(e.getMessage());
		}

		return r;
	}

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/doctor", method = RequestMethod.PUT)
	public ResponseWrapper updateDoctor(@RequestBody DoctorPostPackage doctorPostPackage) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(doctorService.updateDoctor(doctorPostPackage));

		return r;
	}

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/doctor", method = RequestMethod.DELETE)
	public ResponseWrapper deleteDoctor(@RequestBody DoctorPostPackage doctorPostPackage) throws Exception {
		ResponseWrapper r = new ResponseWrapper();

		try {
			r.setResponse(doctorService.deleteDoctor(doctorPostPackage));
		} catch (IllegalArgumentException e) {
			r.setError(e.getMessage());
		}
		return r;
	}

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/pharmacist", method = RequestMethod.GET)
	public ResponseWrapper getListOfPharmacist() {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(pharmacistService.getAllPharmacists());
		return r;
	}

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/pharmacist", method = RequestMethod.POST)
	public ResponseWrapper saveNewPharmacist2(@RequestBody PharmacistPostPackage pharmacistPostPackage)
			throws Exception {
		ResponseWrapper r = new ResponseWrapper();

		try {
			r.setResponse(pharmacistService.saveNewPharmacist(pharmacistPostPackage));
		} catch (IllegalArgumentException e) {
			r.setError(e.getMessage());
		}

		return r;
	}

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/pharmacist", method = RequestMethod.PUT)
	public ResponseWrapper updatePharmacist(@RequestBody PharmacistPostPackage pharmacistPostPackage) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(pharmacistService.updatePharmacist(pharmacistPostPackage));

		return r;
	}

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/pharmacist", method = RequestMethod.DELETE)
	public ResponseWrapper deletePharmacist(@RequestBody PharmacistPostPackage pharmacistPostPackage) throws Exception {
		ResponseWrapper r = new ResponseWrapper();

		try {
			r.setResponse(pharmacistService.deletePharmacist(pharmacistPostPackage));
		} catch (IllegalArgumentException e) {
			r.setError(e.getMessage());
		}

		return r;
	}

	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/patient", method = RequestMethod.GET)
	public ResponseWrapper getListOfPatients() {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(patientService.getAllPatients());
		return r;
	}

	@CrossOrigin
	@RequestMapping(value = "/patient", method = RequestMethod.POST)
	public ResponseWrapper saveNewPatient(@RequestBody PatientPostPackage patientPostPackage) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		try {
			r.setResponse(patientService.saveNewPatient(patientPostPackage));
		} catch (DataIntegrityViolationException e) {
			r.setError("El DNI ingresado ya se encuentra registrado en el sistema");
		}

		return r;
	}

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/patient", method = RequestMethod.PUT)
	public ResponseWrapper updatePatient(@RequestBody PatientPostPackage patientPostPackage) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(patientService.updatePatient(patientPostPackage));

		return r;
	}

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/patient", method = RequestMethod.DELETE)
	public ResponseWrapper deletePharmacist(@RequestBody PatientPostPackage patientPostPackage) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(patientService.deletePatient(patientPostPackage));

		return r;
	}

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/pharmacy", method = RequestMethod.GET)
	public ResponseWrapper getListOfPharmacies() {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(pharmacyService.getAllPharmacies());
		return r;
	}

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/pharmacy", method = RequestMethod.POST)
	public ResponseWrapper saveNewPharmacy(@RequestBody PharmacyPostPackage pharmacyPostPackage) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(pharmacyService.saveNewPharmacy(pharmacyPostPackage));

		return r;
	}

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/pharmacy", method = RequestMethod.PUT)
	public ResponseWrapper updatePharmacy(@RequestBody PharmacyPostPackage pharmacyPostPackage) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(pharmacyService.updatePharmacy(pharmacyPostPackage));

		return r;
	}

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/pharmacy", method = RequestMethod.DELETE)
	public ResponseWrapper deletePharmacy(@RequestBody PharmacyPostPackage pharmacyPostPackage) throws Exception {
		ResponseWrapper r = new ResponseWrapper();

		try {
			r.setResponse(pharmacyService.deletePharmacy(pharmacyPostPackage));
		} catch (IllegalArgumentException e) {
			r.setError(e.getMessage());
		}

		return r;
	}

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/hospital", method = RequestMethod.GET)
	public ResponseWrapper getListOfHospitals() {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(hospitalService.getAllHospitals());
		return r;
	}

	@CrossOrigin @RequestMapping(value="/drugs",method=RequestMethod.POST)

	public String saveCSV(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
		if (file.isEmpty()) {
			redirectAttributes.addFlashAttribute("message", "No File is Present");
			return "redirect:uploadStatus";
		}
		try {
			String row = "";
			int rowCounter = 0;
			// Get the file and save it somewhere
			byte[] bytes = file.getBytes();
			InputStream is = null;
			BufferedReader bfReader = null;
			is = new ByteArrayInputStream(bytes);
			BufferedReader csvReader = new BufferedReader(new InputStreamReader(is, "UTF8"));
			HashMap<String, String[]> drugMap = new HashMap<>();
			while ((row = csvReader.readLine()) != null) {
				rowCounter++;
				String[] data = row.split(",");
				// do something with the data
				if (rowCounter > 1) {
					String comercialName = data[2];
					String genericName = data[3];
					String strength = data[4];
					String pharmaceuticalForm = data[5];
					drugMap.put(comercialName + genericName, data);
					System.out.println(
							"-" + comercialName + "-" + genericName + "-" + strength + "-" + pharmaceuticalForm + "");
				}
			}
			System.out.println("fin del read");
			for (Map.Entry<String, String[]> entry : drugMap.entrySet()) {
				String[] data = entry.getValue();
				String comercialName = data[2];
				String genericName = data[3];
				String strength = data[4];
				String pharmaceuticalForm = data[5];
				drugMap.put(comercialName + genericName, data);
				System.out.println(
						"-" + comercialName + "-" + genericName + "-" + strength + "-" + pharmaceuticalForm + "");
				drugService.addDrugFromExcelRow(comercialName, genericName, strength, pharmaceuticalForm);
			}

			csvReader.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return "redirect:/uploadStatus";
	}

	
	@CrossOrigin
	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value="/hospital", method = RequestMethod.POST)
	public ResponseWrapper saveNewHospital(@RequestBody HospitalPostPackage hospitalPostPackage) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(hospitalService.saveNewHospital(hospitalPostPackage));
		
		return r;
	}

	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/hospital", method = RequestMethod.PUT)
	public ResponseWrapper updateHospital(@RequestBody HospitalPostPackage hospitalPostPackage) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(hospitalService.updateHospital(hospitalPostPackage));

		return r;
	}

	@CrossOrigin
	@Secured({"ROLE_ADMIN"})
	@RequestMapping(value="/hospital", method = RequestMethod.DELETE)
	public ResponseWrapper deleteHospital(@RequestBody HospitalPostPackage hospitalPostPackage) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		
		try {
			r.setResponse(hospitalService.deleteHospital(hospitalPostPackage));
		}
		catch (IllegalArgumentException e) {
			r.setError(e.getMessage());
		}
		
		return r;
	}
	
	@CrossOrigin
	@RequestMapping(value="/mc", method = RequestMethod.GET)
	public ResponseWrapper getAllMC() throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		try {
			r.setResponse(mcService.getAll());
		}
		catch (IllegalArgumentException e) {
			r.setError(e.getMessage());
		}
		return r;
	}
	
	@CrossOrigin
	@RequestMapping(value="/diagnosis", method = RequestMethod.GET)
	public ResponseWrapper getAllDiagnosis() throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		try {
			r.setResponse(diagnosisService.getDiagnosisModelList());
		}
		catch (IllegalArgumentException e) {
			r.setError(e.getMessage());
		}
		return r;
	}
	@CrossOrigin
	@Secured({ "ROLE_ADMIN" })
	@RequestMapping(value = "/diagnosis", method = RequestMethod.POST)
	public ResponseWrapper saveNewDiagnosis(@RequestBody DiagnosisModel dm) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(diagnosisService.saveDiagnosis(dm));
		return r;
	}
}

package com.unlam.recetasapp.controller;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unlam.recetasapp.entity.Hospital;
import com.unlam.recetasapp.model.HospitalModel;
import com.unlam.recetasapp.repository.HospitalRepository;
import com.unlam.recetasapp.security.jwt.AuthenticationException;
import com.unlam.recetasapp.security.jwt.JWTWebSecurityConfig;
import com.unlam.recetasapp.security.jwt.JwtTokenRequest;
import com.unlam.recetasapp.security.jwt.JwtTokenResponse;
import com.unlam.recetasapp.security.jwt.JwtTokenUtil;
import com.unlam.recetasapp.security.jwt.JwtUserDetails;
import com.unlam.recetasapp.service.UserService;

@RestController
@CrossOrigin(origins={ "*" }) //"http://localhost:3000", "http://localhost:4200"
public class LoginController {
	
  Logger logger = LoggerFactory.getLogger(LoginController.class);

  @Value("${jwt.http.request.header}")
  private String tokenHeader;

  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private JwtTokenUtil jwtTokenUtil;

  @Autowired
  private UserDetailsService jwtUserDetailsService;

  
  @Autowired
  private HospitalRepository hospitalRepository;
  
  @Autowired
  private UserService userService;

  @RequestMapping(value = "${jwt.get.token.uri.pharmacist}", method = RequestMethod.POST)
  public ResponseEntity<?> createAuthenticationTokenPharmacist(HttpServletRequest request, @RequestBody JwtTokenRequest authenticationRequest)
      throws AuthenticationException {
	  
	//System.out.println(passwordEncoder.encode(authenticationRequest.getPassword()));
	
	try {			
		final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());
		if(userHasRole(userDetails, "ROLE_PHARMACIST")) {
			authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
			final String token = jwtTokenUtil.generateToken((JwtUserDetails) userDetails);
			return ResponseEntity.ok(new JwtTokenResponse(token));
		}else {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body("El usuario no posee acceso al módulo de farmacia.");
		}
		
	}catch(Exception e) {
		logger.error("Falló la autenticacion del usuario " + authenticationRequest.getUsername() + ": " + e.getMessage(), e);
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Usuario o contraseña incorrecto.");
	}
	
  }
  

@RequestMapping(value = "${jwt.get.token.uri.doctor}", method = RequestMethod.POST)
  public ResponseEntity<?> createAuthenticationTokenDoctor(HttpServletRequest request, @RequestBody JwtTokenRequest authenticationRequest)
      throws AuthenticationException {
	
	try {			
		UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());
		if(userHasRole(userDetails, "ROLE_DOCTOR")) {
			authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
			Hospital hospital = hospitalRepository.getOne(authenticationRequest.getIdHospital());
			addClaim((JwtUserDetails)userDetails, "idHospital", authenticationRequest.getIdHospital().toString());
			addClaim((JwtUserDetails)userDetails, "hospitalName", hospital.getName());
			final String token = jwtTokenUtil.generateToken((JwtUserDetails) userDetails);
			return ResponseEntity.ok(new JwtTokenResponse(token));
		}else {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body("El usuario no posee acceso al módulo de médico.");
		}
		
	}catch(Exception e) {
		logger.error("Falló la autenticacion del usuario " + authenticationRequest.getUsername() + ": " + e.getMessage());
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Usuario o contraseña incorrecto.");
	}

	
  }

private void addClaim(JwtUserDetails userDetails, String key, String value) {
	Map<String, Object> claims = userDetails.getClaims();
	claims.put(key, value);
	userDetails.setClaims(claims);
}


@RequestMapping(value = "${jwt.get.token.uri.preauthentication.doctor}", method = RequestMethod.POST)
public ResponseEntity<?> preAuthenticationTokenDoctor(HttpServletRequest request, @RequestBody JwtTokenRequest authenticationRequest)
    throws AuthenticationException {
	
	try {
		
		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
		List<HospitalModel> listHospitalModels = userService.getDoctorHospitals(authenticationRequest.getUsername());
		return ResponseEntity.ok(listHospitalModels);
//		if(userHasRole(userDetails, "ROLE_DOCTOR")) {
//			User user = userRepository.findByUsername(authenticationRequest.getUsername());
//			final String token = jwtTokenUtil.generateToken((JwtUserDetails) userDetails);
//			return ResponseEntity.ok(new JwtTokenResponse(token));
//		}else {
//			return ResponseEntity.status(HttpStatus.FORBIDDEN).body("El usuario no posee acceso al módulo de médico.");
//		}
		
	}catch(Exception e) {
		logger.error("Falló la autenticacion del usuario " + authenticationRequest.getUsername() + ": " + e.getMessage());
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Usuario o contraseña incorrecto.");
	}

	
}

@RequestMapping(value = "${jwt.get.token.uri.patient}", method = RequestMethod.POST)
public ResponseEntity<?> createAuthenticationTokenPatient(HttpServletRequest request, @RequestBody JwtTokenRequest authenticationRequest)
    throws AuthenticationException {
	
	try {			
		final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());
		if(userHasRole(userDetails, "ROLE_PATIENT")) {
			authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
			final String token = jwtTokenUtil.generateTokenMobile((JwtUserDetails) userDetails);
			return ResponseEntity.ok(new JwtTokenResponse(token));
		}else {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body("El usuario no posee acceso a la aplicación de paciente.");
		}
		
	}catch(Exception e) {
		logger.error("Falló la autenticacion del usuario " + authenticationRequest.getUsername() + ": " + e.getMessage());
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Usuario o contraseña incorrecto.");
	}
	
}


@RequestMapping(value = "${jwt.get.token.uri.admin}", method = RequestMethod.POST)
public ResponseEntity<?> createAuthenticationTokenAdmin(HttpServletRequest request, @RequestBody JwtTokenRequest authenticationRequest)
    throws AuthenticationException {
	
	try {			
		final UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());
		if(userHasRole(userDetails, "ROLE_ADMIN")) {
			authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
			final String token = jwtTokenUtil.generateToken((JwtUserDetails) userDetails);
			return ResponseEntity.ok(new JwtTokenResponse(token));
		}else {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body("El usuario no posee acceso al módulo de administrador.");
		}
		
	}catch(Exception e) {
		logger.error("Falló la autenticacion del usuario " + authenticationRequest.getUsername() + ": " + e.getMessage(), e);
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Usuario o contraseña incorrecto.");
	}
	
}


@RequestMapping(value = "${jwt.get.token.uri.preauthentication.hospitalAdmin}", method = RequestMethod.POST)
public ResponseEntity<?> preAuthenticationTokenHospitalAdmin(HttpServletRequest request, @RequestBody JwtTokenRequest authenticationRequest)
    throws AuthenticationException {
	
	try {
		
		authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
		List<HospitalModel> listHospitalModels = userService.getAdminHospitals(authenticationRequest.getUsername());
		return ResponseEntity.ok(listHospitalModels);
		
	}catch(Exception e) {
		logger.error("Falló la autenticacion del usuario " + authenticationRequest.getUsername() + ": " + e.getMessage());
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Usuario o contraseña incorrecto.");
	}

	
}

@RequestMapping(value = "${jwt.get.token.uri.hospitalAdmin}", method = RequestMethod.POST)
public ResponseEntity<?> createAuthenticationTokenHospitalAdmin(HttpServletRequest request, @RequestBody JwtTokenRequest authenticationRequest)
    throws AuthenticationException {
	
	try {			
		UserDetails userDetails = jwtUserDetailsService.loadUserByUsername(authenticationRequest.getUsername());
		if(userHasRole(userDetails, "ROLE_HOSPITAL_ADMIN")) {
			authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
			Hospital hospital = hospitalRepository.getOne(authenticationRequest.getIdHospital());
			addClaim((JwtUserDetails)userDetails, "idHospital", authenticationRequest.getIdHospital().toString());
			addClaim((JwtUserDetails)userDetails, "hospitalName", hospital.getName());
			final String token = jwtTokenUtil.generateToken((JwtUserDetails) userDetails);
			return ResponseEntity.ok(new JwtTokenResponse(token));
		}else {
			return ResponseEntity.status(HttpStatus.FORBIDDEN).body("El usuario no posee acceso al módulo de médico.");
		}
		
	}catch(Exception e) {
		logger.error("Falló la autenticacion del usuario " + authenticationRequest.getUsername() + ": " + e.getMessage());
		return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Usuario o contraseña incorrecto.");
	}

	
}

  @RequestMapping(value = "${jwt.refresh.token.uri}", method = RequestMethod.POST)
  public ResponseEntity<?> refreshAndGetAuthenticationToken(HttpServletRequest request) {
    String authToken = request.getHeader(tokenHeader);
    final String token = authToken.substring(7);
    
    String username = jwtTokenUtil.getUsernameFromToken(token);
    JwtUserDetails user = (JwtUserDetails) jwtUserDetailsService.loadUserByUsername(username);

    if (jwtTokenUtil.canTokenBeRefreshed(token)) {
      String refreshedToken = jwtTokenUtil.refreshToken(token);
      return ResponseEntity.ok(new JwtTokenResponse(refreshedToken));
    } else {
      return ResponseEntity.badRequest().body(null);
    }
  }

  private Boolean userHasRole(UserDetails userDetails, String role) {
	  List<SimpleGrantedAuthority> authorities = (List<SimpleGrantedAuthority>) userDetails.getAuthorities();
	  for(SimpleGrantedAuthority authority : authorities) {
		  if(authority.getAuthority().equals(role)) {
			  return true;
		  }
	  }
	  return false;
  }
  
  @ExceptionHandler({ AuthenticationException.class })
  public ResponseEntity<String> handleAuthenticationException(AuthenticationException e) {
    return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
  }

  private void authenticate(String username, String password) {
    Objects.requireNonNull(username);
    Objects.requireNonNull(password);
    System.out.println(username + " " + password);

    try {
      authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
    } catch (DisabledException e) {
      throw new AuthenticationException("USER_DISABLED", e);
    } catch (BadCredentialsException e) {
      throw new AuthenticationException("INVALID_CREDENTIALS", e);
    }
  }
}


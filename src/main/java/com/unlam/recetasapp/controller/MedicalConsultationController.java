package com.unlam.recetasapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unlam.recetasapp.model.ChronicAssignmentModel;
import com.unlam.recetasapp.model.MedicalConsultationPostPackage;
import com.unlam.recetasapp.model.PatientChronicDiseaseModel;
import com.unlam.recetasapp.model.RequestFilePrescription;
import com.unlam.recetasapp.service.ChronicService;
import com.unlam.recetasapp.service.MedicalConsultationService;
import com.unlam.recetasapp.util.ResponseWrapper;

@RestController
@RequestMapping(value = "/medicalConsultation")
public class MedicalConsultationController {
	
	@Autowired
	private MedicalConsultationService medicalConsultationService;
	
	@Autowired
	private ChronicService chronicService;
	
	/**
	 * this method persist a medicalConsultation
	 * 
	 * @param medicalConsultationPostPackage
	 * @return
	 * @throws Exception
	 */
	@CrossOrigin
	@Secured({"ROLE_DOCTOR"})
	@RequestMapping(value="/prescription", method = RequestMethod.POST)
	public ResponseWrapper saveMedicalConsultation(@RequestBody MedicalConsultationPostPackage medicalConsultationPostPackage) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(medicalConsultationService.saveMedicalConsultation(medicalConsultationPostPackage));
		return r;
	}
	
	@CrossOrigin
	@RequestMapping(value="/prescription/{prescriptionId}", method = RequestMethod.POST)
	public ResponseWrapper signPrescription(@PathVariable(required = true) Long prescriptionId, @RequestBody RequestFilePrescription file) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(medicalConsultationService.signPrescription(prescriptionId,file.getFile()));
		return r;
	}
	
	@CrossOrigin
	@RequestMapping(value="/prescription/chronic", method = RequestMethod.POST)
	public ResponseWrapper setChronicAssignamentToPatient(@RequestBody ChronicAssignmentModel model) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(chronicService.saveAssignment(model));
		return r;
	}
	
	@CrossOrigin
	@RequestMapping(value="/prescription/patientStatusChronic", method = RequestMethod.POST)
	public ResponseWrapper setChronicDiseaseToPatient(@RequestBody PatientChronicDiseaseModel model) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(chronicService.saveStatusChronic(model));
		return r;
	}
	
	@CrossOrigin
	@RequestMapping(value="/prescription/{prescriptionId}", method = RequestMethod.GET)
	public ResponseWrapper signPrescription(@PathVariable(required = true) Long prescriptionId) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(medicalConsultationService.getPrescription(prescriptionId).getFile());
		return r;
	}
	
	@CrossOrigin
	@RequestMapping(value="/chronic/{hospitalId}/{patientId}", method = RequestMethod.GET)
	public ResponseWrapper getChronicMedicalConsultation(@PathVariable(required = true) Long hospitalId,@PathVariable(required = true) Long patientId) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(medicalConsultationService.getChronicMedicalConsultationPatientIdAndHospitalId(patientId,hospitalId));
		return r;
	}
	
	@CrossOrigin
	@RequestMapping(value="/prescription/chronic/{patientId}", method = RequestMethod.GET)
	public ResponseWrapper getChronicAssignmentByPatientId(@PathVariable Long patientId) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(chronicService.getChronicAssignmentListByPatientId(patientId));
		return r;
	}	
	
	@CrossOrigin
	@RequestMapping(value="/prescription/pending/{doctorId}", method = RequestMethod.GET)
	public ResponseWrapper getPrescriptionPendingByDoctorId(@PathVariable Long doctorId) throws Exception {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(medicalConsultationService.findPrescriptionPendingByDoctorId(doctorId));
		return r;
	}	
}

package com.unlam.recetasapp.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unlam.recetasapp.entity.Patient;
import com.unlam.recetasapp.entity.Pharmacist;
import com.unlam.recetasapp.entity.PharmacistPharmacy;
import com.unlam.recetasapp.entity.PharmacyPrescription;
import com.unlam.recetasapp.entity.Prescription;
import com.unlam.recetasapp.model.PrescriptionPharmacyPreviewModel;
import com.unlam.recetasapp.repository.PharmacyPrescriptionRepository;
import com.unlam.recetasapp.service.PharmacistPharmacyService;
import com.unlam.recetasapp.service.PharmacistService;
import com.unlam.recetasapp.service.PrescriptionService;
import com.unlam.recetasapp.util.ResponseWrapper;

@RestController
@RequestMapping(value = "/pharmacy")
public class PharmacyController {
	
	Logger logger = LoggerFactory.getLogger(TokenController.class);
	
	@Autowired
	private PrescriptionService prescriptionService;
	
	@Autowired
	private PharmacistService pharmacistService;
	
	@Autowired
	private PharmacistPharmacyService pharmacistPharmacyService;
	
	@Autowired
	private PharmacyPrescriptionRepository pharmacyPrescriptionRepository;

	
	//Descarga de listado de recetas presentadas en la farmacia del farmacéutico
	@CrossOrigin
	@Secured({"ROLE_PHARMACIST"})
	@RequestMapping(value="/prescription-list/{dni}", method = RequestMethod.GET)
	public ResponseWrapper getPharmacyPrescriptions(@PathVariable(required = true) Integer dni) {
		ResponseWrapper r = new ResponseWrapper();
		try {
			
			//Buscamos el objeto farmacéutico a partir del DNI
			Pharmacist pharmacist = pharmacistService.getPharmacist(dni);
			//Buscamos la relación entre el farmacéutico y la farmacia en donde trabaja
			PharmacistPharmacy pharmacistPharmacy = pharmacistPharmacyService.getPharmacistPharmacy(pharmacist);
			
			
			List<PharmacyPrescription> pharmacyPrescriptionList = pharmacyPrescriptionRepository.
					findByPharmacyIdOrderByInsertdateDesc(pharmacistPharmacy.getPharmacy().getId());
			
			
			ArrayList<PrescriptionPharmacyPreviewModel> prescriptionPreviews = new ArrayList<PrescriptionPharmacyPreviewModel>();
			
			pharmacyPrescriptionList.stream().forEach(pp -> {
				PrescriptionPharmacyPreviewModel  ppm = new PrescriptionPharmacyPreviewModel(pp);
				
				//Buscamos los datos del paciente a partir de la receta
				Prescription pr = prescriptionService.getById(pp.getPrescription().getId());
				Patient pa = pr.getMedicalConsultation().getPatient();
				String patientFullname = pa.getName() + " " + pa.getSurname();
				Integer patientDni = pa.getDni();
				ppm.setPatientFullname(patientFullname);
				ppm.setPatientDni(patientDni);
				
				prescriptionPreviews.add(ppm);

			});
			
			r.setResponse(prescriptionPreviews);
			
		}catch(Exception e) {
			logger.error(e.getMessage());
			r.setError(e.getMessage());
		}
		return r;
	}
}

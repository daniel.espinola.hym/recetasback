package com.unlam.recetasapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unlam.recetasapp.service.DoctorService;
import com.unlam.recetasapp.util.ResponseWrapper;

@RestController
@RequestMapping(value = "/doctor")
public class DoctorController {
	
	@Autowired
	private DoctorService doctorService;
	
	@CrossOrigin
	@RequestMapping(value="/{doctorId}", method = RequestMethod.GET)
	public ResponseWrapper getDoctorById(@PathVariable(required = true) Long doctorId) {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(doctorService.getDoctorById(doctorId));
		return r;
	}
	
	@CrossOrigin
	@Secured({"ROLE_DOCTOR"})
	@RequestMapping(value="/getChronicPatientList/{doctorId}/{hospitalId}", method = RequestMethod.GET)
	public ResponseWrapper getChronicPatientListByDoctorId(@PathVariable(required = true) Long doctorId,@PathVariable(required = true) Long hospitalId) {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(doctorService.getChronicPatientListByDoctorIdAndHospitalId(doctorId,hospitalId));
		return r;
	}
}

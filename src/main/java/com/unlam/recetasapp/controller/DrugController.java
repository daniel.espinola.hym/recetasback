package com.unlam.recetasapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unlam.recetasapp.service.DrugService;
import com.unlam.recetasapp.util.ResponseWrapper;

@RestController
@RequestMapping(value = "/drug")
public class DrugController {
	
	@Autowired
	private DrugService drugService;
	
	@CrossOrigin
	@Secured({"ROLE_DOCTOR"})
	@RequestMapping(method = RequestMethod.GET)
	public ResponseWrapper getAllDrugs() {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(drugService.getAllDrugs());
		return r;
	}
	
	@CrossOrigin
	@Secured({"ROLE_DOCTOR"})
	@RequestMapping(value="/{drugId}/options", method = RequestMethod.GET)
	public ResponseWrapper getDrugGenericVariety(@PathVariable(required = true) Long drugId) {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(drugService.getDrugGenericVariety(drugId));
		return r;
	}
	
	@CrossOrigin
	@RequestMapping(value="/prescription/{prescriptionId}", method = RequestMethod.GET)
	public ResponseWrapper getAllDrugByPrescriptionId(@PathVariable(required = true) Long prescriptionId) {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(drugService.getAllDrugByPrescriptionId(prescriptionId));
		return r;
	}
		
}

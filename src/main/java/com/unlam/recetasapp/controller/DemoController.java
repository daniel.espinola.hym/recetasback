package com.unlam.recetasapp.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unlam.recetasapp.entity.DrugDemoView;
import com.unlam.recetasapp.entity.Province;
import com.unlam.recetasapp.repository.ProvinceRepository;
import com.unlam.recetasapp.service.ChronicService;
import com.unlam.recetasapp.util.ResponseWrapper;

@RestController
public class DemoController {
	
	
	@Autowired
	private ProvinceRepository provinceRepository;
	
	@Autowired
	private ChronicService chronicService;
	
	Logger logger = LoggerFactory.getLogger(DemoController.class);
	
	@CrossOrigin
	@RequestMapping(value="/getDrug", method = RequestMethod.GET)
	public DrugDemoView getDrug(@RequestParam(name = "drugId", required = true) Integer drugId){
		
		
		logger.info("Se recibió codigo " + drugId);
		
		if(drugId == 1) {
			return new DrugDemoView(1, "Ibuprofeno", "600mg");
		}else if(drugId == 2) {
			return new DrugDemoView(2, "Amoxicilina", "400mg");
		}else if(drugId == 3) {
			return new DrugDemoView(3, "Paracetamol", "500mg");
		}else {
			logger.error("Código " + drugId + " no valido");
			return new DrugDemoView(0, "No se encontró droga con código " + drugId, "X");
		}
		
	}
	
	@CrossOrigin
	@RequestMapping(value="/getProvinces", method = RequestMethod.GET)
	public List<Province> getProvinces(){	
		
		return provinceRepository.findAll();
		
	}
	
	@CrossOrigin
	@RequestMapping(value="/triggerClock", method = RequestMethod.GET)
	public void testMethod(){	
		System.out.println("hola soy el clock");
	}

}

package com.unlam.recetasapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.unlam.recetasapp.service.ChronicService;
import com.unlam.recetasapp.service.DiagnosisService;
import com.unlam.recetasapp.util.ResponseWrapper;


@RestController
@RequestMapping(value="/diagnosis")
public class DiagnosisController {
	
	@Autowired
	private DiagnosisService diagnosisService;
	
	@Autowired
	private ChronicService chronicService;
	
	@CrossOrigin
	@Secured({"ROLE_DOCTOR"})
	@RequestMapping(method = RequestMethod.GET)
	public ResponseWrapper getAllDiagnosis() {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(diagnosisService.getDiagnosisList());
		return r;
	}
	
	
	@CrossOrigin
	@RequestMapping(value="/chronic",method = RequestMethod.GET)
	public ResponseWrapper getAllChronicDiagnosis() {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(diagnosisService.getChronicDiagnosisList());
		return r;
	}
	
	@CrossOrigin
	@RequestMapping(value="/chronic/patient/{patientId}",method = RequestMethod.GET)
	public ResponseWrapper getAllChronicDiagnosis(@PathVariable(required = true) Long patientId) {
		ResponseWrapper r = new ResponseWrapper();
		r.setResponse(chronicService.getChronicDiseaseListByPatientId(patientId));
		return r;
	}
}

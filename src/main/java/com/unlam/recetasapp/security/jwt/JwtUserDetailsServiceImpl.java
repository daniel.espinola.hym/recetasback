package com.unlam.recetasapp.security.jwt;

import java.util.HashMap;
import java.util.Map;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.unlam.recetasapp.entity.Doctor;
import com.unlam.recetasapp.entity.User;
import com.unlam.recetasapp.model.PatientModel;
import com.unlam.recetasapp.repository.UserRepository;

@Service
public class JwtUserDetailsServiceImpl implements UserDetailsService{

	@Autowired
	UserRepository userRepository;
	
	  @Override
	  @Transactional
	  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		  
		Map<String, Object> claims = new HashMap<String, Object>();  
		User user = userRepository.findByUsername(username);
		
		if(user.getPatient() != null) {
			PatientModel patient = new PatientModel(user.getPatient());
			claims.put("fullName", patient.getFullName());
			claims.put("dni", patient.getDni());
			claims.put("age", patient.getAge());
			claims.put("idPatient", patient.getId());
			claims.put("birthDate", patient.getBirthDate());
		}
		if(user.getDoctor() != null) {
			Doctor doctor = user.getDoctor();
			claims.put("fullName", doctor.getName() + " " + doctor.getSurname());
			claims.put("dni", user.getDoctor().getDni());
			claims.put("licenseNumber", user.getDoctor().getDoctorHospital().get(0).getDoctorLicenseNumber());
			claims.put("idDoctor", user.getDoctor().getId());
		}
		if(user.getPharmacist() != null) {
			claims.put("idPharmacist", user.getPharmacist().getId());
			claims.put("dni", user.getPharmacist().getDni());
			claims.put("fullName", user.getPharmacist().getName() + " " + user.getPharmacist().getSurname());
			claims.put("insertDate", user.getPharmacist().getPharmacistPharmacy().get(0).getInsertdate());
			claims.put("pharmacyName", user.getPharmacist().getPharmacistPharmacy().get(0).getPharmacy().getName());
			claims.put("pharmacyAddress", user.getPharmacist().getPharmacistPharmacy().get(0).getPharmacy().getAddress() + " " + user.getPharmacist().getPharmacistPharmacy().get(0).getPharmacy().getAddressNumber());
		}
		if(user.getHospitalAdmin() != null) {
			claims.put("idHospitalAdmin", user.getHospitalAdmin().getId());
			claims.put("dni", user.getHospitalAdmin().getDni());
			claims.put("fullName", user.getHospitalAdmin().getName() + " " + user.getHospitalAdmin().getSurname());
			//claims.put("insertDate", user.getHospitalAdmin().getPharmacistPharmacy().get(0).getInsertdate());
		}
		
	    JwtUserDetails jwtUserDetails = new JwtUserDetails(user.getId(), user.getUsername(), user.getPassword(), user.getRole(), claims);

	    return jwtUserDetails;
	  }
}

package com.unlam.recetasapp.security.jwt;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.function.Function;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Clock;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClock;

@Component
public class JwtTokenUtil implements Serializable {

  static final String CLAIM_KEY_USERNAME = "sub";
  static final String CLAIM_KEY_CREATED = "iat";
  private static final long serialVersionUID = -3301605591108950415L;
  private Clock clock = DefaultClock.INSTANCE;

  @Value("${jwt.signing.key.secret}")
  private String secret;

  @Value("${jwt.token.expiration.in.seconds}")
  private Long longExpiration;
  
  @Value("${jwt.token.short.access.expiration.in.seconds}")
  private Long shortExpiration;
  
  @Value("${jwt.token.mobile.expiration..in.seconds}")
  private Long longMobileExpiration;
  
  @Value("${jwt.token.short.access.mobile.expiration.in.seconds}")
  private Long shortMobileExpiration;

  public String getUsernameFromToken(String token) {
    return getClaimFromToken(token, Claims::getSubject);
  }

  public Date getIssuedAtDateFromToken(String token) {
    return getClaimFromToken(token, Claims::getIssuedAt);
  }

  public Date getExpirationDateFromToken(String token) {
    return getClaimFromToken(token, Claims::getExpiration);
  }
  
  public Date getShortAccessExpirationDateFromToken(String token) {
	    final Claims claims = getAllClaimsFromToken(token);
	    return claims.get("shortAccessExpirationDate", Date.class);
  }

  public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
    final Claims claims = getAllClaimsFromToken(token);
    return claimsResolver.apply(claims);
  }

  private Claims getAllClaimsFromToken(String token) {
    return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
  }

  private Boolean isTokenExpired(String token) {
    final Date expiration = getExpirationDateFromToken(token);
    return expiration.before(clock.now());
  }
  
  private Boolean isShortAccessExpired(String token) {
	    final Date expiration = getShortAccessExpirationDateFromToken(token);
	    return expiration.before(clock.now());
  }

  public String generateToken(JwtUserDetails userDetails) {
    return doGenerateToken(userDetails.getClaims(), userDetails.getUsername());
  }

  private String doGenerateToken(Map<String, Object> claims, String subject) {
    final Date createdDate = clock.now();
    final Date expirationDate = calculateExpirationDate(createdDate);
    final Date shortAccessExpirationDate = calculateShortAccessExpirationDate(createdDate);
    
    claims.put("shortAccessExpirationDate", shortAccessExpirationDate);

    return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(createdDate)
        .setExpiration(expirationDate).signWith(SignatureAlgorithm.HS512, secret).compact();
  }

  public Boolean canTokenBeRefreshed(String token) {
    return (!isTokenExpired(token) || !isShortAccessExpired(token));
  }

  public String refreshToken(String token) {
    final Date createdDate = clock.now();
    //final Date expirationDate = calculateExpirationDate(createdDate);
    
    final Date shortAccessExpirationDate = calculateShortAccessExpirationDate(createdDate);
    

    final Claims claims = getAllClaimsFromToken(token);
    claims.setIssuedAt(createdDate);
    //claims.setExpiration(expirationDate);
    
    claims.put("shortAccessExpirationDate", shortAccessExpirationDate);
    

    return Jwts.builder().setClaims(claims).signWith(SignatureAlgorithm.HS512, secret).compact();
  }

  public Boolean validateToken(String token, UserDetails userDetails) {
    JwtUserDetails user = (JwtUserDetails) userDetails;
    final String username = getUsernameFromToken(token);
    return (username.equals(user.getUsername()) && !isTokenExpired(token));
  }

  private Date calculateExpirationDate(Date createdDate) {
    return new Date(createdDate.getTime() + longExpiration * 1000);
  }
  
  private Date calculateShortAccessExpirationDate(Date createdDate) {
	   return new Date(createdDate.getTime() + shortExpiration * 1000);
  }

	public Boolean isValidShortLivingToken(String jwtToken) {
		return !isShortAccessExpired(jwtToken);
	}

	public String generateTokenMobile(JwtUserDetails userDetails) {
		return doGenerateTokenMobile(userDetails.getClaims(), userDetails.getUsername());
	}

	private String doGenerateTokenMobile(Map<String, Object> claims, String subject) {
	    final Date createdDate = clock.now();
	    final Date expirationDate = calculateExpirationDateMobile(createdDate);
	    final Date shortAccessExpirationDate = calculateShortAccessExpirationDateMobile(createdDate);
	    
	    claims.put("shortAccessExpirationDate", shortAccessExpirationDate);

	    return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(createdDate)
	        .setExpiration(expirationDate).signWith(SignatureAlgorithm.HS512, secret).compact();
	}

	private Date calculateShortAccessExpirationDateMobile(Date createdDate) {
		return new Date(createdDate.getTime() + shortMobileExpiration * 1000);
	}

	private Date calculateExpirationDateMobile(Date createdDate) {
		return new Date(createdDate.getTime() + longMobileExpiration * 1000);
	}
  
}


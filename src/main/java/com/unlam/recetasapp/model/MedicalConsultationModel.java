package com.unlam.recetasapp.model;

public class MedicalConsultationModel implements Comparable<MedicalConsultationModel> {
	private Long id;
	private Long doctorId;
	private String doctorName;
	private Long patientId;
	private String patientName;
	private Long hospitalId;
	private String hospitalName;
	private PrescriptionModel prescription;
	private String medicalConsultationDate;
	private String dateString;
	private Long prescriptionId;
	private boolean tieneReceta;

	public MedicalConsultationModel() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public PrescriptionModel getPrescription() {
		return prescription;
	}

	public void setPrescription(PrescriptionModel prescription) {
		this.prescription = prescription;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public Long getHospitalId() {
		return hospitalId;
	}

	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}

	public String getMedicalConsultationDate() {
		return medicalConsultationDate;
	}

	public void setMedicalConsultationDate(String medicalConsultationDate) {
		this.medicalConsultationDate = medicalConsultationDate;
	}

	public String getDateString() {
		return dateString;
	}

	public void setDateString(String dateString) {
		this.dateString = dateString;
	}

	public Long getPrescriptionId() {
		return prescriptionId;
	}

	public void setPrescriptionId(Long prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	public boolean isTieneReceta() {
		return tieneReceta;
	}

	public void setTieneReceta(boolean tieneReceta) {
		this.tieneReceta = tieneReceta;
	}

	@Override
	public int compareTo(MedicalConsultationModel o) {
		if (o.id > this.id) {
			return 1;
		}
		return -1;
	}

}

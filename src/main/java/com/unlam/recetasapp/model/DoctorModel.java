package com.unlam.recetasapp.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;

import com.unlam.recetasapp.entity.Doctor;
import com.unlam.recetasapp.entity.DoctorHospital;
import com.unlam.recetasapp.entity.User;
import com.unlam.recetasapp.repository.UserRepository;

public class DoctorModel {
	private String name;
	private String surname;
	private Integer dni;
	private String hospitalLicense;
	private String assignedHospital;
	private String birthDate;
	private Integer dniType;
	private boolean enabledStatus = true;
	
	@Autowired
	UserRepository userRepository;

	public DoctorModel() {
		
	}
	
	public DoctorModel(Doctor doctor) {
		this.name = doctor.getName();
		this.surname = doctor.getSurname();
		this.dni = doctor.getDni();
		this.dniType = doctor.getDnitype();
		this.birthDate = new SimpleDateFormat("dd/MM/yyyy").format(doctor.getBirthdate());
	}
	
	public DoctorModel(Doctor doctor, DoctorHospital dh) {
		this.name = doctor.getName();
		this.surname = doctor.getSurname();
		this.dni = doctor.getDni();
		this.dniType = doctor.getDnitype();
		this.birthDate = new SimpleDateFormat("dd/MM/yyyy").format(doctor.getBirthdate());
		this.setHospitalLicenseData(dh);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getAssignedHospital() {
		return assignedHospital;
	}

	public void setAssignedHospital(String assignedHospital) {
		this.assignedHospital = assignedHospital;
	}
	
	public String getSurename() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}
	
	public Integer getDniType() {
		return dniType;
	}

	public void setDniType(Integer dniType) {
		this.dniType = dniType;
	}

	public String getHospitalLicense() {
		return hospitalLicense;
	}

	public void setHospitalLicense(String hospitalLicense) {
		this.hospitalLicense = hospitalLicense;
	}
	
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getBirthDate() {
		return birthDate;
	}
	
	public void setHospitalLicenseData(DoctorHospital dh) {
		this.assignedHospital = dh.getHospital().getName();
		this.hospitalLicense = dh.getDoctorLicenseNumber();
	}
	
	public boolean getEnabledStatus() {
		return enabledStatus;
	}

	public void setEnabledStatus(boolean enabledStatus) {
		this.enabledStatus = enabledStatus;
	}

	
}

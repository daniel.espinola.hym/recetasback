package com.unlam.recetasapp.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.unlam.recetasapp.entity.Locality;

public class HospitalPostPackage {

	private Long id;
	private String name;
	private String address;
	private Integer addressNumber;
	private Long localityId;

	
	HospitalPostPackage(Long id, String name, String address, Integer addressNumber, Long localityId){
		this.id = id;
		this.name = name;
		this.address = address;
		this.addressNumber = addressNumber;
		this.localityId = localityId;
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public Integer getAddressNumber() {
		return addressNumber;
	}
	
	public void setAddressNumber(Integer addressNumber) {
		this.addressNumber = addressNumber;
	}
	
	public Long getLocalityId() {
		return localityId;
	}
	
	public void setLocalityId(Long localityId) {
		this.localityId = localityId;
	}
}

package com.unlam.recetasapp.model;

import java.util.List;

public class DrugsPackage {
	
	private List<DrugGenericModel> drugGenericList;
	
	private List<DrugComercialModel> drugComercialList;
	

	public DrugsPackage(List<DrugGenericModel> dgModelList, List<DrugComercialModel> dcModelList) {
		super();
		this.drugGenericList = dgModelList;
		this.drugComercialList = dcModelList;
	}

	public List<DrugGenericModel> getDrugGenericList() {
		return drugGenericList;
	}

	public void setDrugGenericList(List<DrugGenericModel> drugGenericList) {
		this.drugGenericList = drugGenericList;
	}

	public List<DrugComercialModel> getDrugComercialList() {
		return drugComercialList;
	}

	public void setDrugComercialList(List<DrugComercialModel> drugComercialList) {
		this.drugComercialList = drugComercialList;
	}



}

package com.unlam.recetasapp.model;

import java.util.List;

public class PrescriptionPreviewPackage {
	
	private List<PrescriptionPreviewModel> prescriptionPreviewList;
	

	public PrescriptionPreviewPackage(List<PrescriptionPreviewModel> prescriptionPreviewList) {
		super();
		this.prescriptionPreviewList = prescriptionPreviewList;
	}

	public List<PrescriptionPreviewModel> getPrescriptionPreviewList() {
		return prescriptionPreviewList;
	}

	public void setPrescriptionPreviewList(List<PrescriptionPreviewModel> prescriptionPreviewList) {
		this.prescriptionPreviewList = prescriptionPreviewList;
	}
	
	

}

package com.unlam.recetasapp.model;

public class RequestChronicDisease {
	private Long patientId;
	private Long chronicDiseaseId;

	public RequestChronicDisease() {

	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public Long getChronicDiseaseId() {
		return chronicDiseaseId;
	}

	public void setChronicDiseaseId(Long chronicDiseaseId) {
		this.chronicDiseaseId = chronicDiseaseId;
	}

}

package com.unlam.recetasapp.model;

public class DoctorPatientModel implements Comparable<DoctorPatientModel>{
	private String patientName;
	private String patientDni;
	private String consultationDate;
	private String diagnosis;
	private String hasChronicAssignment;
	private int prescriptionId;

	public DoctorPatientModel() {
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public String getPatientDni() {
		return patientDni;
	}

	public void setPatientDni(String patientDni) {
		this.patientDni = patientDni;
	}

	public String getConsultationDate() {
		return consultationDate;
	}

	public void setConsultationDate(String consultationDate) {
		this.consultationDate = consultationDate;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public String getHasChronicAssignment() {
		return hasChronicAssignment;
	}

	public void setHasChronicAssignment(String hasChronicAssignment) {
		this.hasChronicAssignment = hasChronicAssignment;
	}

	public int getPrescriptionId() {
		return prescriptionId;
	}

	public void setPrescriptionId(int prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	@Override
	public int compareTo(DoctorPatientModel o) {
		if(this.prescriptionId>o.getPrescriptionId()) {
			return -1;
		}
		return 1;
	}

}

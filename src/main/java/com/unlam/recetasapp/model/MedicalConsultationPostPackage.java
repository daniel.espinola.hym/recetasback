package com.unlam.recetasapp.model;

public class MedicalConsultationPostPackage {
	private Long doctorId;
	private Long hospitalId;
	
	private Long patientId;
	
	private String observation;
	
	private PrescriptionView prescription;
	

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public Long getHospitalId() {
		return hospitalId;
	}

	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public PrescriptionView getPrescription() {
		return prescription;
	}

	public void setPrescription(PrescriptionView prescriptionView) {
		this.prescription = prescriptionView;
	}


	
	
	
}

package com.unlam.recetasapp.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.unlam.recetasapp.entity.Prescription;
import com.unlam.recetasapp.entity.PrescriptionItem;
import com.unlam.recetasapp.entity.PrescriptionItemDiagnosis;

public class PrescriptionPreviewModel {

	@JsonProperty("id")
	private Long prescriptionId;

	private List<String> diagnosisList = new ArrayList<String>();
	private MedicalConsultationPreview medicalConsultation;
	private Long prescriptionStatus;

	public PrescriptionPreviewModel(Long prescriptionId, List<String> diagnosisList,
			MedicalConsultationPreview medicalConsultation) {
		super();
		this.prescriptionId = prescriptionId;
		this.diagnosisList = diagnosisList;
		this.medicalConsultation = medicalConsultation;
	}

	public PrescriptionPreviewModel() {
	}

	public PrescriptionPreviewModel(Prescription prescription) {
		this.prescriptionId = prescription.getId();

		List<PrescriptionItem> prescriptionItems = prescription.getPrescriptionItem();

		for (PrescriptionItem pItem : prescriptionItems) {
			List<PrescriptionItemDiagnosis> pIDiagnosisList = pItem.getPrescriptionItemDiagnosis();
			for (PrescriptionItemDiagnosis pIDiagnosis : pIDiagnosisList) {
				diagnosisList.add(pIDiagnosis.getDiagnosis().getDescription());
			}
		}

		this.prescriptionStatus = prescription.getPrescriptionStatus().getId();

		this.medicalConsultation = new MedicalConsultationPreview(prescription.getMedicalConsultation());

	}

	public Long getPrescriptionId() {
		return prescriptionId;
	}

	public void setPrescriptionId(Long prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	public List<String> getDiagnosisList() {
		return diagnosisList;
	}

	public void setDiagnosisList(List<String> diagnosisList) {
		this.diagnosisList = diagnosisList;
	}

	public MedicalConsultationPreview getMedicalConsultation() {
		return medicalConsultation;
	}

	public void setMedicalConsultation(MedicalConsultationPreview medicalConsultation) {
		this.medicalConsultation = medicalConsultation;
	}

	public Long getPrescriptionStatus() {
		return prescriptionStatus;
	}

	public void setPrescriptionStatus(Long prescriptionStatus) {
		this.prescriptionStatus = prescriptionStatus;
	}

}
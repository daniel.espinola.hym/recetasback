package com.unlam.recetasapp.model;

import com.unlam.recetasapp.entity.WaitingRoom;

public class WaitingRoomResponsePackage {
	private WaitingRoom waitingRoom;
	
	private String patientFullName;
	
	public WaitingRoomResponsePackage() {
		super();
	}
	
	public WaitingRoomResponsePackage(WaitingRoom waitingRoom, String patientFullName) {
		super();
		this.waitingRoom = waitingRoom;
		this.patientFullName = patientFullName;
	}

	public WaitingRoom getWaitingRoom() {
		return waitingRoom;
	}

	public void setWaitingRoom(WaitingRoom waitingRoom) {
		this.waitingRoom = waitingRoom;
	}

	public String getPatientFullName() {
		return patientFullName;
	}

	public void setPatientFullName(String patientFullName) {
		this.patientFullName = patientFullName;
	}
	
	
}

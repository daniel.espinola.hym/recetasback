package com.unlam.recetasapp.model;

import java.util.List;

public class PrescriptionView {

	
	private List<DrugView> drugList;
	
	private List<Long> diagnosisList;

	public List<DrugView> getDrugList() {
		return drugList;
	}

	public void setDrugList(List<DrugView> drugList) {
		this.drugList = drugList;
	}

	public List<Long> getDiagnosisList() {
		return diagnosisList;
	}

	public void setDiagnosisList(List<Long> diagnosisList) {
		this.diagnosisList = diagnosisList;
	}

	
}

package com.unlam.recetasapp.model;

import java.util.Date;

public class PatientPostPackage {
	private String name;
	private String surname;
	private Integer dni;
	private String birthDate;
	private Integer dniType;
	private Date insertDate;
	private String password;
	
	PatientPostPackage(Integer dni, Integer dniType, String password, String name, String surname, String birthDate){
		this.dni = dni;
		this.dniType = dniType;
		this.name = name;
		this.surname = surname;
		this.birthDate = birthDate;
		this.insertDate = new Date();
		this.password = password;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getBirthDate() {
		return birthDate;
	}
	
	public Date getInsertDate() {
		return this.insertDate;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surename) {
		this.surname = surename;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}
	
	public Integer getDniType() {
		return dniType;
	}

	public void setDniType(Integer dniType) {
		this.dniType = dniType;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}

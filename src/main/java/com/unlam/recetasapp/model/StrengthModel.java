package com.unlam.recetasapp.model;

public class StrengthModel {

	private Long id;
	private Integer number;
	private String unity;
	private String description;

	public StrengthModel(Long id, Integer number, String unity) {
		super();
		this.id = id;
		this.number = number;
		this.unity = unity;
		this.description = number + "" + unity;
	}

	public StrengthModel(String description) {
		super();
		this.description = description;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public String getUnity() {
		return unity;
	}

	public void setUnity(String unity) {
		this.unity = unity;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}

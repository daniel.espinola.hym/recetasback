package com.unlam.recetasapp.model;

import com.unlam.recetasapp.entity.Pharmacy;

public class PharmacyModel {
	private Long id;
	private String name;
	private String address;
	private Integer addressNumber;
	private Long localityId;
	private String localityName;
	private Long provinceId;
	private String provinceName;
	
	public PharmacyModel(Pharmacy pharmacy) {
		this.id = pharmacy.getId();
		this.name = pharmacy.getName();
		this.address = pharmacy.getAddress();
		this.addressNumber = pharmacy.getAddressNumber();
		this.localityId = pharmacy.getLocality().getId();
		this.localityName = pharmacy.getLocality().getDescription();
		this.provinceId = pharmacy.getLocality().getProvince().getId();
		this.provinceName = pharmacy.getLocality().getProvince().getDescription();
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	public Integer getAddressNumber() {
		return addressNumber;
	}
	
	public void setAddressNumber(Integer addressNumber) {
		this.addressNumber = addressNumber;
	}
	
	public Long getLocalityId() {
		return localityId;
	}
	
	public void setLocalityId(Long localityId) {
		this.localityId = localityId;
	}
	
	public String getLocalityName() {
		return localityName;
	}

	public void setLocalityName(String localityName) {
		this.localityName = localityName;
	}
	
	public Long getProvinceId() {
		return provinceId;
	}
	
	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}
	
	public String getProvinceName() {
		return provinceName;
	}

	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
}

package com.unlam.recetasapp.model;

import java.sql.Date;
import java.text.SimpleDateFormat;

import com.unlam.recetasapp.entity.PharmacyPrescription;

public class PrescriptionPharmacyPreviewModel {

	private Long prescriptionId;
	private String insertDate;
	private String pharmacistFullname;
	private Integer pharmacistDni;
	private String patientFullname;
	private Integer patientDni;


	public PrescriptionPharmacyPreviewModel(PharmacyPrescription pp) {
		this.prescriptionId = pp.getPrescription().getId();
		this.insertDate = new SimpleDateFormat("dd/MM/yyyy").format(pp.getInsertdate());
		this.pharmacistFullname = pp.getPharmacist().getName() + " " + pp.getPharmacist().getSurname();
		this.pharmacistDni = pp.getPharmacist().getDni();
		this.patientFullname = "";
		this.patientDni = 0;
	}
	
	public Long getPrescriptionId() {
		return prescriptionId;
	}

	public void setPrescriptionId(Long prescriptionId) {
		this.prescriptionId = prescriptionId;
	}
	
	public String getInsertDate() {
		return insertDate;
	}

	public void setInsertDate(Date date) {
		this.insertDate = new SimpleDateFormat("dd/MM/yyyy").format(date);
	}
	
	public String getPharmacistFullname() {
		return pharmacistFullname;
	}

	public void setPharmacistFullname(String fullname) {
		this.pharmacistFullname = fullname;
	}
	
	public Integer getPharmacistDni() {
		return pharmacistDni;
	}

	public void setPharmacistDni(Integer dni) {
		this.pharmacistDni = dni;
	}
	
	public String getPatientFullname() {
		return patientFullname;
	}

	public void setPatientFullname(String fullname) {
		this.patientFullname = fullname;
	}
	
	public Integer getPatientDni() {
		return patientDni;
	}

	public void setPatientDni(Integer dni) {
		this.patientDni = dni;
	}

}
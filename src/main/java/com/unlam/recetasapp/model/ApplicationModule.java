package com.unlam.recetasapp.model;

public enum ApplicationModule {
	PHARMACY, PATIENT, DOCTOR
}

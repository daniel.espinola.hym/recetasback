package com.unlam.recetasapp.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChronicAssignmentModel {
	private Long hospitalId;
	private Long doctorId;
	private Long patientChronicDiseaseId;
	private int quantity;
	private String starterDate;
	private PrescriptionView prescription;
	private int triggeredQuantity;
	private String frequency;

	// table
	private String doctorName;
	private String diagnosis;
	private String hospitalName;
	private Long prescriptionId;
	private Long id;

	public ChronicAssignmentModel() {

	}

	public Long getHospitalId() {
		return hospitalId;
	}

	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public Long getPatientChronicDiseaseId() {
		return patientChronicDiseaseId;
	}

	public void setPatientChronicDiseaseId(Long patientChronicDiseaseId) {
		this.patientChronicDiseaseId = patientChronicDiseaseId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getStarterDate() {
		return starterDate;
	}

	public void setStarterDate(String starterDate) {
		this.starterDate = starterDate;
	}

	public Date getStartedDateParsed() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Date dateParsed = null;
		try {
			dateParsed = sdf.parse(this.starterDate);
		} catch (ParseException e) {
			dateParsed = new Date();
		}
		return dateParsed;
	}

	public PrescriptionView getPrescription() {
		return prescription;
	}

	public void setPrescription(PrescriptionView prescription) {
		this.prescription = prescription;
	}

	public int getTriggeredQuantity() {
		return triggeredQuantity;
	}

	public void setTriggeredQuantity(int triggeredQuantity) {
		this.triggeredQuantity = triggeredQuantity;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public Long getPrescriptionId() {
		return prescriptionId;
	}

	public void setPrescriptionId(Long prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	public String getDoctorName() {
		return doctorName;
	}

	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}

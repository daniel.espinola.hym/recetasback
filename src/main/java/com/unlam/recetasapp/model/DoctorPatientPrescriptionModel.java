package com.unlam.recetasapp.model;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.unlam.recetasapp.entity.PrescriptionItemDiagnosis;
import com.unlam.recetasapp.entity.PrescriptionItemDrugComercial;
import com.unlam.recetasapp.entity.PrescriptionItemDrugGeneric;

public class DoctorPatientPrescriptionModel {
	private Long medicalConsultationId;
	private String medicalConsultationDate;
	private List<DiagnosisModel> diagnosisList;
	private List<DrugModel> drugList;

	public DoctorPatientPrescriptionModel() {
	}

	public DoctorPatientPrescriptionModel(Long medicalConsultationId,
			List<PrescriptionItemDiagnosis> prescriptionItemDiagnosisList,
			List<PrescriptionItemDrugGeneric> prescriptionItemDrugGenericList,
			List<PrescriptionItemDrugComercial> prescriptionItemDrugComercialList, Date medicalConsultationDate) {
		this.diagnosisList = new ArrayList<DiagnosisModel>();
		this.drugList = new ArrayList<DrugModel>();
		System.out.println("MCID: " + medicalConsultationId);
		System.out.println("Empezamos con los diagnosticos");
		prescriptionItemDiagnosisList.stream().forEach(item -> {

			System.out.println("d: " + item.getId() + " name: " + item.getDiagnosis().getDescription());
			DiagnosisModel diagnosisModel = new DiagnosisModel();
			diagnosisModel.setName(item.getDiagnosis().getDescription());
			this.diagnosisList.add(diagnosisModel);
		});
		System.out.println("******************");
		System.out.println("Empezamos con las genericas");
		prescriptionItemDrugGenericList.stream().forEach(item -> {

			System.out.println("d: " + item.getId() + " name: " + item.getDrugGeneric().getName());
			DrugModel drugModel = new DrugModel();
			drugModel.setGeneric(true);
			drugModel.setName(item.getDrugGeneric().getName());
			drugModel.setPharmaceuticalForm(item.getPharmaceuticalForm().getDescription());
			drugModel.setStrength(item.getDrugGeneric().getStrength().getNumber() + ""
					+ item.getDrugGeneric().getStrength().getUnityId().getDescription());
			drugModel.setTreatment(item.getTreatment());
			this.drugList.add(drugModel);
		});
		System.out.println("******************");
		System.out.println("Empezamos con las comerciales");
		prescriptionItemDrugComercialList.stream().forEach(item -> {

			System.out.println("d: " + item.getId() + " name: " + item.getDrugComercial().getName());
			DrugModel drugModel = new DrugModel();
			drugModel.setGeneric(false);
			drugModel.setName(item.getDrugComercial().getName());
			drugModel.setPharmaceuticalForm(item.getDrugComercial().getPharmaceuticalForm().getDescription());
			drugModel.setTreatment(item.getTreatment());
			this.drugList.add(drugModel);
		});
		System.out.println("******************");
		this.medicalConsultationId = medicalConsultationId;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		this.medicalConsultationDate = sdf.format(medicalConsultationDate);
	}

	public Long getMedicalConsultationId() {
		return medicalConsultationId;
	}

	public void setMedicalConsultationId(Long medicalConsultationId) {
		this.medicalConsultationId = medicalConsultationId;
	}

	public List<DiagnosisModel> getDiagnosisList() {
		return diagnosisList;
	}

	public void setDiagnosisList(List<DiagnosisModel> diagnosisList) {
		this.diagnosisList = diagnosisList;
	}

	public List<DrugModel> getDrugList() {
		return drugList;
	}

	public void setDrugList(List<DrugModel> drugList) {
		this.drugList = drugList;
	}

	public String getMedicalConsultationDate() {
		return medicalConsultationDate;
	}

	public void setMedicalConsultationDate(String medicalConsultationDate) {
		this.medicalConsultationDate = medicalConsultationDate;
	}

}

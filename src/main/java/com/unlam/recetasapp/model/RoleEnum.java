package com.unlam.recetasapp.model;

public enum RoleEnum {
	ROLE_PHARMACIST, ROLE_DOCTOR, ROLE_PATIENT, ROLE_HOSPITAL_ADMIN, ROLE_GLOBAL_ADMIN
}

package com.unlam.recetasapp.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unlam.recetasapp.entity.DrugGeneric;
import com.unlam.recetasapp.entity.Strength;

public class DrugGenericModel {

	@JsonInclude(Include.NON_NULL)
	private Long drugId;

	private String description;

	@JsonInclude(Include.NON_NULL)
	private String strength;

	@JsonInclude(Include.NON_NULL)
	private String idString;

	@JsonInclude(Include.NON_NULL)
	private String treatment;

	@JsonInclude(Include.NON_NULL)
	private Long treatmentDays;

	@JsonInclude(Include.NON_NULL)
	private String pharmaceuticalForm;

	public DrugGenericModel(Long drugId, String description, Strength strength) {
		super();
		this.drugId = drugId;
		if (drugId != null) {
			this.idString = "G" + drugId;
		}
		this.description = description;
		if (strength != null) {
			this.strength = strength.getNumber() + strength.getUnityId().getDescription();
		}
	}

	public DrugGenericModel(DrugGeneric drugGeneric) {
		drugId = drugGeneric.getId();
		idString = "G" + drugGeneric.getId();
		description = drugGeneric.getName();
		strength = drugGeneric.getStrength().getNumber() + drugGeneric.getStrength().getUnityId().getDescription();
	}

	public DrugGenericModel(Long drugId, String description, String strength, String idString, String treatment,
			String pharmaceuticalForm, Long treatmentDays) {
		super();
		if (drugId != null) {
			this.drugId = drugId;
		}
		if (description != null) {
			this.description = description;
		}
		if (strength != null) {
			this.strength = strength;
		}
		if (drugId != null) {
			this.idString = idString;
		}
		if (treatment != null) {
			this.treatment = treatment;
		}
		if (pharmaceuticalForm != null) {
			this.pharmaceuticalForm = pharmaceuticalForm;
		}	
		if (treatmentDays != null) {
			this.treatmentDays = treatmentDays;
		}
	}

	public String getTreatment() {
		return treatment;
	}

	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}

	public String getPharmaceuticalForm() {
		return pharmaceuticalForm;
	}

	public void setPharmaceuticalForm(String pharmaceuticalForm) {
		this.pharmaceuticalForm = pharmaceuticalForm;
	}

	public Long getDrugId() {
		return drugId;
	}

	public void setDrugId(Long drugId) {
		this.drugId = drugId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getIdString() {
		return idString;
	}

	public void setIdString(String idString) {
		this.idString = idString;
	}

	public Long getTreatmentDays() {
		return treatmentDays;
	}

	public void setTreatmentDays(Long treatmentDays) {
		this.treatmentDays = treatmentDays;
	}

}

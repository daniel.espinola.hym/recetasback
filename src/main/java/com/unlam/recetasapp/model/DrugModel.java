package com.unlam.recetasapp.model;

import java.util.List;

public class DrugModel {
	private String id;
	private Long drugId;
	private String name;
	private String strength;
	private String pharmaceuticalForm;
	private String treatment;
	private Long days;
	private boolean isGeneric;
	private boolean isComercial;
	private String description;
	private List<DrugModel> composition;

	public DrugModel() {
	}

	public DrugModel(Long drugId, String strength, String pharmaceuticalForm, boolean isGeneric) {
		String prefix = "";
		if (isGeneric) {
			prefix = "G";
		} else {
			prefix = "C";
		}
		this.id = prefix + drugId;
		this.drugId = drugId;
		this.strength = strength;
		this.pharmaceuticalForm = pharmaceuticalForm;
	}

	public Long getDrugId() {
		return drugId;
	}

	public void setDrugId(Long drugId) {
		this.drugId = drugId;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPharmaceuticalForm() {
		return pharmaceuticalForm;
	}

	public void setPharmaceuticalForm(String pharmaceuticalForm) {
		this.pharmaceuticalForm = pharmaceuticalForm;
	}

	public String getTreatment() {
		return treatment;
	}

	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}

	public boolean isGeneric() {
		return isGeneric;
	}

	public void setGeneric(boolean isGeneric) {
		this.isGeneric = isGeneric;
	}

	public Long getDays() {
		return days;
	}

	public void setDays(Long days) {
		this.days = days;
	}

	public boolean isComercial() {
		return isComercial;
	}

	public void setComercial(boolean isComercial) {
		this.isComercial = isComercial;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<DrugModel> getComposition() {
		return composition;
	}

	public void setComposition(List<DrugModel> composition) {
		this.composition = composition;
	}

}

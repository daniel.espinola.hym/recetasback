package com.unlam.recetasapp.model;

import java.util.List;

public class DrugGenericVarietyPackage {
	
	private List<StrengthModel> strengthList;
	
	private List<PharmaceuticalFormModel> pharmaceuticalFormList;

	public List<StrengthModel> getStrengthList() {
		return strengthList;
	}

	public void setStrengthList(List<StrengthModel> strengthList) {
		this.strengthList = strengthList;
	}

	public List<PharmaceuticalFormModel> getPharmaceuticalFormList() {
		return pharmaceuticalFormList;
	}

	public void setPharmaceuticalFormList(List<PharmaceuticalFormModel> pharmaceuticalFormList) {
		this.pharmaceuticalFormList = pharmaceuticalFormList;
	}

	public DrugGenericVarietyPackage(List<StrengthModel> strengthList,
			List<PharmaceuticalFormModel> pharmaceuticalFormList) {
		super();
		this.strengthList = strengthList;
		this.pharmaceuticalFormList = pharmaceuticalFormList;
	}
	
	

}

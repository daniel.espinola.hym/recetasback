package com.unlam.recetasapp.model;

import com.unlam.recetasapp.entity.Hospital;

public class HospitalModel {

	private Long idHospital;

	private String hospitalName;

	private String address;

	private String addressNumber;

	private String province;

	private String locality;
	private Long localityId;

	public HospitalModel() {

	}

	public HospitalModel(Hospital hospital) {
		this.idHospital = hospital.getId();
		this.hospitalName = hospital.getName();
		this.province = hospital.getLocality().getProvince().getDescription();
		this.locality = hospital.getLocality().getDescription();
		this.address = hospital.getAddress();
		this.addressNumber = hospital.getAddressNumber().toString();
		this.localityId = hospital.getLocality().getId();
	}

	public Long getIdHospital() {
		return idHospital;
	}

	public void setIdHospital(Long idHospital) {
		this.idHospital = idHospital;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getLocality() {
		return locality;
	}

	public void setLocality(String locality) {
		this.locality = locality;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getAddressNumber() {
		return addressNumber;
	}

	public void setAddressNumber(String addressNumber) {
		this.addressNumber = addressNumber;
	}

	public Long getLocalityId() {
		return localityId;
	}

	public void setLocalityId(Long localityId) {
		this.localityId = localityId;
	}

}

package com.unlam.recetasapp.model;

public class RequestFilePrescription {
	private String file;

	public RequestFilePrescription() {

	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

}

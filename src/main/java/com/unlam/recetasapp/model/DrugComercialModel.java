package com.unlam.recetasapp.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unlam.recetasapp.entity.DrugComercial;
import com.unlam.recetasapp.entity.DrugGeneric;

public class DrugComercialModel {

	@JsonInclude(Include.NON_NULL)
	private Long drugId;

	private String description;
	private List<DrugGenericModel> drugComponentList;
	private String pharmaceuticalForm;

	@JsonInclude(Include.NON_NULL)
	private String treatment;

	@JsonInclude(Include.NON_NULL)
	private Long treatmentDays;

	@JsonInclude(Include.NON_NULL)
	private String idString;

	public DrugComercialModel(Long drugId, String description, List<DrugGenericModel> drugComponentList,
			String pharmaceuticalForm) {
		super();
		this.drugId = drugId;
		this.description = description;
		this.drugComponentList = drugComponentList;
		this.pharmaceuticalForm = pharmaceuticalForm;
		this.idString = "C" + drugId;
	}

	public DrugComercialModel(Long drugId, DrugComercial drugComercial, List<DrugGeneric> drugComponents,
			String treatment, Long treatmentDays) {

		if (drugId != null) {
			this.drugId = drugComercial.getId();
		}
		description = drugComercial.getName();
		pharmaceuticalForm = drugComercial.getPharmaceuticalForm().getDescription();

		drugComponentList = new ArrayList<DrugGenericModel>();

		for (DrugGeneric drugComponent : drugComponents) {
			DrugGenericModel dgm = new DrugGenericModel(null, drugComponent.getName(), drugComponent.getStrength());
			if(dgm.getStrength()==null) {
				dgm.setStrength(drugComponent.getStrengthString());
			}
			drugComponentList.add(dgm);
		}
		if (drugId != null) {
			this.idString = "C" + drugComercial.getId();
		}

		if (treatment != null) {
			this.treatment = treatment;
		}
		
		if (treatmentDays != null) {
			this.treatmentDays = treatmentDays;
		}
	}

	public Long getDrugId() {
		return drugId;
	}

	public void setDrugId(Long drugId) {
		this.drugId = drugId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public List<DrugGenericModel> getDrugComponentList() {
		return drugComponentList;
	}

	public void setDrugComponentList(List<DrugGenericModel> drugComponentList) {
		this.drugComponentList = drugComponentList;
	}

	public String getPharmaceuticalForm() {
		return pharmaceuticalForm;
	}

	public void setPharmaceuticalForm(String pharmaceuticalForm) {
		this.pharmaceuticalForm = pharmaceuticalForm;
	}

	public String getIdString() {
		return idString;
	}

	public void setIdString(String idString) {
		this.idString = idString;
	}

	public String getTreatment() {
		return treatment;
	}

	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}

	public Long getTreatmentDays() {
		return treatmentDays;
	}

	public void setTreatmentDays(Long treatmentDays) {
		this.treatmentDays = treatmentDays;
	}

}

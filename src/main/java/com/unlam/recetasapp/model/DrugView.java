package com.unlam.recetasapp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class DrugView {

	@JsonProperty("days")
	private Long treatmentDays;

	private String id;

	private String name;

	@JsonProperty("pharmaceuticalForm")
	private Long pharmaceuticalFormId;

	@JsonProperty("strength")
	private Long strengthId;

	private String treatment;

	public Long getTreatmentDays() {
		return treatmentDays;
	}

	public void setTreatmentDays(Long treatmentDays) {
		this.treatmentDays = treatmentDays;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getPharmaceuticalFormId() {
		return pharmaceuticalFormId;
	}

	public void setPharmaceuticalFormId(Long pharmaceuticalFormId) {
		this.pharmaceuticalFormId = pharmaceuticalFormId;
	}

	public Long getStrengthId() {
		return strengthId;
	}

	public void setStrengthId(Long strengthId) {
		this.strengthId = strengthId;
	}

	public String getTreatment() {
		return treatment;
	}

	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}

}

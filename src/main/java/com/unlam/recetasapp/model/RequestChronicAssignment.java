package com.unlam.recetasapp.model;

public class RequestChronicAssignment {
	private Long hospitalId;
	private Long doctorId;
	private Long patientChronicDiseaseId;

	public RequestChronicAssignment() {

	}

	public Long getHospitalId() {
		return hospitalId;
	}

	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public Long getPatientChronicDiseaseId() {
		return patientChronicDiseaseId;
	}

	public void setPatientChronicDiseaseId(Long patientChronicDiseaseId) {
		this.patientChronicDiseaseId = patientChronicDiseaseId;
	}

}

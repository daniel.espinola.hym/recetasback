package com.unlam.recetasapp.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.unlam.recetasapp.entity.DrugComercial;
import com.unlam.recetasapp.entity.DrugComercialCompose;
import com.unlam.recetasapp.entity.DrugGeneric;
import com.unlam.recetasapp.entity.MedicalConsultation;
import com.unlam.recetasapp.entity.Prescription;
import com.unlam.recetasapp.entity.PrescriptionItem;
import com.unlam.recetasapp.entity.PrescriptionItemDiagnosis;
import com.unlam.recetasapp.entity.PrescriptionItemDrugComercial;
import com.unlam.recetasapp.entity.PrescriptionItemDrugGeneric;

public class PrescriptionDetailModel {

	@JsonProperty("id")
	private Long prescriptionId;
	
	@JsonProperty("drugList")
	List<DrugGenericModel> drugGenericList = new ArrayList<DrugGenericModel>();
	
	@JsonProperty("drugComercialList")
	List<DrugComercialModel> drugComercialList = new ArrayList<DrugComercialModel>();
	
	private List<String> diagnosisList = new ArrayList<String>();
	private MedicalConsultationPreview medicalConsultation;
	
	private Boolean delayedSign;
	
	
	public PrescriptionDetailModel(Long prescriptionId, List<String> diagnosisList,
			MedicalConsultationPreview medicalConsultation) {
		super();
		this.prescriptionId = prescriptionId;
		this.diagnosisList = diagnosisList;
		this.medicalConsultation = medicalConsultation;
	}
	
	public PrescriptionDetailModel() {
		
	}

	public PrescriptionDetailModel(Prescription prescription) {
		this.prescriptionId = prescription.getId();
		
		List<PrescriptionItem> prescriptionItems = prescription.getPrescriptionItem();
		
		for(PrescriptionItem pItem : prescriptionItems) {
			List<PrescriptionItemDiagnosis> pIDiagnosisList = pItem.getPrescriptionItemDiagnosis();
			for(PrescriptionItemDiagnosis pIDiagnosis : pIDiagnosisList) {
				diagnosisList.add(pIDiagnosis.getDiagnosis().getDescription());
			}
			
			List<PrescriptionItemDrugGeneric> pItemDrugGenerics = pItem.getPrescriptionItemDrugGeneric();
			for(PrescriptionItemDrugGeneric pItemDrugGeneric : pItemDrugGenerics) {
				drugGenericList.add(new DrugGenericModel(null, pItemDrugGeneric.getDrugGeneric().getName(), pItemDrugGeneric.getDrugGeneric().getStrength().getNumber() + pItemDrugGeneric.getDrugGeneric().getStrength().getUnityId().getDescription(), null, pItemDrugGeneric.getTreatment(), pItemDrugGeneric.getPharmaceuticalForm().getDescription(), pItemDrugGeneric.getTreatmentDays()));
			}
			
			List<PrescriptionItemDrugComercial> pItemDrugComercials = pItem.getPrescriptionItemDrugComercial();
			for(PrescriptionItemDrugComercial pItemDrugComercial : pItemDrugComercials) {
				drugComercialList.add(new DrugComercialModel(null, pItemDrugComercial.getDrugComercial(),getDrugComponents(pItemDrugComercial.getDrugComercial()), pItemDrugComercial.getTreatment() , pItemDrugComercial.getTreatmentDays()));
			}
		}
		
		MedicalConsultation medicalConsultation = prescription.getMedicalConsultation();
		
		PatientModel patient = new PatientModel(medicalConsultation.getPatient());
		
		this.medicalConsultation = new MedicalConsultationPreview(medicalConsultation.getDoctor().getName() + " " + medicalConsultation.getDoctor().getSurname(), medicalConsultation.getConsultationDate(), medicalConsultation.getHospital().getName(), "5487457" , patient.getFullName(), patient.getDni());
		
//		Date date1 = myFormat.parse(inputString1);
//	    Date date2 = myFormat.parse(inputString2);
		
		if(prescription.getPrescriptionStatus().getId().equals(7L)) {
			long diff = new Date().getTime() - medicalConsultation.getConsultationDate().getTime();
			long daysDiff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
			
			System.out.println ("Days: " + TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
			if(daysDiff >= 5) {
				delayedSign = true;
			}else {
				delayedSign = false;
			}
			
		}
		
		
	}
	
	private List<DrugGeneric> getDrugComponents(DrugComercial drugComercial) {
		List<DrugComercialCompose> drugComercialComposeList = drugComercial.getDrugComercialCompose();
		List<DrugGeneric> drugComponents = new ArrayList<DrugGeneric>();
				
		for(DrugComercialCompose drugComercialCompose : drugComercialComposeList) {
			drugComponents.add(drugComercialCompose.getDrugGeneric());
		}

		return drugComponents;
	}

	public Long getPrescriptionId() {
		return prescriptionId;
	}
	public void setPrescriptionId(Long prescriptionId) {
		this.prescriptionId = prescriptionId;
	}
	public List<String> getDiagnosisList() {
		return diagnosisList;
	}
	public void setDiagnosisList(List<String> diagnosisList) {
		this.diagnosisList = diagnosisList;
	}
	public MedicalConsultationPreview getMedicalConsultation() {
		return medicalConsultation;
	}
	public void setMedicalConsultation(MedicalConsultationPreview medicalConsultation) {
		this.medicalConsultation = medicalConsultation;
	}

	public Boolean getDelayedSign() {
		return delayedSign;
	}

	public void setDelayedSign(Boolean delayedSign) {
		this.delayedSign = delayedSign;
	}
	
	
	
	
}

package com.unlam.recetasapp.model;

public enum TokenStatus {
	SIN_UTILIZAR, UTILIZADO, EXPIRADO_POR_TIEMPO, EXPIRADO_POR_NUEVO_TOKEN
}

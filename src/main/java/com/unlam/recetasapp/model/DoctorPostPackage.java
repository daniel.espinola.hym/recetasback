package com.unlam.recetasapp.model;

import java.util.Date;

public class DoctorPostPackage {
	private String name;
	private String surename;
	private Integer dni;
	private String birthDate;
	private Date insertDate;
	private Integer dniType;
	private String password;
	private Long hospitalId;
	private Integer licenseNumber;
	
	DoctorPostPackage(Integer dni, Integer dniType, String password, String name, 
			String surename, String birthDate, Long hospitalId, Integer licenseNumber){
		this.dni = dni;
		this.dniType = dniType;
		this.name = name;
		this.surename = surename;
		this.birthDate = birthDate;
		this.insertDate = new Date();
		this.password = password;
		this.hospitalId = hospitalId;
		this.licenseNumber = licenseNumber;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}
	
	public Date getInsertDate() {
		return this.insertDate;
	}

	public String getSurename() {
		return surename;
	}

	public void setSurename(String surename) {
		this.surename = surename;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}
	
	public Integer getDniType() {
		return dniType;
	}

	public void setDniType(Integer dniType) {
		this.dniType = dniType;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	public Integer getLicenseNumber() {
		return licenseNumber;
	}

	public void setLicenseNumber(Integer licenseNumber) {
		this.licenseNumber = licenseNumber;
	}
	
	public Long getHospitalId() {
		return hospitalId;
	}

	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}
}

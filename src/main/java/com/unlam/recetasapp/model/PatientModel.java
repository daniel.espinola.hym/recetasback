package com.unlam.recetasapp.model;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

import com.unlam.recetasapp.entity.Patient;

public class PatientModel {
	private Long id;
	
	private String fullName;
	private Integer age;
	private Integer dni;
	private String name;
	private String surname;
	private String birthDate;
	private Integer dniType;
	private boolean isDoctor;
	private boolean isPharmacist;
	private boolean isEnabled;
	
	
	public PatientModel() {
		super();
	}

	public PatientModel(String fullName, Integer age, Integer dni) {
		super();
		this.fullName = fullName;
		this.age = age;
		this.dni = dni;
		this.isDoctor = false;
		this.isPharmacist = false;
		this.isEnabled = false;
	}
	
	public PatientModel(Patient patient) {
		super();
		this.fullName = patient.getName() + ' ' + patient.getSurname();
		Period diff = Period.between(convertToLocalDateViaInstant(patient.getBirthdate()), LocalDate.now());
		this.age = diff.getYears();
		this.dni = patient.getDni();
		this.id = patient.getId();
		this.birthDate = new SimpleDateFormat("dd/MM/yyyy").format(patient.getBirthdate());
		this.isDoctor = false;
		this.isPharmacist = false;
		this.isEnabled = false;
	}
	
	private LocalDate convertToLocalDateViaInstant(Date dateToConvert) {
	    return dateToConvert.toInstant()
	      .atZone(ZoneId.systemDefault())
	      .toLocalDate();
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}
	
	public Integer getDniType() {
		return dniType;
	}

	public void setDniType(Integer dniType) {
		this.dniType = dniType;
	}
	
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getBirthDate() {
		return birthDate;
	}
	
	public boolean getIsDoctor() {
		return isDoctor;
	}

	public void setIsDoctor(boolean isDoctor) {
		this.isDoctor = isDoctor;
	}
	
	public boolean getIsPharmacist() {
		return isPharmacist;
	}

	public void setIsPharmacist(boolean isPharmacist) {
		this.isPharmacist = isPharmacist;
	}
	
	public boolean getIsEnabled() {
		return isEnabled;
	}

	public void setIsEnabled(boolean isEnabled) {
		this.isEnabled = isEnabled;
	}
}

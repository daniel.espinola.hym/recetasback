package com.unlam.recetasapp.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.unlam.recetasapp.entity.MedicalConsultation;

public class MedicalConsultationPreview {

	private String doctorName;
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private Date date;
	
	private String hospital;
	
	@JsonInclude(Include.NON_NULL)
	private String license;
	
	private String patientName;
	
	private Integer patientDni;
	
	public MedicalConsultationPreview(String doctorName, Date date, String hospital) {
		super();
		this.doctorName = doctorName;
		this.date = date;
		this.hospital = hospital;
	}
	
	public MedicalConsultationPreview(String doctorName, Date date, String hospital, String license, String patientName, Integer patientDni) {
		super();
		if(doctorName != null) {
			this.doctorName = doctorName;			
		}
		if(date != null) {
			this.date = date;
		}
		if(hospital != null) {
			this.hospital = hospital;
		}
		if(license != null) {
			this.license = license;
		}
		if(patientName != null) {
			this.patientName = patientName;
		}
		if(patientDni != null) {
			this.patientDni = patientDni;
		}
	}



	public MedicalConsultationPreview(MedicalConsultation medicalConsultation) {
		this.doctorName = medicalConsultation.getDoctor().getName() + " " + medicalConsultation.getDoctor().getSurname();
		this.date = medicalConsultation.getConsultationDate();
		this.hospital = medicalConsultation.getHospital().getName();
	}

	public String getDoctorName() {
		return doctorName;
	}
	public void setDoctorName(String doctorName) {
		this.doctorName = doctorName;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getHospital() {
		return hospital;
	}
	public void setHospital(String hospital) {
		this.hospital = hospital;
	}

	public String getLicense() {
		return license;
	}

	public void setLicense(String license) {
		this.license = license;
	}
	
	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public Integer getPatientDni() {
		return patientDni;
	}

	public void setPatientDni(Integer patientDni) {
		this.patientDni = patientDni;
	}
	
}

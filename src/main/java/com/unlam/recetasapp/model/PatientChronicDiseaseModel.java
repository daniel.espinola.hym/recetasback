package com.unlam.recetasapp.model;

public class PatientChronicDiseaseModel {
	private Long patientId;
	private Long chronicDiagnosisId;
	private Long doctorId;
	private Long hospitalId;

	public PatientChronicDiseaseModel() {

	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public Long getChronicDiagnosisId() {
		return chronicDiagnosisId;
	}

	public void setChronicDiagnosisId(Long chronicDiagnosisId) {
		this.chronicDiagnosisId = chronicDiagnosisId;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public Long getHospitalId() {
		return hospitalId;
	}

	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}

}

package com.unlam.recetasapp.model;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.unlam.recetasapp.entity.Pharmacist;
import com.unlam.recetasapp.entity.Pharmacy;

public class PharmacistModel {
	private Integer dni;
	private Integer dniType;
	private String name;
	private String surname;
	private String birthDate;
	private Long pharmacyId;
	private String pharmacyName;
	private String pharmacyLocation;
	private boolean enabledStatus = true;
	
	public PharmacistModel() {
		
	}
	
	public PharmacistModel(Pharmacist pharmacist) {
		this.dni = pharmacist.getDni();
		this.dniType = pharmacist.getDnitype();
		this.name = pharmacist.getName();
		this.surname = pharmacist.getSurname();
		this.birthDate = new SimpleDateFormat("dd/MM/yyyy").format(pharmacist.getBirthdate());
	}
	
	public PharmacistModel(Pharmacist pharmacist, Pharmacy pharmacy) {
		this.dni = pharmacist.getDni();
		this.dniType = pharmacist.getDnitype();
		this.name = pharmacist.getName();
		this.surname = pharmacist.getSurname();
		this.birthDate = new SimpleDateFormat("dd/MM/yyyy").format(pharmacist.getBirthdate());
		this.setPharmacyData(pharmacy);
	}
	
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}
	
	public Integer getDniType() {
		return dniType;
	}

	public void setDniType(Integer dniType) {
		this.dniType = dniType;
	}
	
	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getBirthDate() {
		return birthDate;
	}
	
	public Long getPharmacyId() {
		return pharmacyId;
	}
	
	public String getPharmacyName() {
		return pharmacyName;
	}
	
	public String getPharmacyLocation() {
		return pharmacyLocation;
	}
	
	public void setPharmacyData(Pharmacy pharmacy) {
		this.pharmacyId = pharmacy.getId();
		this.pharmacyName = pharmacy.getName();
		
		String locationName = pharmacy.getLocality().getDescription() + " (" + pharmacy.getLocality().getProvince().getDescription() + ")";
		this.pharmacyLocation = locationName;
	}
	
	public boolean getEnabledStatus() {
		return enabledStatus;
	}

	public void setEnabledStatus(boolean enabledStatus) {
		this.enabledStatus = enabledStatus;
	}
}

package com.unlam.recetasapp.model;

public class WaitingRoomPostPackage {

	private Long dni;
	private Long idHospital;
	
	public Long getDni() {
		return dni;
	}
	public void setDni(Long dni) {
		this.dni = dni;
	}
	public Long getIdHospital() {
		return idHospital;
	}
	public void setIdHospital(Long idHospital) {
		this.idHospital = idHospital;
	}
	
	
	
}

package com.unlam.recetasapp.model;

import java.util.List;

public class PrescriptionModel {
	private List<DiagnosisModel> diagnosisList;
	private List<DrugModel> drugList;
	private Long id;
	private String date;
	private String hospitalName;
	private String status;
	private String patientName;
	private Long medicalConsultationId;
	private Long patientId;
	private Long hospitalId;

	public PrescriptionModel() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<DiagnosisModel> getDiagnosisList() {
		return diagnosisList;
	}

	public void setDiagnosisList(List<DiagnosisModel> diagnosisList) {
		this.diagnosisList = diagnosisList;
	}

	public List<DrugModel> getDrugList() {
		return drugList;
	}

	public void setDrugList(List<DrugModel> drugList) {
		this.drugList = drugList;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getHospitalName() {
		return hospitalName;
	}

	public void setHospitalName(String hospitalName) {
		this.hospitalName = hospitalName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getPatientName() {
		return patientName;
	}

	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}

	public Long getMedicalConsultationId() {
		return medicalConsultationId;
	}

	public void setMedicalConsultationId(Long medicalConsultationId) {
		this.medicalConsultationId = medicalConsultationId;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public Long getHospitalId() {
		return hospitalId;
	}

	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}

}

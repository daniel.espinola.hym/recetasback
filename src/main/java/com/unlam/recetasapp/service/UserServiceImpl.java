package com.unlam.recetasapp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import com.unlam.recetasapp.entity.Doctor;
import com.unlam.recetasapp.entity.DoctorHospital;
import com.unlam.recetasapp.entity.Hospital;
import com.unlam.recetasapp.entity.HospitalAdmin;
import com.unlam.recetasapp.entity.HospitalAdminRelation;
import com.unlam.recetasapp.entity.User;
import com.unlam.recetasapp.model.HospitalModel;
import com.unlam.recetasapp.model.RoleEnum;
import com.unlam.recetasapp.repository.UserRepository;

@Service
public class UserServiceImpl implements UserService{
	
	@Autowired
	private UserRepository userRepository;

	@Override
	public List<HospitalModel> getDoctorHospitals(String dni) {
		
		User user = userRepository.findByUsername(dni);
		
		List<HospitalModel> listHospitalModels = new ArrayList<HospitalModel>();
		if(userHasRole(user, RoleEnum.ROLE_DOCTOR) && user.getDoctor() != null) {
			Doctor doctor = user.getDoctor();
			List<DoctorHospital> listHospitals = doctor.getDoctorHospital();
			for(DoctorHospital doctorHospital : listHospitals) {
				Hospital hospital = doctorHospital.getHospital();
				HospitalModel hospitalModel = new HospitalModel();
				hospitalModel.setHospitalName(hospital.getName());
				hospitalModel.setLocality(hospital.getLocality().getDescription());
				hospitalModel.setProvince(hospital.getLocality().getProvince().getDescription());
				hospitalModel.setIdHospital(hospital.getId());
				listHospitalModels.add(hospitalModel);
			}
		}else {
			//throw new Exception();
		}
		
		
		return listHospitalModels;
	}
	
	@Override
	public List<HospitalModel> getAdminHospitals(String dni) {
		
		User user = userRepository.findByUsername(dni);
		
		List<HospitalModel> listHospitalModels = new ArrayList<HospitalModel>();
		if(userHasRole(user, RoleEnum.ROLE_HOSPITAL_ADMIN) && user.getHospitalAdmin() != null) {
			HospitalAdmin hospitalAdmin = user.getHospitalAdmin();
			List<HospitalAdminRelation> listHospitals = hospitalAdmin.getAdminHospitalList();
			for(HospitalAdminRelation hospitalAdminRelation : listHospitals) {
				Hospital hospital = hospitalAdminRelation.getHospital();
				HospitalModel hospitalModel = new HospitalModel();
				hospitalModel.setHospitalName(hospital.getName());
				hospitalModel.setLocality(hospital.getLocality().getDescription());
				hospitalModel.setProvince(hospital.getLocality().getProvince().getDescription());
				hospitalModel.setIdHospital(hospital.getId());
				listHospitalModels.add(hospitalModel);
			}
		}else {
			//throw new Exception();
		}
		
		
		return listHospitalModels;
	}

	@Override
	public boolean userHasRole(User user, RoleEnum role) {
        JsonArray arr = new JsonParser().parse(user.getRole()).getAsJsonArray();
        for (int i = 0; i < arr.size(); i++) {
            if(arr.get(i).getAsString().equals(role.name())) {
            	return true;
            }
        }
		return false;
	}

}

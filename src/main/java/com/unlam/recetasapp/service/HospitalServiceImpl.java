package com.unlam.recetasapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unlam.recetasapp.entity.DoctorHospital;
import com.unlam.recetasapp.entity.Hospital;
import com.unlam.recetasapp.entity.Locality;
import com.unlam.recetasapp.entity.MedicalConsultation;
import com.unlam.recetasapp.entity.Pharmacy;
import com.unlam.recetasapp.model.HospitalModel;
import com.unlam.recetasapp.model.HospitalPostPackage;
import com.unlam.recetasapp.model.PharmacyModel;
import com.unlam.recetasapp.model.PharmacyPostPackage;
import com.unlam.recetasapp.repository.DoctorHospitalRepository;
import com.unlam.recetasapp.repository.HospitalRepository;
import com.unlam.recetasapp.repository.LocalityRepository;
import com.unlam.recetasapp.repository.MedicalConsultationRepository;
import com.unlam.recetasapp.repository.ProvinceRepository;

@Service
public class HospitalServiceImpl implements HospitalService {

	@Autowired
	HospitalRepository hospitalRepository;
	
	@Autowired
	MedicalConsultationRepository medicalConsultationRepository;
	
	@Autowired
	LocalityRepository localityRepository;
	
	@Autowired
	ProvinceRepository provinceRepository;
	
	@Autowired
	DoctorHospitalRepository doctorHospitalRepository;

	@Override
	public List<HospitalModel> getAllHospitals(){
		ArrayList<Hospital> hospitalList = new ArrayList<Hospital>(hospitalRepository.findAll());
		ArrayList<HospitalModel> hospitalModelList = new ArrayList<HospitalModel>();
		
		hospitalList.stream().forEach(hos -> {
			HospitalModel hm = new HospitalModel(hos);
			hospitalModelList.add(hm);
		});
		
		return hospitalModelList;
	}
	
	
	@Override
	public HospitalModel saveNewHospital(HospitalPostPackage newHospitalData){
		
		Hospital newHospitalEntity = new Hospital();
		newHospitalEntity.setName(newHospitalData.getName());
		newHospitalEntity.setAddress(newHospitalData.getAddress());
		newHospitalEntity.setAddressNumber(newHospitalData.getAddressNumber());
		
		Optional<Locality> returnedObject = localityRepository.findById(newHospitalData.getLocalityId());
		Locality loc = returnedObject.get();
		
		newHospitalEntity.setLocality(loc);
		hospitalRepository.save(newHospitalEntity);
		
		HospitalModel newHospitalModel = new HospitalModel(newHospitalEntity);
		return newHospitalModel;
	}
	
	@Override
	public HospitalModel updateHospital(HospitalPostPackage updatedHospitalData){
		Optional<Hospital> returnedObject = hospitalRepository.findById(updatedHospitalData.getId());
		
		Hospital h = returnedObject.get();
		
		Optional<Locality> returnedObject2 = localityRepository.findById(updatedHospitalData.getLocalityId());
		Locality loc = returnedObject2.get();
		
		h.setName(updatedHospitalData.getName());
		h.setAddress(updatedHospitalData.getAddress());
		h.setAddressNumber(updatedHospitalData.getAddressNumber());
		h.setLocality(loc);
		
		
		hospitalRepository.save(h);
		HospitalModel hm = new HospitalModel(h);
		return hm;

	}
	
	@Override
	public HospitalModel deleteHospital(HospitalPostPackage deletedHospitalData) {
		List<DoctorHospital> list_dh = doctorHospitalRepository.findByHospitalId(deletedHospitalData.getId());
		List<MedicalConsultation> list_mc = medicalConsultationRepository.findByHospitalId(deletedHospitalData.getId());
		
		if(!list_dh.isEmpty() || !list_mc.isEmpty()) {
			throw new IllegalArgumentException("El hospital tiene médicos asignados y/o consultas médicas asociadas, no se puede eliminar");
		}
		
		Optional<Hospital> returnedObject = hospitalRepository.findById(deletedHospitalData.getId());
		Hospital h = returnedObject.get();
		hospitalRepository.delete(h);
		
		HospitalModel deletedHospitalModel = new HospitalModel(h);
		return deletedHospitalModel;
	}

}

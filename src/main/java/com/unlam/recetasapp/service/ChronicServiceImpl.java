package com.unlam.recetasapp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unlam.recetasapp.entity.ChronicAssignment;
import com.unlam.recetasapp.entity.MedicalConsultation;
import com.unlam.recetasapp.entity.PatientChronicDisease;
import com.unlam.recetasapp.entity.Prescription;
import com.unlam.recetasapp.entity.PrescriptionItem;
import com.unlam.recetasapp.entity.PrescriptionItemDiagnosis;
import com.unlam.recetasapp.entity.PrescriptionItemDrugComercial;
import com.unlam.recetasapp.entity.PrescriptionItemDrugGeneric;
import com.unlam.recetasapp.entity.PrescriptionStatus;
import com.unlam.recetasapp.model.ChronicAssignmentModel;
import com.unlam.recetasapp.model.PatientChronicDiseaseModel;
import com.unlam.recetasapp.repository.ChronicAssignmentRepository;
import com.unlam.recetasapp.repository.ChronicDiseaseRepository;
import com.unlam.recetasapp.repository.DoctorRepository;
import com.unlam.recetasapp.repository.HospitalRepository;
import com.unlam.recetasapp.repository.MedicalConsultationRepository;
import com.unlam.recetasapp.repository.PatientChronicDiseaseRepository;
import com.unlam.recetasapp.repository.PatientRepository;

@Service
public class ChronicServiceImpl implements ChronicService {

	@Autowired
	private ChronicAssignmentRepository chronicAssignamentRepository;
	@Autowired
	private PatientChronicDiseaseRepository patientChronicDiseaseRepository;
	@Autowired
	private ChronicDiseaseRepository chronicDiseaseRepository;
	@Autowired
	private DoctorRepository doctorRepository;
	@Autowired
	private HospitalRepository hospitalRepository;
	@Autowired
	private PatientRepository patientRepository;
	@Autowired
	private MedicalConsultationRepository medicalConsultationRepository;
	@Autowired
	private PrescriptionService prescriptionService;

	@Override
	public PatientChronicDisease saveStatusChronic(PatientChronicDiseaseModel status) {
		return patientChronicDiseaseRepository.save(this.patientChronicDiseaseModelToEntity(status));
	}

	@Override
	public ChronicAssignment saveAssignment(ChronicAssignmentModel status) {
		ChronicAssignment entity = chronicAssignamentRepository.save(this.chronicAssignmentModelToEntity(status));
		
		List<Long> diagnosisList  = new ArrayList<Long>();
		diagnosisList.add(entity.getPatientChronicDiseaseId().getChronicDisease().getDiagnosis().getId());
		status.getPrescription().setDiagnosisList(diagnosisList);
		Prescription prescription = prescriptionService.generatePrescriptionWithPrescriptionItem(status.getPrescription());
		prescription.setPrescriptionStatus(new PrescriptionStatus(6L));
		prescription.setChronicAssignment(entity);
		
		prescriptionService.savePrescription(prescription);
		
		return entity;
	}
	
	@Override
	public List<PatientChronicDisease> getChronicDiseaseListByPatientId(Long patientId){
		return patientChronicDiseaseRepository.findByPatientId(patientId);
	}
	
	@Override
	public List<PatientChronicDisease> getChronicDiseaseListByDoctorIdAndHospitalId(Long doctorId, Long hospitalId){
		return patientChronicDiseaseRepository.findByDoctorIdAndHospitalId(doctorId, hospitalId);
	}
	
	@Override
	public boolean hasChronicAssignment(Long patientChronicDiseaseId){
		List<ChronicAssignment> resultList = chronicAssignamentRepository.findChronicAssignmentByPatientChronicDiseaseIdId(patientChronicDiseaseId);
		return resultList!=null && resultList.size()>0;
	}
	
	@Override
	public List<ChronicAssignmentModel> getChronicAssignmentListByPatientId(Long patientId){
		List<ChronicAssignmentModel> resultList = new ArrayList<>();
		
		List<ChronicAssignment> entityList = chronicAssignamentRepository.findChronicAssignmentByPatientId(patientId);
		
		entityList.forEach(ca -> {
			ChronicAssignmentModel cam = new ChronicAssignmentModel();
			cam.setStarterDate(ca.getStarterDateString());
			cam.setDiagnosis(ca.getDiagnosis());
			cam.setDoctorName(ca.getDoctor().getName());
			cam.setHospitalName(ca.getHospital().getName());
			cam.setTriggeredQuantity(ca.getTriggeredQuantity());
			cam.setQuantity(ca.getQuantity());
			cam.setFrequency(ca.getFrequency());
			cam.setPrescriptionId(prescriptionService.findPrescriptionIdByChronicAssignmentId(ca.getId()));
			cam.setId(ca.getId());
			resultList.add(cam);
		});
		
		return resultList;
	}
	
	@Override
	public void testClock() {
		//assignmen
		List<ChronicAssignment> resultList = chronicAssignamentRepository.findAllChronicAssignmentToWork();
		
		for(ChronicAssignment ca : resultList) {
			System.out.println("++++++++++++++++++++++++++++++++++");
			System.out.println("se esta analizando: "+ca.getId());
			
			Long pId = prescriptionService.findPrescriptionIdByChronicAssignmentId(ca.getId());
			
			MedicalConsultation mc = new MedicalConsultation();
			mc.setChronicAssignment(ca);
			mc.setConsultationDate(new Date());
			mc.setDoctor(ca.getDoctor());
			mc.setHospital(ca.getHospital());
			mc.setPatient(ca.getPatientChronicDiseaseId().getPatient());
			medicalConsultationRepository.save(mc);
			
			Prescription modelPrescription = prescriptionService.getById(pId);
			Prescription newPrescription = new Prescription();
			
			newPrescription.setMedicalConsultation(mc);
			newPrescription.setPrescriptionStatus(new PrescriptionStatus(7L));
			
			System.out.println("vamos a obtener los ids");
			for(PrescriptionItem pi : modelPrescription.getPrescriptionItem()) {
				System.out.println("Id: "+pi.getId());
			}
			
			newPrescription.setPrescriptionItem(this.generateItems(modelPrescription.getPrescriptionItem(), newPrescription));
			prescriptionService.savePrescription(newPrescription);
			
			ca.setTriggeredQuantity(ca.getTriggeredQuantity()+1);
			chronicAssignamentRepository.save(ca);
		}
		
		
		System.out.println(resultList.size());
	}
	
	
	private PatientChronicDisease patientChronicDiseaseModelToEntity(PatientChronicDiseaseModel model) {
		PatientChronicDisease entity = new PatientChronicDisease();
		entity.setInsertdate(new Date());
		entity.setChronicDisease(chronicDiseaseRepository.findByDiagnosisId(model.getChronicDiagnosisId()));
		entity.setPatient(patientRepository.findById(model.getPatientId()).get());
		entity.setDoctor(doctorRepository.findById(model.getDoctorId()).get());
		entity.setHospital(hospitalRepository.findById(model.getHospitalId()).get());
		return entity;
	}
	
	private ChronicAssignment chronicAssignmentModelToEntity(ChronicAssignmentModel model) {
		ChronicAssignment entity = new ChronicAssignment();
		entity.setDoctor(doctorRepository.findById(model.getDoctorId()).get());
		entity.setHospital(hospitalRepository.findById(model.getHospitalId()).get());
		entity.setPatientChronicDiseaseId(patientChronicDiseaseRepository.findById(model.getPatientChronicDiseaseId()).get());		
		entity.setStarterDate(new Date());
		entity.setFrequency(model.getFrequency());
		entity.setQuantity(model.getQuantity());
		return entity;
	}
	
	private List<PrescriptionItem> generateItems(List<PrescriptionItem> piList, Prescription newPrescription) {
		List<PrescriptionItem> piNewList = new ArrayList<>();
		for(PrescriptionItem pi : piList) {
			PrescriptionItem piNew = new PrescriptionItem();
			piNew.setPrescription(newPrescription);
			List<PrescriptionItemDiagnosis> newDiagnosisList = new ArrayList<>();
			for(PrescriptionItemDiagnosis pid : pi.getPrescriptionItemDiagnosis()) {
				PrescriptionItemDiagnosis pidNew = new PrescriptionItemDiagnosis();
				
				pidNew.setDiagnosis(pid.getDiagnosis());
				
				pidNew.setPrescriptionItem(piNew);
				newDiagnosisList.add(pidNew);
			}
			
			List<PrescriptionItemDrugGeneric> newDrugGenericList = new ArrayList<>();
			for(PrescriptionItemDrugGeneric pidg : pi.getPrescriptionItemDrugGeneric()) {
				PrescriptionItemDrugGeneric pidgNew = new PrescriptionItemDrugGeneric();
				pidgNew.setDrugGeneric(pidg.getDrugGeneric());
				
				pidgNew.setPharmaceuticalForm(pidg.getPharmaceuticalForm());
				pidgNew.setTreatment(pidg.getTreatment());
				pidgNew.setTreatmentDays(pidg.getTreatmentDays());
				
				pidgNew.setPrescriptionItem(piNew);
				newDrugGenericList.add(pidgNew);
			}
			
			List<PrescriptionItemDrugComercial> newDrugComercialList = new ArrayList<>();
			for(PrescriptionItemDrugComercial pidc : pi.getPrescriptionItemDrugComercial()) {
				PrescriptionItemDrugComercial pidcNew = new PrescriptionItemDrugComercial();
				
				pidcNew.setDrugComercial(pidc.getDrugComercial());
				pidcNew.setTreatment(pidc.getTreatment());
				pidcNew.setTreatmentDays(pidc.getTreatmentDays());
				
				pidcNew.setPrescriptionItem(piNew);
				newDrugComercialList.add(pidcNew);
			}
			
			piNew.setPrescriptionItemDiagnosis(newDiagnosisList);
			piNew.setPrescriptionItemDrugGeneric(newDrugGenericList);
			piNew.setPrescriptionItemDrugComercial(newDrugComercialList);
			
			piNewList.add(piNew);
		}
		
		return piNewList;
	}
}

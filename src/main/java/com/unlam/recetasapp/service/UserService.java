package com.unlam.recetasapp.service;

import java.util.List;

import com.unlam.recetasapp.entity.User;
import com.unlam.recetasapp.model.HospitalModel;
import com.unlam.recetasapp.model.RoleEnum;

public interface UserService {
	
	public List<HospitalModel> getDoctorHospitals(String dni);

	boolean userHasRole(User user, RoleEnum role);

	List<HospitalModel> getAdminHospitals(String dni);
	
	
}

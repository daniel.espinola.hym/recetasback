package com.unlam.recetasapp.service;

import java.util.List;

import com.unlam.recetasapp.entity.Doctor;
import com.unlam.recetasapp.model.DoctorModel;
import com.unlam.recetasapp.model.DoctorPatientModel;
import com.unlam.recetasapp.model.DoctorPostPackage;
import com.unlam.recetasapp.model.HospitalModel;
import com.unlam.recetasapp.model.HospitalPostPackage;
import com.unlam.recetasapp.model.PharmacyModel;
import com.unlam.recetasapp.model.PharmacyPostPackage;

public interface HospitalService {

	public List<HospitalModel> getAllHospitals();
	public HospitalModel saveNewHospital(HospitalPostPackage newHospitalData);
	public HospitalModel updateHospital(HospitalPostPackage updatedHospitalData);
	public HospitalModel deleteHospital(HospitalPostPackage deletedHospitalData);

}

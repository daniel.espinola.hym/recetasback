package com.unlam.recetasapp.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.unlam.recetasapp.entity.Doctor;
import com.unlam.recetasapp.entity.DoctorHospital;
import com.unlam.recetasapp.entity.MedicalConsultation;
import com.unlam.recetasapp.entity.Patient;
import com.unlam.recetasapp.entity.Pharmacist;
import com.unlam.recetasapp.entity.PharmacistPharmacy;
import com.unlam.recetasapp.entity.Pharmacy;
import com.unlam.recetasapp.entity.PharmacyPrescription;
import com.unlam.recetasapp.entity.User;
import com.unlam.recetasapp.model.DoctorModel;
import com.unlam.recetasapp.model.PharmacistModel;
import com.unlam.recetasapp.model.PharmacistPostPackage;
import com.unlam.recetasapp.repository.DoctorRepository;
import com.unlam.recetasapp.repository.MedicalConsultationRepository;
import com.unlam.recetasapp.repository.PatientRepository;
import com.unlam.recetasapp.repository.PharmacistPharmacyRepository;
import com.unlam.recetasapp.repository.PharmacistRepository;
import com.unlam.recetasapp.repository.PharmacyPrescriptionRepository;
import com.unlam.recetasapp.repository.PharmacyRepository;
import com.unlam.recetasapp.repository.UserRepository;

@Service
public class PharmacistServiceImpl implements PharmacistService {
	
	@Autowired
	PharmacistRepository pharmacistRepository;
	
	@Autowired
	DoctorRepository doctorRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	PharmacyRepository pharmacyRepository;
	
	@Autowired
	MedicalConsultationRepository medicalRepository;
	
	@Autowired
	PharmacyPrescriptionRepository pharmacyPrescriptionRepository;
	
	@Autowired
	PharmacistPharmacyRepository pharmacistPharmacyRepository;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	PatientRepository patientRepository;
	
	//Nuevos
	public List<PharmacistModel> getAllPharmacists(){
		ArrayList<Pharmacist> pharmacistList = new ArrayList<Pharmacist>(pharmacistRepository.findAll());
		ArrayList<PharmacistModel> pharmacistModelList = new ArrayList<PharmacistModel>();
		
		pharmacistList.stream().forEach(record -> {

			//Recuperar la relación con la farmacia en donde trabaja
			PharmacistPharmacy pp = record.getPharmacistPharmacy().get(0);
			//Generar una vista del farmacéutico a partir del objeto farmacéutico y la farmacia en donde trabaja
			PharmacistModel pm = new PharmacistModel(record, pp.getPharmacy());
			
			User existingUser = userRepository.findByUsername(record.getDni().toString());
			pm.setEnabledStatus(existingUser != null ? true : false);
			
			pharmacistModelList.add(pm);
		});
		
		return pharmacistModelList;
	}
	
	@Override
	public PharmacistModel saveNewPharmacist(PharmacistPostPackage newPharmacistData){
		
		//Generamos un nuevo entity de Farmacéutico con los datos provistos, pero no lo guardamos aún
		Pharmacist newPharmacistEntity = new Pharmacist();
		newPharmacistEntity.setInsertdate(newPharmacistData.getInsertDate());
		
		//Buscamos la farmacia asignada
		Optional<Pharmacy> returnedObject = pharmacyRepository.findById(newPharmacistData.getPharmacyId());
		Pharmacy assignedPharmacy = returnedObject.get();
		//Generamos un objeto con la relación Farmacista-Farmacia
		PharmacistPharmacy relation = new PharmacistPharmacy();
		relation.setPharmacist(newPharmacistEntity);
		relation.setPharmacy(assignedPharmacy);
		relation.setInsertdate(new Date());
		
		
		//Buscamos en la base de usuarios el dni que nos envían
		User existingUser = userRepository.findByUsername(newPharmacistData.getDni().toString());
		
		//Si ya existía algún usuario con ese ID, tenemos que identificar cada caso posible
		if (existingUser != null) {
			
			//Ya existe como farmaceutico, rechazar el alta de nuevo farmaceutico
			if (existingUser.getPharmacist() != null) {
				throw new IllegalArgumentException("El dni ingresado ya está dado de alta como farmacéutico");
			}
			//Ya existe como farmacéutico, rechazar el alta porque no puede tener ambos roles
			else if (existingUser.getDoctor() != null) {
				throw new IllegalArgumentException("El dni ingresado ya está dado de alta como médico, no puede ser incluído como farmacéutico");
			}
			//Ya existe como paciente, dar de alta el farmacéutico pero mantener el usuario
			else if(existingUser.getPatient() != null) {
				
				newPharmacistEntity.setDni(existingUser.getPatient().getDni());
				newPharmacistEntity.setName(existingUser.getPatient().getName());
				newPharmacistEntity.setSurname(existingUser.getPatient().getSurname());
				newPharmacistEntity.setDnitype(existingUser.getPatient().getDnitype());
				newPharmacistEntity.setBirthdate(existingUser.getPatient().getBirthdate());
				
				pharmacistRepository.save(newPharmacistEntity);
				pharmacistPharmacyRepository.save(relation);
				
				existingUser.setRole("[\"ROLE_PATIENT\",\"ROLE_PHARMACIST\"]");
				existingUser.setPharmacist(newPharmacistEntity);
				userRepository.save(existingUser);
				
			}
		} else {
			
			Doctor d = doctorRepository.findByDni(newPharmacistData.getDni());
			Pharmacist p = pharmacistRepository.findByDni(newPharmacistData.getDni());
			if (d != null) {
				throw new IllegalArgumentException("El dni ingresado ya fue dado previamente de alta como médico y fue eliminado manteniendo su información, "
						+ "consulte con el soporte técnico para continuar con el alta");
			} else if (p != null) {
				throw new IllegalArgumentException("El dni ingresado ya fue dado previamente de alta como farmacéutico y fue eliminado manteniendo su información, "
						+ "consulte con el soporte técnico para continuar con el alta");
			} else {
				throw new IllegalArgumentException("Antes de continuar con el alta la persona debe registrarse a través de la aplicación de paciente");
			}
			}
		
		PharmacistModel newPharmacistModel = new PharmacistModel(newPharmacistEntity, assignedPharmacy);
		return newPharmacistModel;
		
	}
	
	@Override
	public PharmacistModel updatePharmacist(PharmacistPostPackage updatedPharmacistData){
		
		//Recuperamos el entity del farmacéutico que hay que actualizar
		Pharmacist updatedPharmacistEntity = pharmacistRepository.findByDni(updatedPharmacistData.getDni());
		
		//Actualizamos la vinculación con la farmacia
		//Buscamos la farmacia asociada al id que recibimos en el package
		Optional<Pharmacy> returnedObject = pharmacyRepository.findById(updatedPharmacistData.getPharmacyId());
		Pharmacy newPharmacy = returnedObject.get();
		//Buscamos la farmacia asignada actualmente al farmacéutico, y vemos si cambió o no
		//En caso de que hay cambiado, procedemos a borrar la relación actual y generamos la nueva
		PharmacistPharmacy relation = updatedPharmacistEntity.getPharmacistPharmacy().get(0);
		if(relation.getPharmacy().getId() != updatedPharmacistData.getPharmacyId()) {
			//Borramos la relación vieja
			pharmacistPharmacyRepository.delete(relation);
			
			//Armamos la relación con la nueva farmacia
			//Generamos un objeto con la relación Farmacista - Farmacia Nueva
			PharmacistPharmacy newRelation = new PharmacistPharmacy();
			relation.setPharmacist(updatedPharmacistEntity);
			relation.setPharmacy(newPharmacy);
			relation.setInsertdate(new Date());
			//Persistimos la relación
			pharmacistPharmacyRepository.save(relation);
		}
		
		//Creamos el model a partir del farmacéutico actualizado y de la farmacia
		//Respecto a la farmacia, puede ser la misma que tenía o la nueva, y por eso el objeto newPharmacy no lo generamos dentro del if
		PharmacistModel updatedPharmacistModel = new PharmacistModel(updatedPharmacistEntity, newPharmacy);
		return updatedPharmacistModel;

	}
	
	@Override
	public PharmacistModel deletePharmacist(PharmacistPostPackage deletedPharmacistData) {
		
		//El usuario lo borramos siempre, es lo que me marca que va a pasar a estar "deshabilitado"
		//Sin embargo lo hacemos más adelante cuando definimos cómo avanzar con el borrado
		User deletedPharmacistUser = userRepository.findByUsername(deletedPharmacistData.getDni().toString());	
		
			
		//Antes de avanzar con el borrado de los registros de la persona como médico y como paciente
		//vamos a ver si tienen consultas médicas en las que hayan participado con uno u otro rol
		//???? Qué buscamos del farmaceutico
		Patient p = patientRepository.findByDni(deletedPharmacistData.getDni());
		Pharmacist deletedPharmacistEntity = pharmacistRepository.findByDni(deletedPharmacistData.getDni());
		PharmacistPharmacy relation = deletedPharmacistEntity.getPharmacistPharmacy().get(0);
		List<MedicalConsultation> mc_list_as_patient = medicalRepository.findByPatientId(p.getId());
		List<PharmacyPrescription> pp_list = pharmacyPrescriptionRepository.findAllByPharmacistId(deletedPharmacistUser.getPharmacist().getId());
		
		PharmacistModel deletedPharmacistModel = new PharmacistModel(deletedPharmacistEntity, relation.getPharmacy());
		//Si no tenemos ....?,
		//borramos el registro de farmaceutico y la farmacia asignada
		if (mc_list_as_patient.isEmpty() && pp_list.isEmpty()) {

			userRepository.delete(deletedPharmacistUser);
			pharmacistPharmacyRepository.delete(relation);
			pharmacistRepository.delete(deletedPharmacistEntity);
			patientRepository.delete(p);
			
			return deletedPharmacistModel;
		} else {
			//Solamente borramos el usuario y lanzamos el mensaje de error para mostrar en el front
			userRepository.delete(deletedPharmacistUser);
			throw new IllegalArgumentException("Existen recetas presentadas al farmacéutico y/o consultas médicas realizadas por el mismo como paciente, "
					+ "se mantendrán almacenados los datos y se eliminará el usuario");
		}
	}
	
	@Override
	public Pharmacist getPharmacist(Integer dni) {
		return pharmacistRepository.findByDni(dni);
	}

}

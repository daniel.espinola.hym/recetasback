package com.unlam.recetasapp.service;

import java.util.List;

import com.unlam.recetasapp.entity.Doctor;
import com.unlam.recetasapp.model.DoctorModel;
import com.unlam.recetasapp.model.DoctorPatientModel;
import com.unlam.recetasapp.model.DoctorPostPackage;

public interface DoctorService {

	public DoctorModel getDoctorById(Long id);

	public DoctorModel getDoctorByDni(Integer dni);
	
	//Nuevos
	public List<DoctorModel> getAllDoctors();
	public DoctorModel saveNewDoctor(DoctorPostPackage newDoctorData);
	public DoctorModel updateDoctor(DoctorPostPackage newDoctorData);
	public DoctorModel deleteDoctor(DoctorPostPackage newDoctorData);

	public List<DoctorPatientModel> getChronicPatientListByDoctorIdAndHospitalId(Long id, Long hospitalId);

}

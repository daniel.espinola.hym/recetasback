package com.unlam.recetasapp.service;

import java.util.List;

import com.unlam.recetasapp.entity.MedicalConsultation;
import com.unlam.recetasapp.entity.Prescription;
import com.unlam.recetasapp.model.MedicalConsultationModel;
import com.unlam.recetasapp.model.MedicalConsultationPostPackage;
import com.unlam.recetasapp.model.PrescriptionDetailModel;
import com.unlam.recetasapp.model.PrescriptionModel;
import com.unlam.recetasapp.model.PrescriptionPreviewPackage;

public interface MedicalConsultationService {

	List<MedicalConsultation> getMedicalConsultationListByPatient(Long patientId);

	PrescriptionPreviewPackage getPrescriptionPreviewPackageByPatient(Long patientId);

	PrescriptionDetailModel getPrescriptionDetailById(Long prescriptionId);

	MedicalConsultation saveMedicalConsultation(MedicalConsultationPostPackage medicalConsultationPackage);
	
	List<MedicalConsultationModel> getChronicMedicalConsultationPatientIdAndHospitalId(Long patientId, Long hospitalId);

	MedicalConsultationModel signPrescription(Long prescriptionId, String file);

	Prescription getPrescription(Long prescriptionId);
	
	List<PrescriptionModel> findPrescriptionPendingByDoctorId(Long doctorId);

	PrescriptionDetailModel getPrescriptionDetailByMedicalConsultationId(Long medicalConsultationId);
	
	List<MedicalConsultationModel> getAll();

}

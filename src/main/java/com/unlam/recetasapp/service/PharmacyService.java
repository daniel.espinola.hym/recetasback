package com.unlam.recetasapp.service;

import java.util.List;

import com.unlam.recetasapp.entity.Doctor;
import com.unlam.recetasapp.entity.Pharmacist;
import com.unlam.recetasapp.model.DoctorModel;
import com.unlam.recetasapp.model.DoctorPatientModel;
import com.unlam.recetasapp.model.DoctorPostPackage;
import com.unlam.recetasapp.model.PharmacistModel;
import com.unlam.recetasapp.model.PharmacistPostPackage;
import com.unlam.recetasapp.model.PharmacyModel;
import com.unlam.recetasapp.model.PharmacyPostPackage;

public interface PharmacyService {
	
	//Nuevos
	public List<PharmacyModel> getAllPharmacies();
	public PharmacyModel saveNewPharmacy(PharmacyPostPackage newPharmacyData);
	public PharmacyModel updatePharmacy(PharmacyPostPackage updatedPharmacyData);
	public PharmacyModel deletePharmacy(PharmacyPostPackage deletedPharmacyData);
}

package com.unlam.recetasapp.service;

import java.util.List;

import com.unlam.recetasapp.entity.Prescription;
import com.unlam.recetasapp.entity.PrescriptionItem;
import com.unlam.recetasapp.entity.PrescriptionItemDrugComercial;
import com.unlam.recetasapp.entity.PrescriptionItemDrugGeneric;
import com.unlam.recetasapp.model.PrescriptionDetailModel;
import com.unlam.recetasapp.model.PrescriptionView;

public interface PrescriptionService {

	List<PrescriptionItemDrugGeneric> getPrescriptionItemDrugGeneric(PrescriptionView prescriptionView);

	List<PrescriptionDetailModel> getPrescriptionsByDniAndCode(Integer dni, Integer otpCode) throws Exception;

	Integer generateReadToken(Integer dni, Long prescriptionId);

	List<PrescriptionItemDrugComercial> getPrescriptionItemDrugComercial(PrescriptionView prescriptionView);

	List<PrescriptionItem> generatePrescriptionItemList(PrescriptionView prescriptionView);
	/**
	 *
	 * generate Prescription with PrescriptionItemList parsed with diagnosis, generic and comercial drugs
	 *
	 * @param prescriptionView
	 * @return
	 */
	Prescription generatePrescriptionWithPrescriptionItem(PrescriptionView prescriptionView);
	
	Prescription getById(Long id);
	
	Prescription updateWithSign(Prescription p);
	
	Prescription savePrescription(Prescription p);
	
	Long findPrescriptionIdByChronicAssignmentId(Long chronicAssignmentId);
	
	void updatePrescriptionStatus(Integer prescriptionId, Integer newStatus) throws Exception;

}

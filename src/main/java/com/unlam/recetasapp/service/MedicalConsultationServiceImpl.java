package com.unlam.recetasapp.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unlam.recetasapp.entity.Doctor;
import com.unlam.recetasapp.entity.Hospital;
import com.unlam.recetasapp.entity.MedicalConsultation;
import com.unlam.recetasapp.entity.Patient;
import com.unlam.recetasapp.entity.Prescription;
import com.unlam.recetasapp.entity.PrescriptionItemDiagnosis;
import com.unlam.recetasapp.entity.PrescriptionItemDrugComercial;
import com.unlam.recetasapp.entity.PrescriptionItemDrugGeneric;
import com.unlam.recetasapp.entity.PrescriptionStatus;
import com.unlam.recetasapp.model.DiagnosisModel;
import com.unlam.recetasapp.model.DrugModel;
import com.unlam.recetasapp.model.MedicalConsultationModel;
import com.unlam.recetasapp.model.MedicalConsultationPostPackage;
import com.unlam.recetasapp.model.PrescriptionDetailModel;
import com.unlam.recetasapp.model.PrescriptionModel;
import com.unlam.recetasapp.model.PrescriptionPreviewModel;
import com.unlam.recetasapp.model.PrescriptionPreviewPackage;
import com.unlam.recetasapp.repository.MedicalConsultationRepository;
import com.unlam.recetasapp.repository.PrescriptionRepository;

@Service
public class MedicalConsultationServiceImpl implements MedicalConsultationService {

	@Autowired
	private MedicalConsultationRepository medicalConsultationRepository;

	@Autowired
	private PrescriptionRepository prescriptionRepository;

	@Autowired
	private PrescriptionService prescriptionService;

	@Override
	public List<MedicalConsultation> getMedicalConsultationListByPatient(Long patientId) {

		return medicalConsultationRepository.findByPatientIdOrderByConsultationDateDesc(patientId);

	}

	@Override
	@Transactional
	public MedicalConsultation saveMedicalConsultation(MedicalConsultationPostPackage medicalConsultationPackage) {

		// instance new medicalConsultation
		MedicalConsultation medicalConsultation = new MedicalConsultation(
				new Hospital(medicalConsultationPackage.getHospitalId()),
				new Doctor(medicalConsultationPackage.getDoctorId()),
				new Patient(medicalConsultationPackage.getPatientId()), null, new Date(),
				medicalConsultationPackage.getObservation());
		medicalConsultationRepository.save(medicalConsultation);
		Prescription prescription = prescriptionService.generatePrescriptionWithPrescriptionItem(medicalConsultationPackage.getPrescription());
		prescription.setMedicalConsultation(medicalConsultation);
		prescription.setPrescriptionStatus(new PrescriptionStatus(1L));
		prescriptionRepository.save(prescription);

		return medicalConsultation;
	}

	@Override
	public PrescriptionPreviewPackage getPrescriptionPreviewPackageByPatient(Long patientId) {

		List<MedicalConsultation> medicalConsultationList = medicalConsultationRepository.findByPatientIdOrderByConsultationDateDesc(patientId);

		List<PrescriptionPreviewModel> precriptionPreviewModelList = new ArrayList<PrescriptionPreviewModel>();

		for (MedicalConsultation medicalConsultation : medicalConsultationList) {
			List<Prescription> prescriptionList = medicalConsultation.getPrescriptionList();
			PrescriptionPreviewModel prescriptionPreview;
			for (Prescription prescription : prescriptionList) {
				prescriptionPreview = new PrescriptionPreviewModel(prescription);
				precriptionPreviewModelList.add(prescriptionPreview);
			}

		}

		return new PrescriptionPreviewPackage(precriptionPreviewModelList);

	}

	@Override
	public PrescriptionDetailModel getPrescriptionDetailById(Long prescriptionId) {

		Prescription prescription = prescriptionRepository.getOne(prescriptionId);

		return new PrescriptionDetailModel(prescription);

	}

	@Override
	public List<MedicalConsultationModel> getChronicMedicalConsultationPatientIdAndHospitalId(Long patientId, Long hospitalId) {
		List<MedicalConsultationModel> medicalConsultationChronicList = new ArrayList<MedicalConsultationModel>();
		List<MedicalConsultation> medicalConsultationList = this.medicalConsultationRepository.findAllMCChronicByPatientAndHospital(patientId, hospitalId);

		for (MedicalConsultation mc : medicalConsultationList) {
			MedicalConsultationModel mcm = new MedicalConsultationModel();
			
			mcm.setId(mc.getId());
			
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			mcm.setMedicalConsultationDate(sdf.format(mc.getConsultationDate()));
			
			mcm.setDoctorName(mc.getDoctor().getName() + " " + mc.getDoctor().getSurname());
			mcm.setDoctorId(mc.getDoctorId());

			mcm.setPatientName(mc.getPatient().getName() + " " + mc.getPatient().getSurname());
			mcm.setPatientId(mc.getPatientId());

			mcm.setHospitalName(mc.getHospital().getName());
			mcm.setHospitalId(mc.getHospitalId());

			PrescriptionModel pm = new PrescriptionModel();

			List<DiagnosisModel> diagnosisModelList = new ArrayList<>();
			List<DrugModel> drugModelList = new ArrayList<>();

			for (PrescriptionItemDiagnosis pid : mc.getPrescriptionList().get(0).getPrescriptionItem().get(0)
					.getPrescriptionItemDiagnosis()) {
				DiagnosisModel dm = new DiagnosisModel();
				dm.setId(pid.getDiagnosis().getId());
				dm.setName(pid.getDiagnosis().getDescription());
				diagnosisModelList.add(dm);
			}

			for (PrescriptionItemDrugComercial pidc : mc.getPrescriptionList().get(0).getPrescriptionItem().get(0)
					.getPrescriptionItemDrugComercial()) {
				DrugModel dm = new DrugModel();
				dm.setId(pidc.getDrugComercial().getId() + "C");
				dm.setName(pidc.getDrugComercial().getName());
				dm.setStrength("");
				dm.setTreatment(pidc.getTreatment());
				dm.setDays(pidc.getTreatmentDays());

				drugModelList.add(dm);
			}

			for (PrescriptionItemDrugGeneric pidg : mc.getPrescriptionList().get(0).getPrescriptionItem().get(0)
					.getPrescriptionItemDrugGeneric()) {
				DrugModel dm = new DrugModel();
				dm.setId(pidg.getDrugGeneric().getId() + "G");
				dm.setName(pidg.getDrugGeneric().getName());
				dm.setStrength(pidg.getDrugGeneric().getStrength().getNumber() + ""
						+ pidg.getDrugGeneric().getStrength().getUnityId().getDescription());
				dm.setTreatment(pidg.getTreatment());
				dm.setDays(pidg.getTreatmentDays());

				drugModelList.add(dm);
			}

			pm.setDiagnosisList(diagnosisModelList);
			pm.setDrugList(drugModelList);

			mcm.setPrescription(pm);

			medicalConsultationChronicList.add(mcm);

		}

		return medicalConsultationChronicList;
	}

	@Override
	public MedicalConsultationModel signPrescription(Long prescriptionId, String file) {
		Prescription p = prescriptionService.getById(prescriptionId);
		p.setFile(file);
		p.setPrescriptionStatus(new PrescriptionStatus(2L));
		prescriptionService.updateWithSign(p);
		return new MedicalConsultationModel();
	}
	
	@Override
	public Prescription getPrescription(Long prescriptionId) {
		Prescription p = prescriptionService.getById(prescriptionId);
		return p;
	}

	@Override
	public PrescriptionDetailModel getPrescriptionDetailByMedicalConsultationId(Long medicalConsultationId) {
		MedicalConsultation mc = medicalConsultationRepository.findById(medicalConsultationId).get();
		return new PrescriptionDetailModel(mc.getPrescriptionList().get(0));
	}

	@Override
	public List<PrescriptionModel> findPrescriptionPendingByDoctorId(Long doctorId) {
		List<Prescription> prescriptionList = prescriptionRepository.findPrescriptionPendingByDoctorId(doctorId);
		List<PrescriptionModel> resultList = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		prescriptionList.forEach(p -> {
			PrescriptionModel pm = new PrescriptionModel();
			pm.setId(p.getId());
			pm.setStatus("Pendiente");
			pm.setHospitalName(p.getMedicalConsultation().getHospital().getName());
			pm.setDate(sdf.format(p.getMedicalConsultation().getConsultationDate()));
			pm.setPatientName(p.getMedicalConsultation().getPatient().getSurname()+" "+p.getMedicalConsultation().getPatient().getName());
			
			pm.setMedicalConsultationId(p.getMedicalConsultation().getId());
			pm.setPatientId(p.getMedicalConsultation().getPatient().getId());
			pm.setHospitalId(p.getMedicalConsultation().getHospital().getId());
			
			List<DiagnosisModel> diagnosisModelList = new ArrayList<>();
			List<DrugModel> drugModelList = new ArrayList<>();

			for (PrescriptionItemDiagnosis pid : p.getPrescriptionItem().get(0)
					.getPrescriptionItemDiagnosis()) {
				DiagnosisModel dm = new DiagnosisModel();
				dm.setId(pid.getDiagnosis().getId());
				dm.setName(pid.getDiagnosis().getDescription());
				diagnosisModelList.add(dm);
			}

			for (PrescriptionItemDrugComercial pidc : p.getPrescriptionItem().get(0)
					.getPrescriptionItemDrugComercial()) {
				DrugModel dm = new DrugModel();
				dm.setId(pidc.getDrugComercial().getId() + "C");
				dm.setName(pidc.getDrugComercial().getName());
				dm.setStrength("");
				dm.setTreatment(pidc.getTreatment());
				dm.setDays(pidc.getTreatmentDays());

				drugModelList.add(dm);
			}

			for (PrescriptionItemDrugGeneric pidg : p.getPrescriptionItem().get(0)
					.getPrescriptionItemDrugGeneric()) {
				DrugModel dm = new DrugModel();
				dm.setId(pidg.getDrugGeneric().getId() + "G");
				dm.setName(pidg.getDrugGeneric().getName());
				dm.setStrength(pidg.getDrugGeneric().getStrength().getNumber() + ""
						+ pidg.getDrugGeneric().getStrength().getUnityId().getDescription());
				dm.setTreatment(pidg.getTreatment());
				dm.setDays(pidg.getTreatmentDays());

				drugModelList.add(dm);
			}

			pm.setDiagnosisList(diagnosisModelList);
			pm.setDrugList(drugModelList);
			
			resultList.add(pm);
		});
		
		return resultList;
	}

	@Override
	public List<MedicalConsultationModel> getAll() {
		List<MedicalConsultationModel> mcList = new ArrayList<>();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		medicalConsultationRepository.findAll().forEach(x->{
			MedicalConsultationModel mcm = new MedicalConsultationModel();
			mcm.setDoctorName(x.getDoctor().getFullName());
			mcm.setHospitalName(x.getHospital().getName());
			mcm.setPatientName(x.getPatient().getName()+" "+x.getPatient().getSurname());
			mcm.setDateString(sdf.format(x.getConsultationDate()));
			mcm.setId(x.getId());
			mcm.setTieneReceta(false);
			if(x.getPrescriptionList().get(0).getPrescriptionStatus().getId()==2L || x.getPrescriptionList().get(0).getPrescriptionStatus().getId()==3L || x.getPrescriptionList().get(0).getPrescriptionStatus().getId()==4L) {
				mcm.setPrescriptionId(x.getPrescriptionList().get(0).getId());
				mcm.setTieneReceta(true);
			}
			mcList.add(mcm);
		});
		Collections.sort(mcList);
		return mcList;
	}
	
}

package com.unlam.recetasapp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unlam.recetasapp.entity.DrugComercial;
import com.unlam.recetasapp.entity.DrugComercialCompose;
import com.unlam.recetasapp.entity.DrugGeneric;
import com.unlam.recetasapp.entity.PharmaceuticalForm;
import com.unlam.recetasapp.entity.Prescription;
import com.unlam.recetasapp.entity.PrescriptionItemDrugComercial;
import com.unlam.recetasapp.entity.PrescriptionItemDrugGeneric;
import com.unlam.recetasapp.model.DrugComercialModel;
import com.unlam.recetasapp.model.DrugGenericModel;
import com.unlam.recetasapp.model.DrugGenericVarietyPackage;
import com.unlam.recetasapp.model.DrugModel;
import com.unlam.recetasapp.model.DrugsPackage;
import com.unlam.recetasapp.model.PharmaceuticalFormModel;
import com.unlam.recetasapp.model.StrengthModel;
import com.unlam.recetasapp.repository.DrugComercialComposeRepository;
import com.unlam.recetasapp.repository.DrugComercialRepository;
import com.unlam.recetasapp.repository.DrugGenericRepository;
import com.unlam.recetasapp.repository.LaboratoryRepository;
import com.unlam.recetasapp.repository.PharmaceuticalFormRepository;
import com.unlam.recetasapp.util.AlphabetMap;

@Service
public class DrugServiceImpl implements DrugService {

	@Autowired
	private DrugGenericRepository drugGenericRepository;

	@Autowired
	private DrugComercialRepository drugComercialRepository;

	@Autowired
	private DrugComercialComposeRepository drugComercialComposeRepository;
	
	@Autowired
	private PharmaceuticalFormRepository PFRepo;

	@Autowired
	private PrescriptionService prescriptionService;

	@Autowired
	private LaboratoryRepository laboRepository;

	@Override
	//@Transactional
	public DrugGenericVarietyPackage getDrugGenericVariety(Long idDrugGeneric) {
		
		DrugGeneric drugGeneric = drugGenericRepository.getOne(idDrugGeneric);
		
		List<StrengthModel> strengthVariety = drugGenericRepository.findStrengthByName(drugGeneric.getName());
		if(drugGeneric.getStrengthString()!=null) {
			if(strengthVariety==null) {
				strengthVariety = new ArrayList<>();
			}
			strengthVariety.add(new StrengthModel(drugGeneric.getStrengthString()));
		}
		List<PharmaceuticalFormModel> pharmaceuticalFormList = drugGenericRepository.findPharmaceuticalFormByName(drugGeneric.getName());
		

		DrugGenericVarietyPackage dgVarietyPackage = new DrugGenericVarietyPackage(strengthVariety, pharmaceuticalFormList);
		
		return dgVarietyPackage;
	}

	@Override
	public DrugsPackage getAllDrugs() {

		List<DrugGeneric> drugGenericList = drugGenericRepository.findAllByOrderByNameAsc();

		List<DrugGeneric> drugGenericToRemove = new ArrayList<DrugGeneric>();
		boolean firstDG = true;
		DrugGeneric auxDG = null;
		for (DrugGeneric drugGeneric : drugGenericList) {
			if (!firstDG) {
				if (drugGeneric.getName().equals(auxDG.getName())) {
					drugGenericToRemove.add(drugGeneric);
				}
			} else {
				firstDG = false;
			}
			auxDG = drugGeneric;
		}

		drugGenericList.removeAll(drugGenericToRemove);

		List<DrugComercial> drugComercialList = drugComercialRepository.findAllByOrderByNameAsc();

		List<DrugGenericModel> dgModelList = new ArrayList<DrugGenericModel>();

		List<DrugComercialModel> dcModelList = new ArrayList<DrugComercialModel>();

		for (DrugGeneric drugGeneric : drugGenericList) {
			DrugGenericModel drugGenericModel = new DrugGenericModel(drugGeneric.getId(), drugGeneric.getName(), null);
			dgModelList.add(drugGenericModel);
		}

		for (DrugComercial drugComercial : drugComercialList) {
			List<DrugGeneric> drugComponents = getDrugComponents(drugComercial);
			DrugComercialModel drugComercialModel = new DrugComercialModel(drugComercial.getId(), drugComercial,
					drugComponents, null, null);
			dcModelList.add(drugComercialModel);
		}

		boolean first = true;
		Integer countRepeated = 0;
		DrugComercialModel previousDcModel = null;
		boolean firstMarkLoop = true;
		for (DrugComercialModel drugComercialModel : dcModelList) {
			if (!first) {
				if (drugComercialModel.getDescription().equals(previousDcModel.getDescription())) {
					if (firstMarkLoop) {
						countRepeated++;
						previousDcModel.setDescription(previousDcModel.getDescription() + "("
								+ AlphabetMap.alphabetMap.get(countRepeated) + ")");
						firstMarkLoop = false;
					}
					countRepeated++;
					drugComercialModel.setDescription(drugComercialModel.getDescription() + "("
							+ AlphabetMap.alphabetMap.get(countRepeated) + ")");
				} else {
					firstMarkLoop = true;
					countRepeated = 0;
				}
			} else {
				first = false;
			}
			previousDcModel = drugComercialModel;
		}
		DrugsPackage drugsPackage = new DrugsPackage(dgModelList, dcModelList);
		return drugsPackage;
	}

	@Override
	public List<DrugModel> getAllDrugByPrescriptionId(Long PrescriptionId) {
		Prescription p = prescriptionService.getById(PrescriptionId);
		List<DrugModel> drugModelList = new ArrayList<>();

		for (PrescriptionItemDrugComercial pidc : p.getPrescriptionItem().get(0).getPrescriptionItemDrugComercial()) {
			DrugModel dm = new DrugModel();
			dm.setId(pidc.getDrugComercial().getId() + "C");
			dm.setName(pidc.getDrugComercial().getName());
			dm.setStrength("");
			dm.setTreatment(pidc.getTreatment());
			dm.setDays(pidc.getTreatmentDays());
			dm.setPharmaceuticalForm(pidc.getDrugComercial().getPharmaceuticalForm().getDescription());
			dm.setComercial(true);

			List<DrugModel> composition = new ArrayList<>();
			for (DrugComercialCompose dcc : pidc.getDrugComercial().getDrugComercialCompose()) {
				DrugGeneric dg = dcc.getDrugGeneric();
				DrugModel dmc = new DrugModel();
				String description = dg.getName() + " " + dg.getStrength().getNumber() + ""
						+ dg.getStrength().getUnityId().getDescription();
				dmc.setDescription(description);
				composition.add(dmc);
			}
			dm.setComposition(composition);

			drugModelList.add(dm);
		}

		for (PrescriptionItemDrugGeneric pidg : p.getPrescriptionItem().get(0).getPrescriptionItemDrugGeneric()) {
			DrugModel dm = new DrugModel();
			dm.setId(pidg.getDrugGeneric().getId() + "G");
			dm.setName(pidg.getDrugGeneric().getName());
			dm.setStrength(pidg.getDrugGeneric().getStrength().getNumber() + ""
					+ pidg.getDrugGeneric().getStrength().getUnityId().getDescription());
			dm.setTreatment(pidg.getTreatment());
			dm.setDays(pidg.getTreatmentDays());
			dm.setPharmaceuticalForm(pidg.getPharmaceuticalForm().getDescription());
			dm.setComercial(false);
			drugModelList.add(dm);
		}
		return drugModelList;
	}

	private List<DrugGeneric> getDrugComponents(DrugComercial drugComercial) {
		List<DrugComercialCompose> drugComercialComposeList = drugComercial.getDrugComercialCompose();
		List<DrugGeneric> drugComponents = new ArrayList<DrugGeneric>();

		for (DrugComercialCompose drugComercialCompose : drugComercialComposeList) {
			drugComponents.add(drugComercialCompose.getDrugGeneric());
		}

		return drugComponents;
	}

	@Override
	public void addDrugFromExcelRow(String comercialName, String genericName, String strength,
			String pharmaceuticalForm) {

		String genericCompose[] = genericName.split("-");
		String strengthCompose[] = strength.split("-");
		ArrayList<DrugComercialCompose> dccl = new ArrayList<>();
		for (int x = 0; x < genericCompose.length; x++) {
			DrugGeneric dg = new DrugGeneric();
			DrugComercialCompose dcc = new DrugComercialCompose();
			dg.setName(genericCompose[x]);
			dg.setStrengthString(strengthCompose[x]);
			drugGenericRepository.save(dg);
			dcc.setDrugGeneric(dg);
			dccl.add(dcc);
		}

		DrugComercial dc = new DrugComercial();
		dc.setName(comercialName);
		dc.setLaboratory(laboRepository.findById(1L).get());
		
		PharmaceuticalForm pf = PFRepo.findByDescription(pharmaceuticalForm);
		if(pf==null) {
			PharmaceuticalForm pfToSave = new PharmaceuticalForm();
			pfToSave.setDescription(pharmaceuticalForm);
			pf = PFRepo.save(pfToSave);
		}
		dc.setPharmaceuticalForm(pf);
		dc.setQuantity(1);
		for (DrugComercialCompose a : dccl) {
			a.setDrugComercial(dc);
		}
		dc.setDrugComercialCompose(dccl);

		drugComercialRepository.save(dc);

		for (DrugComercialCompose a : dccl) {
			drugComercialComposeRepository.save(a);
			System.out.println("saved");
		}
	}

}

package com.unlam.recetasapp.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unlam.recetasapp.entity.Diagnosis;
import com.unlam.recetasapp.model.DiagnosisModel;
import com.unlam.recetasapp.repository.ChronicDiseaseRepository;
import com.unlam.recetasapp.repository.DiagnosisRepository;

@Service
public class DiagnosisServiceImpl implements DiagnosisService{
	
	@Autowired
	DiagnosisRepository diagnosisRepository;
	
	@Autowired
	ChronicDiseaseRepository chronicDiagnosisRepository;
	
	@Override
	public List<Diagnosis> getDiagnosisList() {
		return diagnosisRepository.findAll();
	}
	
	@Override
	public List<DiagnosisModel> getDiagnosisModelList() {
		ArrayList<DiagnosisModel> dmList = new ArrayList<>();
		diagnosisRepository.findAll().forEach(x->{
			DiagnosisModel dm = new DiagnosisModel();
			dm.setId(x.getId());
			dm.setName(x.getDescription());
			dmList.add(dm);
		});
 		return dmList;
	}

	@Override
	public List<Diagnosis> getChronicDiagnosisList() {
		List<Diagnosis> chronicDiagnosisList = new ArrayList<>();
		chronicDiagnosisRepository.findAll().stream().forEach(cd -> {
			chronicDiagnosisList.add(cd.getDiagnosis());
		});
		return chronicDiagnosisList;
	}

	@Override
	public DiagnosisModel saveDiagnosis(DiagnosisModel dm) {
		Diagnosis d = new Diagnosis();
		d.setDescription(dm.getName());
		diagnosisRepository.save(d);
		dm.setId(d.getId());
		return dm;
	}

}

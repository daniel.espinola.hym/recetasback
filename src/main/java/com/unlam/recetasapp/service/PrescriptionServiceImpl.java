package com.unlam.recetasapp.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unlam.recetasapp.entity.DrugGeneric;
import com.unlam.recetasapp.entity.Prescription;
import com.unlam.recetasapp.entity.PrescriptionItem;
import com.unlam.recetasapp.entity.PrescriptionItemDiagnosis;
import com.unlam.recetasapp.entity.PrescriptionItemDrugComercial;
import com.unlam.recetasapp.entity.PrescriptionItemDrugGeneric;
import com.unlam.recetasapp.entity.SecurityToken;
import com.unlam.recetasapp.entity.Strength;
import com.unlam.recetasapp.model.DrugView;
import com.unlam.recetasapp.model.PrescriptionDetailModel;
import com.unlam.recetasapp.model.PrescriptionView;
import com.unlam.recetasapp.model.TokenStatus;
import com.unlam.recetasapp.repository.DrugGenericRepository;
import com.unlam.recetasapp.repository.PrescriptionRepository;
import com.unlam.recetasapp.repository.PrescriptionStatusRepository;
import com.unlam.recetasapp.repository.TokenRepository;


@Service
public class PrescriptionServiceImpl implements PrescriptionService{
	
	Logger logger = LoggerFactory.getLogger(PrescriptionServiceImpl.class);

	@Autowired
	private DrugGenericRepository drugGenericRepository;
	
	@Autowired
	private TokenRepository tokenRepository;
	
	@Autowired
	private PrescriptionRepository prescriptionRepository;
	
	@Autowired
	private PrescriptionStatusRepository prescriptionStatusRepository;
	
	
	@Override
	public List<PrescriptionItemDrugGeneric> getPrescriptionItemDrugGeneric(PrescriptionView prescriptionView) {
    	List<DrugView> drugList = prescriptionView.getDrugList();
    	List<PrescriptionItemDrugGeneric> pItemDrugGenericList = new ArrayList<>();
    	for(DrugView drug : drugList) {
    		if(drug.getId().startsWith("G")) {
    			//find generic drug by drug id and strength description
				DrugGeneric drugGeneric = drugGenericRepository.findByNameAndStrength(drug.getName(), new Strength(drug.getStrengthId()));
				PrescriptionItemDrugGeneric pItemDrugGeneric = 
						new PrescriptionItemDrugGeneric(drugGeneric, 
								drug.getPharmaceuticalFormId(), 
								drug.getTreatment(),
								drug.getTreatmentDays());
				pItemDrugGenericList.add(pItemDrugGeneric);
    		}
    	}
    	return pItemDrugGenericList;
	};
	
	@Override
	public List<PrescriptionItemDrugComercial> getPrescriptionItemDrugComercial(PrescriptionView prescriptionView) {
    	List<DrugView> drugList = prescriptionView.getDrugList();
    	List<PrescriptionItemDrugComercial> pItemDrugComercialList = new ArrayList<>();
    	for(DrugView drug : drugList) {
    		if(drug.getId().startsWith("C")) {
    			//instance comercial drug by drugVO
    			PrescriptionItemDrugComercial pItemDrugComercial = new PrescriptionItemDrugComercial(drug);
    			pItemDrugComercialList.add(pItemDrugComercial);
    		}
    	}
    	return pItemDrugComercialList;
	};
	
	@Override
	public List<PrescriptionItem> generatePrescriptionItemList(PrescriptionView prescriptionView) {
    	List<DrugView> drugList = prescriptionView.getDrugList();
    	List<PrescriptionItem> prescriptionItem = new ArrayList<>();
    	List<PrescriptionItemDrugComercial> pItemDrugComercialList = new ArrayList<>();
    	List<PrescriptionItemDrugGeneric> pItemDrugGenericList = new ArrayList<>();
    	for(DrugView drug : drugList) {
    		if(drug.getId().startsWith("G")) {
    			//find generic drug by drug id and strength description
				DrugGeneric drugGeneric = drugGenericRepository.findByNameAndStrength(drug.getName(), new Strength(drug.getStrengthId()));
				PrescriptionItemDrugGeneric pItemDrugGeneric = 
						new PrescriptionItemDrugGeneric(drugGeneric, 
								drug.getPharmaceuticalFormId(), 
								drug.getTreatment(),
								drug.getTreatmentDays());
				pItemDrugGenericList.add(pItemDrugGeneric);
    		}
    		if(drug.getId().startsWith("C")) {
    			//instance comercial drug by drugVO
    			PrescriptionItemDrugComercial pItemDrugComercial = new PrescriptionItemDrugComercial(drug);
    			pItemDrugComercialList.add(pItemDrugComercial);
    		}
    	}
    	return prescriptionItem;
	};
	
	@Override
	public Prescription generatePrescriptionWithPrescriptionItem(PrescriptionView prescriptionView) {
		List<DrugView> drugList = prescriptionView.getDrugList();
		List<PrescriptionItemDrugComercial> pItemDrugComercialList = new ArrayList<>();
    	List<PrescriptionItemDrugGeneric> pItemDrugGenericList = new ArrayList<>();
    	List<Long> diagnosisList = prescriptionView.getDiagnosisList();
    	List<PrescriptionItemDiagnosis> pItemDiagnosisList = new ArrayList<PrescriptionItemDiagnosis>();
    	
		Prescription prescription = new Prescription();
		List<PrescriptionItem> piList = new ArrayList<>();
		PrescriptionItem pi = new PrescriptionItem();
		
		for(DrugView drug : drugList) {
    		if(drug.getId().startsWith("G")) {
    			//find generic drug by drug id and strength description
				DrugGeneric drugGeneric = drugGenericRepository.findByNameAndStrength(drug.getName(), new Strength(drug.getStrengthId()));
				PrescriptionItemDrugGeneric pItemDrugGeneric = 
						new PrescriptionItemDrugGeneric(drugGeneric, 
								drug.getPharmaceuticalFormId(), 
								drug.getTreatment(),
								drug.getTreatmentDays());
				pItemDrugGeneric.setPrescriptionItem(pi);
				pItemDrugGenericList.add(pItemDrugGeneric);
    		}
    		if(drug.getId().startsWith("C")) {
    			//instance comercial drug by drugVO
    			PrescriptionItemDrugComercial pItemDrugComercial = new PrescriptionItemDrugComercial(drug);
    			pItemDrugComercial.setPrescriptionItem(pi);
    			pItemDrugComercialList.add(pItemDrugComercial);
    		}
    	}
		
		for(Long diagnosisId : diagnosisList) {
    		PrescriptionItemDiagnosis pItemDiagnosis = new PrescriptionItemDiagnosis(diagnosisId);
    		pItemDiagnosis.setPrescriptionItem(pi);
    		pItemDiagnosisList.add(pItemDiagnosis);
    	}
		
		pi.setPrescription(prescription);
		pi.setPrescriptionItemDrugComercial(pItemDrugComercialList);
		pi.setPrescriptionItemDrugGeneric(pItemDrugGenericList);
		pi.setPrescriptionItemDiagnosis(pItemDiagnosisList);
		piList.add(pi);
		
		prescription.setPrescriptionItem(piList);
		
		return prescription;
	}
	
	//ES PARA LA HISTORIA DE CONFIRMAR USO DE RECETAS
	@Override
	public void updatePrescriptionStatus(Integer prescriptionId, Integer newStatus) throws Exception{	
		Prescription pr = this.getById((long)prescriptionId);
		pr.setPrescriptionStatus(prescriptionStatusRepository.getOne((long)newStatus));
		prescriptionRepository.save(pr);
	}
	
	@Override
	public List<PrescriptionDetailModel> getPrescriptionsByDniAndCode(Integer dni, Integer otpCode) throws Exception{
		
		SecurityToken token = tokenRepository.findByDniAndOtpCode(dni, otpCode);
		
		List<PrescriptionDetailModel> prescriptionDetailList = new ArrayList<PrescriptionDetailModel>();
		
		if(token != null) {
			if(isValidToken(token)) {
				logger.info("El codigo es valido, se procede a obtener las recetas seleccionadas para dicho codigo");
				for(Prescription prescription : token.getPrescriptions()) {
					prescriptionDetailList.add(new PrescriptionDetailModel(prescription));
					
					//ESTO ES LO NUEVO PARA PONER EL ESTADO POSTERIOR A PRESENTARLE LA RECETA AL FARMACEUTICO
					//ES PARA LA HISTORIA DE CONFIRMAR USO DE RECETAS
					//SIN EMBARGO, NO QUIERO QUE SE HAGA AUTOMATICAMENTE SIN QUE EL FARMACEUTICO PARTICIPE
					//prescription.setPrescriptionStatus(prescriptionStatusRepository.getOne(3L));
				}
				token.setTokenStatus(TokenStatus.UTILIZADO);
				tokenRepository.save(token);
			}else{
				if(token.getTokenStatus().equals(TokenStatus.UTILIZADO)) {
					throw new Exception("El codigo " + otpCode + " ya fue utilizado");
				}else if(token.getTokenStatus().equals(TokenStatus.EXPIRADO_POR_TIEMPO) || token.getTokenStatus().equals(TokenStatus.EXPIRADO_POR_NUEVO_TOKEN)){
					throw new Exception("El codigo " + otpCode + " ya expiró");
				}
				
			}			
		}else {
			throw new Exception("No se encontró el codigo ingresado");
		}
		
				
		return prescriptionDetailList;
		
	}
	
	
	private boolean isValidToken(SecurityToken token) {
		if(token.getTokenStatus().equals(TokenStatus.SIN_UTILIZAR) && new Date().before(token.getDateExpiration())) {
			return true;
		}
		return false;
	}
	
	

	@Override
	public Integer generateReadToken(Integer dni, Long prescriptionId) {
		
		SecurityToken tokenVigente = tokenRepository.findByDniAndTokenStatus(dni, TokenStatus.SIN_UTILIZAR);
		
		if(tokenVigente != null) {
			tokenVigente.setTokenStatus(TokenStatus.EXPIRADO_POR_NUEVO_TOKEN);
		}
		
		SecurityToken token = new SecurityToken(dni, prescriptionId);
		tokenRepository.save(token);
		
		
		return token.getOtpCode();
	}

	@Override
	public Prescription getById(Long id) {
		return prescriptionRepository.findById(id).get();
	}

	@Override
	public Prescription updateWithSign(Prescription p) {
		return prescriptionRepository.save(p);
	}

	@Override
	public Prescription savePrescription(Prescription p) {
		return this.prescriptionRepository.save(p);
	}

	@Override
	public Long findPrescriptionIdByChronicAssignmentId(Long chronicAssignmentId) {
		return this.prescriptionRepository.findPrescriptionIdChronicAssignmentId(chronicAssignmentId);
	}
	
}

package com.unlam.recetasapp.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unlam.recetasapp.entity.Doctor;
import com.unlam.recetasapp.entity.DoctorHospital;
import com.unlam.recetasapp.entity.Hospital;
import com.unlam.recetasapp.entity.MedicalConsultation;
import com.unlam.recetasapp.entity.Patient;
import com.unlam.recetasapp.entity.PatientChronicDisease;
import com.unlam.recetasapp.entity.Pharmacist;
import com.unlam.recetasapp.entity.PharmacistPharmacy;
import com.unlam.recetasapp.entity.Pharmacy;
import com.unlam.recetasapp.entity.User;
import com.unlam.recetasapp.model.DoctorModel;
import com.unlam.recetasapp.model.DoctorPatientModel;
import com.unlam.recetasapp.model.DoctorPostPackage;
import com.unlam.recetasapp.repository.ChronicDiseaseRepository;
import com.unlam.recetasapp.repository.DoctorHospitalRepository;
import com.unlam.recetasapp.repository.DoctorRepository;
import com.unlam.recetasapp.repository.HospitalRepository;
import com.unlam.recetasapp.repository.MedicalConsultationRepository;
import com.unlam.recetasapp.repository.PatientRepository;
import com.unlam.recetasapp.repository.PharmacistRepository;
import com.unlam.recetasapp.repository.UserRepository;


@Service
public class DoctorServiceImpl implements DoctorService {

	@Autowired
	DoctorRepository doctorRepository;
	
	@Autowired
	PharmacistRepository pharmacistRepository;
	
	@Autowired
	HospitalRepository hospitalRepository;
	
	@Autowired
	DoctorHospitalRepository doctorHospitalRepository;
	
	@Autowired
	UserRepository userRepository;

	@Autowired
	ChronicDiseaseRepository chronicDiseaseRepository;

	@Autowired
	MedicalConsultationRepository medicalRepository;
	
	@Autowired
	ChronicService chronicService;
	
	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	PatientRepository patientRepository;
	
	//Nuevo: para hacer GET de todos los doctores, es para el controller de aplicación de admin
	@Override
	public List<DoctorModel> getAllDoctors(){
		ArrayList<Doctor> doctorList = new ArrayList<Doctor>(doctorRepository.findAll());
		ArrayList<DoctorModel> doctorModelList = new ArrayList<DoctorModel>();
		
		doctorList.stream().forEach(doc -> {
			DoctorModel dm = new DoctorModel(doc);
			
			User existingUser = userRepository.findByUsername(doc.getDni().toString());
			dm.setEnabledStatus(existingUser != null ? true : false);
			
			//Devolvemos la primera matrícula y hospital asignados
			List<DoctorHospital> dh_list = doc.getDoctorHospital();
			if(!dh_list.isEmpty()) {
				dm.setHospitalLicenseData(dh_list.get(0));
			}
			
			doctorModelList.add(dm);
			
		});
		
		return doctorModelList;
	}
	
	
	@Override
	public DoctorModel saveNewDoctor(DoctorPostPackage newDoctorData){
		
		//Generamos un nuevo entity de Doctor con los datos provistos, pero no lo guardamos aún
		Doctor newDoctorEntity = new Doctor();
		newDoctorEntity.setInsertdate(newDoctorData.getInsertDate());
	
		//Generamos una nueva relación Doctor/Hospital con la matricula provista, pero no lo guardamos aún
		Optional<Hospital> returnedObject = hospitalRepository.findById(newDoctorData.getHospitalId());
		Hospital hos = returnedObject.get();
		DoctorHospital dh = new DoctorHospital();
		dh.setDoctor(newDoctorEntity);
		dh.setHospital(hos);
		dh.setDoctorLicenseNumber(newDoctorData.getLicenseNumber().toString());
		
		
		//Buscamos en la base de usuarios el dni que nos envían
		User existingUser = userRepository.findByUsername(newDoctorData.getDni().toString());
		
		//Si ya existía algún usuario con ese ID, tenemos que identificar cada caso posible
		if (existingUser != null) {
			
			//Ya existe como doctor, rechazar el alta de nuevo doctor
			if (existingUser.getDoctor() != null) {
				throw new IllegalArgumentException("El usuario ingresado ya está dado de alta como médico");
			}
			//Ya existe como farmacéutico, rechazar el alta porque no puede tener ambos roles
			else if (existingUser.getPharmacist() != null) {
				throw new IllegalArgumentException("El usuario ingresado ya está dado de alta como farmacéutico, no puede ser incluído como médico");
			}
			//Ya existe como paciente, dar de alta el doctor pero mantener el usuario
			else if(existingUser.getPatient() != null) {
				
				newDoctorEntity.setDni(existingUser.getPatient().getDni());
				newDoctorEntity.setName(existingUser.getPatient().getName());
				newDoctorEntity.setSurname(existingUser.getPatient().getSurname());
				newDoctorEntity.setDnitype(existingUser.getPatient().getDnitype());
				newDoctorEntity.setBirthdate(existingUser.getPatient().getBirthdate());
				
				doctorRepository.save(newDoctorEntity);
				doctorHospitalRepository.save(dh);
				
				existingUser.setRole("[\"ROLE_PATIENT\",\"ROLE_DOCTOR\"]");
				existingUser.setDoctor(newDoctorEntity);
				userRepository.save(existingUser);
				

			}
		} else {
			
			Doctor d = doctorRepository.findByDni(newDoctorData.getDni());
			Pharmacist p = pharmacistRepository.findByDni(newDoctorData.getDni());
			if (d != null) {
				throw new IllegalArgumentException("El dni ingresado ya fue dado previamente de alta como médico y fue eliminado, consulte con el soporte técnico para continuar con el alta");
			} else if (p != null) {
				throw new IllegalArgumentException("El dni ingresado ya fue dado previamente de alta como farmacéutico y fue eliminado, consulte con el soporte técnico para continuar con el alta");
			} else {
				throw new IllegalArgumentException("Primero debe darse de alta como paciente a través de la aplicación móvil");
			}

		}
		
		DoctorModel newDoctorModel = new DoctorModel(newDoctorEntity, dh);
		return newDoctorModel;
		
	}
	
	@Override
	public DoctorModel updateDoctor(DoctorPostPackage newDoctorData){
		Doctor d = doctorRepository.findByDni(newDoctorData.getDni());
			
		//Actualizamos la vinculación con el hospital
		//Buscamos el hospital asociado al id que recibimos en el package
		Optional<Hospital> returnedObject = hospitalRepository.findById(newDoctorData.getHospitalId());
		Hospital newHospital = returnedObject.get();
		//Buscamos el hospital asignado actualmente al doctor, y vemos si cambió o no
		//En caso de que hay cambiado, procedemos a borrar la relación actual y generamos la nueva
		DoctorHospital relation = d.getDoctorHospital().get(0);
		if(relation.getHospital().getId() != newDoctorData.getHospitalId()) {
			//Borramos la relación vieja
			doctorHospitalRepository.delete(relation);
			
			//Armamos la relación con el nuevo hospital
			//Generamos un objeto con la relación Doctor - Hospital Nuevo
			relation = new DoctorHospital();
			relation.setDoctor(d);
			relation.setHospital(newHospital);
			relation.setDoctorLicenseNumber(newDoctorData.getLicenseNumber().toString());
			//Persistimos la relación
		}
		doctorHospitalRepository.save(relation);
		
		DoctorModel newDoctorModel = new DoctorModel(d, relation);
		return newDoctorModel;

	}
	
	@Override
	public DoctorModel deleteDoctor(DoctorPostPackage deletedDoctorData){		 

		//El usuario lo borramos siempre, es lo que me marca que va a pasar a estar "deshabilitado"
		//Sin embargo lo hacemos más adelante cuando definimos cómo avanzar con el borrado
		User deletedDoctorUser = userRepository.findByUsername(deletedDoctorData.getDni().toString());	
		
			
		//Antes de avanzar con el borrado de los registros de la persona como médico y como paciente
		//vamos a ver si tienen consultas médicas en las que hayan participado con uno u otro rol
		Doctor d = doctorRepository.findByDni(deletedDoctorData.getDni());
		Patient p = patientRepository.findByDni(deletedDoctorData.getDni());
		List<MedicalConsultation> mc_list_as_doctor = medicalRepository.findAllByDoctorId(d.getId());
		List<MedicalConsultation> mc_list_as_patient = medicalRepository.findByPatientId(p.getId());
		
		DoctorModel deletedDoctorModel = new DoctorModel(d);
		//Si no tenemos consultas médicas generadas de parte del usuario actuando en rol de doctor,
		//borramos ell registro de doctor y el hospital y matricula asignados
		if (mc_list_as_doctor.isEmpty() && mc_list_as_patient.isEmpty()) {
			List<DoctorHospital> dh_list = d.getDoctorHospital();
			
			//Validamos por las dudas, pero siempre debería haber un registro
			if(!dh_list.isEmpty()) {
				deletedDoctorModel.setHospitalLicenseData(dh_list.get(0));
				doctorHospitalRepository.delete(dh_list.get(0));
			}
			
			userRepository.delete(deletedDoctorUser);
			doctorRepository.delete(d);
			patientRepository.delete(p);
			
			return deletedDoctorModel;
		} else {
			//Solamente borramos el usuario y lanzamos el mensaje de error para mostrar en el front
			userRepository.delete(deletedDoctorUser);
			throw new IllegalArgumentException("Existen consultas médicas asociadas al usuario realizadas como médico y/o paciente, "
					+ "se mantendrán almacenados los datos y se eliminará el usuario");
		}

	}
	
	//////////////////////////////////////////
	
	@Override
	@Transactional
	public DoctorModel getDoctorByDni(Integer dni) {
		DoctorModel dm = new DoctorModel();
		Doctor d = userRepository.findDoctorByDni(dni.toString());
		dm.setDni(d.getDni());
		dm.setName(d.getName());
		dm.setSurname(d.getSurname());

		return dm;
	}

	@Override
	public DoctorModel getDoctorById(Long id) {
		DoctorModel dm = new DoctorModel();
		Doctor d = doctorRepository.findById(id).get();
		dm.setDni(d.getDni());
		dm.setName(d.getName());
		dm.setSurname(d.getSurname());

		return dm;
	}

	@Override
	public List<DoctorPatientModel> getChronicPatientListByDoctorIdAndHospitalId(Long doctorId, Long hospitalId) {
		List<PatientChronicDisease> patientList = chronicService.getChronicDiseaseListByDoctorIdAndHospitalId(doctorId, hospitalId);
		List<DoctorPatientModel> patientModelList = new ArrayList<>();
		patientList.forEach(chronicPatient -> {
			DoctorPatientModel dpm = new DoctorPatientModel();
			dpm.setConsultationDate(chronicPatient.getInsertDateString());
			dpm.setDiagnosis(chronicPatient.getChronicDisease().getDiagnosis().getDescription());
			dpm.setPatientDni(chronicPatient.getPatient().getDni()+"");
			dpm.setPatientName(chronicPatient.getPatient().getSurname()+" "+chronicPatient.getPatient().getName());
			
			if(chronicService.hasChronicAssignment(chronicPatient.getId())) {
				dpm.setHasChronicAssignment("SI");
			}else {
				dpm.setHasChronicAssignment("NO");
			}
			
			patientModelList.add(dpm);
			
			//Collections.sort(list, c);
		});
		Collections.sort(patientModelList);
		return patientModelList;
	}

}

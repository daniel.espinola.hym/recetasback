package com.unlam.recetasapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unlam.recetasapp.entity.Pharmacist;
import com.unlam.recetasapp.entity.PharmacistPharmacy;
import com.unlam.recetasapp.repository.PharmacistPharmacyRepository;

@Service
public class PharmacistPharmacyServiceImpl implements PharmacistPharmacyService {
	
	@Autowired
	PharmacistPharmacyRepository pharmacistPharmacyRepository;

	@Override
	public PharmacistPharmacy getPharmacistPharmacy(Pharmacist pharmacist) {
		return pharmacistPharmacyRepository.findByPharmacist(pharmacist);
	}

}

package com.unlam.recetasapp.service;

import java.util.List;

import com.unlam.recetasapp.entity.ChronicAssignment;
import com.unlam.recetasapp.entity.PatientChronicDisease;
import com.unlam.recetasapp.model.ChronicAssignmentModel;
import com.unlam.recetasapp.model.PatientChronicDiseaseModel;

public interface ChronicService {
	public PatientChronicDisease saveStatusChronic(PatientChronicDiseaseModel status);
	public ChronicAssignment saveAssignment(ChronicAssignmentModel status);
	public List<PatientChronicDisease> getChronicDiseaseListByPatientId(Long patientId);
	public List<PatientChronicDisease> getChronicDiseaseListByDoctorIdAndHospitalId(Long doctorId, Long hospitalId);
	public List<ChronicAssignmentModel> getChronicAssignmentListByPatientId(Long patientId);
	public void testClock();
	public boolean hasChronicAssignment(Long patientChronicDiseaseId);
}

package com.unlam.recetasapp.service;

import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.unlam.recetasapp.entity.Doctor;
import com.unlam.recetasapp.entity.Hospital;
import com.unlam.recetasapp.entity.Patient;
import com.unlam.recetasapp.entity.Pharmacist;
import com.unlam.recetasapp.entity.User;
import com.unlam.recetasapp.entity.WaitingRoom;
import com.unlam.recetasapp.model.PatientModel;
import com.unlam.recetasapp.model.PatientPostPackage;
import com.unlam.recetasapp.model.WaitingRoomPostPackage;
import com.unlam.recetasapp.model.WaitingRoomResponsePackage;
import com.unlam.recetasapp.repository.DoctorRepository;
import com.unlam.recetasapp.repository.PatientRepository;
import com.unlam.recetasapp.repository.PharmacistRepository;
import com.unlam.recetasapp.repository.UserRepository;
import com.unlam.recetasapp.repository.WaitingRoomRepository;

@Service
public class PatientServiceImpl implements PatientService{
	
	Logger logger = LoggerFactory.getLogger(PatientServiceImpl.class);

	@Autowired
	PatientRepository patientRepository;
	
	@Autowired
	UserRepository userRepository;
	
	@Autowired
	PasswordEncoder passwordEncoder;
	
	@Autowired
	DoctorRepository doctorRepository;
	
	@Autowired
	PharmacistRepository pharmacistRepository;
	@Autowired
	WaitingRoomRepository waitingRoomRepository;
	
	
	@Override
	@Transactional
	public Patient getPatientByDni(Integer dni) {
		Patient patient = patientRepository.findByDni(dni);
		return patient;
	}
	
	@Override
	public List<PatientModel> getAllPatients(){
		ArrayList<Patient> patientList = new ArrayList<Patient>(patientRepository.findAll());
		ArrayList<PatientModel> patientModelList = new ArrayList<PatientModel>();
		
		patientList.stream().forEach(pa -> {
			PatientModel pm = new PatientModel();
			pm.setDni(pa.getDni());
			pm.setName(pa.getName());
			pm.setSurname(pa.getSurname());
			pm.setBirthDate(new SimpleDateFormat("dd/MM/yyyy").format(pa.getBirthdate()));
			pm.setDniType(pa.getDnitype());
			
			Doctor d = doctorRepository.findByDni(pa.getDni());
			pm.setIsDoctor(d != null ? true : false);
			Pharmacist ph = pharmacistRepository.findByDni(pa.getDni());
			pm.setIsPharmacist(ph != null ? true : false);
			User us = userRepository.findByUsername(pa.getDni().toString());
			pm.setIsEnabled(us != null ? true : false);
			
			patientModelList.add(pm);
		});
		
		return patientModelList;
	}
	
	@Override
	public PatientModel saveNewPatient(PatientPostPackage newPatientData) throws DataIntegrityViolationException{
		
		Patient newPatientEntity = new Patient();
		newPatientEntity.setDni(newPatientData.getDni());
		newPatientEntity.setName(newPatientData.getName());
		newPatientEntity.setSurname(newPatientData.getSurname());
		newPatientEntity.setDnitype(newPatientData.getDniType());
		
		Date birth = null;
		try {
			birth = new SimpleDateFormat("dd/MM/yyyy").parse(newPatientData.getBirthDate());
		} catch(Exception ex){
			
		}
		newPatientEntity.setBirthdate(birth);
		newPatientEntity.setInsertdate(newPatientData.getInsertDate());
		
//		try {
			patientRepository.save(newPatientEntity);			
//		}catch(Exception e) {
//			System.out.println("lala");
//			e.printStackTrace();
//		}
		
		//Ahora generamos el usuario
		User newPatientUser = new User();
		newPatientUser.setPatient(newPatientEntity);
		newPatientUser.setUsername(newPatientEntity.getDni().toString());
		newPatientUser.setRole("[\"ROLE_PATIENT\"]");
		String passwordHash = passwordEncoder.encode(newPatientData.getPassword());
		newPatientUser.setPassword(passwordHash);
		userRepository.save(newPatientUser);
		
		PatientModel newPatientModel = new PatientModel();
		newPatientModel.setDni(newPatientEntity.getDni());
		newPatientModel.setName(newPatientEntity.getName());
		newPatientModel.setSurname(newPatientEntity.getSurname());
		newPatientModel.setDniType(newPatientEntity.getDnitype());
		newPatientModel.setBirthDate(new SimpleDateFormat("dd/MM/yyyy").format(newPatientEntity.getBirthdate()));
		return newPatientModel;
	}
	
	@Override
	public PatientModel updatePatient(PatientPostPackage newPatientData){
		Patient p = patientRepository.findByDni(newPatientData.getDni());
		p.setName(newPatientData.getName());
		p.setSurname(newPatientData.getSurname());
		p.setDnitype(newPatientData.getDniType());
		
		
		Date birth = null;
		try {
			birth = new SimpleDateFormat("dd/MM/yyyy").parse(newPatientData.getBirthDate());
		} catch(Exception ex){
			
		}
		p.setBirthdate(birth);
		
		patientRepository.save(p);
		
		User newPatientUser = userRepository.findByUsername(newPatientData.getDni().toString());
		String passwordHash = passwordEncoder.encode(newPatientData.getPassword());
		newPatientUser.setPassword(passwordHash);
		userRepository.save(newPatientUser);
		
		PatientModel newPatientModel = new PatientModel();
		newPatientModel.setDni(p.getDni());
		newPatientModel.setName(p.getName());
		newPatientModel.setSurname(p.getSurname());
		newPatientModel.setDniType(p.getDnitype());
		newPatientModel.setBirthDate(new SimpleDateFormat("dd/MM/yyyy").format(p.getBirthdate()));
		return newPatientModel;

	}
	
	@Override
	public PatientModel deletePatient(PatientPostPackage newPatientData) {
		User newPatientUser = userRepository.findByUsername(newPatientData.getDni().toString());
		userRepository.delete(newPatientUser);
		
		Patient p = patientRepository.findByDni(newPatientData.getDni());
		patientRepository.delete(p);
		
		PatientModel newPatientModel = new PatientModel();
		newPatientModel.setDni(p.getDni());
		newPatientModel.setName(p.getName());
		newPatientModel.setSurname(p.getSurname());
		newPatientModel.setDniType(p.getDnitype());
		newPatientModel.setBirthDate(new SimpleDateFormat("dd/MM/yyyy").format(p.getBirthdate()));
		return newPatientModel;
	}
	
	@Override
	@Transactional
	public WaitingRoomResponsePackage addPatientToWaitingRoom(WaitingRoomPostPackage waitingRoomPackage) throws Exception {
		Patient patient = getPatientByDni(waitingRoomPackage.getDni().intValue());
		
		if(patient == null) {
			throw new Exception("El paciente ingresado no está dado de alta en el sistema");
		}
		
		PatientModel patientModel = new PatientModel(patient);
		
		
		WaitingRoom waitingRoom = null;
		WaitingRoomResponsePackage waitingRoomResponsePackage = null;
		
		if(!patientIsInWaitingRoom(waitingRoomPackage.getDni(), waitingRoomPackage.getIdHospital())) {
			waitingRoom = new WaitingRoom();
			waitingRoom.setDni(waitingRoomPackage.getDni());
			waitingRoom.setEntranceDate(new Date());
			waitingRoom.setHospital(new Hospital(waitingRoomPackage.getIdHospital()));
			waitingRoom = waitingRoomRepository.save(waitingRoom);
			
			
			waitingRoomResponsePackage = new WaitingRoomResponsePackage();
			waitingRoomResponsePackage.setWaitingRoom(waitingRoom);
			waitingRoomResponsePackage.setPatientFullName(patientModel.getFullName());
			
		}else {
			throw new Exception("El paciente " + patientModel.getFullName() + " ya se encuentra agregado a la sala de espera");
		}
		
		
		return waitingRoomResponsePackage;
	}

	@Override
	public Boolean patientIsInWaitingRoom(Long dni, Long idHospital) {
		
		Date startOfCurrentDay = atStartOfDay(new Date());
		Date endOfCurrentDay = atEndOfDay(new Date());
		
		WaitingRoom waitingRoom = waitingRoomRepository.findByDniAndHospitalAndEntranceDateBetween(dni, new Hospital(idHospital), startOfCurrentDay, endOfCurrentDay);
		
		if(waitingRoom != null) {
			return true;
		}else {
			return false;
		}
		
	}
	
	public static Date atStartOfDay(Date date) {
	    LocalDateTime localDateTime = dateToLocalDateTime(date);
	    LocalDateTime startOfDay = localDateTime.with(LocalTime.MIN);
	    return localDateTimeToDate(startOfDay);
	}

	public static Date atEndOfDay(Date date) {
	    LocalDateTime localDateTime = dateToLocalDateTime(date);
	    LocalDateTime endOfDay = localDateTime.with(LocalTime.MAX);
	    return localDateTimeToDate(endOfDay);
	}

	private static LocalDateTime dateToLocalDateTime(Date date) {
	    return LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault());
	}

	private static Date localDateTimeToDate(LocalDateTime localDateTime) {
	    return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
	}

}

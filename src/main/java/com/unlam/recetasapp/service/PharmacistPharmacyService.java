package com.unlam.recetasapp.service;

import com.unlam.recetasapp.entity.Pharmacist;
import com.unlam.recetasapp.entity.PharmacistPharmacy;

public interface PharmacistPharmacyService {
	
	public PharmacistPharmacy getPharmacistPharmacy(Pharmacist pharmacist);

}

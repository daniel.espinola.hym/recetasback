package com.unlam.recetasapp.service;

import java.util.List;

import com.unlam.recetasapp.entity.Diagnosis;
import com.unlam.recetasapp.model.DiagnosisModel;

public interface DiagnosisService {
	public List<Diagnosis> getDiagnosisList();
	public List<DiagnosisModel> getDiagnosisModelList();
	public List<Diagnosis> getChronicDiagnosisList();
	public DiagnosisModel saveDiagnosis(DiagnosisModel dm);
}

package com.unlam.recetasapp.service;

import java.util.List;

import com.unlam.recetasapp.entity.Pharmacist;
import com.unlam.recetasapp.model.PharmacistModel;
import com.unlam.recetasapp.model.PharmacistPostPackage;

public interface PharmacistService {
	
	//Nuevos
	public List<PharmacistModel> getAllPharmacists();
	public PharmacistModel saveNewPharmacist(PharmacistPostPackage newPharmacistData);
	public PharmacistModel updatePharmacist(PharmacistPostPackage newPharmacistData);
	public PharmacistModel deletePharmacist(PharmacistPostPackage newPharmacistData);
	public Pharmacist getPharmacist(Integer dni);

}

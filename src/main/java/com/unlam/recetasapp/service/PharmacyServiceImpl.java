package com.unlam.recetasapp.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unlam.recetasapp.entity.Locality;
import com.unlam.recetasapp.entity.PharmacistPharmacy;
import com.unlam.recetasapp.entity.Pharmacy;
import com.unlam.recetasapp.entity.PharmacyPrescription;
import com.unlam.recetasapp.model.PharmacyModel;
import com.unlam.recetasapp.model.PharmacyPostPackage;
import com.unlam.recetasapp.repository.LocalityRepository;
import com.unlam.recetasapp.repository.PharmacistPharmacyRepository;
import com.unlam.recetasapp.repository.PharmacyPrescriptionRepository;
import com.unlam.recetasapp.repository.PharmacyRepository;
import com.unlam.recetasapp.repository.ProvinceRepository;

@Service
public class PharmacyServiceImpl implements PharmacyService {
	
	@Autowired
	PharmacyRepository pharmacyRepository;
	
	@Autowired
	LocalityRepository localityRepository;
	
	@Autowired
	ProvinceRepository provinceRepository;
	
	@Autowired
	PharmacyPrescriptionRepository pharmacyPrescriptionRepository;
	
	@Autowired
	PharmacistPharmacyRepository pharmacistPharmacyRepository;
	
	
	//Nuevos
	public List<PharmacyModel> getAllPharmacies(){
		ArrayList<Pharmacy> pharmacyList = new ArrayList<Pharmacy>(pharmacyRepository.findAll());
		ArrayList<PharmacyModel> pharmacyModelList = new ArrayList<PharmacyModel>();
		
		pharmacyList.stream().forEach(pharmacy -> {
			PharmacyModel pm = new PharmacyModel(pharmacy);
			pharmacyModelList.add(pm);
		});
		
		return pharmacyModelList;
	}
	
	@Override
	public PharmacyModel saveNewPharmacy(PharmacyPostPackage newPharmacyData){
		
		Pharmacy newPharmacyEntity = new Pharmacy();
		newPharmacyEntity.setName(newPharmacyData.getName());
		newPharmacyEntity.setAddress(newPharmacyData.getAddress());
		newPharmacyEntity.setAddressNumber(newPharmacyData.getAddressNumber());
		
		Optional<Locality> returnedObject = localityRepository.findById(newPharmacyData.getLocalityId());
		Locality loc = returnedObject.get();
		
		newPharmacyEntity.setLocality(loc);
		pharmacyRepository.save(newPharmacyEntity);
		
		PharmacyModel newPharmacyModel = new PharmacyModel(newPharmacyEntity);
		return newPharmacyModel;
	}
	
	@Override
	public PharmacyModel updatePharmacy(PharmacyPostPackage updatedPharmacyData){
		Optional<Pharmacy> returnedObject = pharmacyRepository.findById(updatedPharmacyData.getId());
		
		Pharmacy p = returnedObject.get();
		
		Optional<Locality> returnedObject2 = localityRepository.findById(updatedPharmacyData.getLocalityId());
		Locality loc = returnedObject2.get();
		
		p.setName(updatedPharmacyData.getName());
		p.setAddress(updatedPharmacyData.getAddress());
		p.setAddressNumber(updatedPharmacyData.getAddressNumber());
		p.setLocality(loc);
		
		
		pharmacyRepository.save(p);
		PharmacyModel pm = new  PharmacyModel(p);
		return pm;

	}
	
	public PharmacyModel deletePharmacy(PharmacyPostPackage deletedPharmacyData) {
		
		List<PharmacistPharmacy> list_pp = pharmacistPharmacyRepository.findByPharmacyId(deletedPharmacyData.getId());
		List<PharmacyPrescription> list_pp2 = pharmacyPrescriptionRepository.findByPharmacyId(deletedPharmacyData.getId());
		
		if(!list_pp.isEmpty() || !list_pp2.isEmpty()) {
			throw new IllegalArgumentException("La farmacia tiene farmacéuticos asignados y/o recetas presentadas, no se puede eliminar");
		}
		
		
		Optional<Pharmacy> returnedObject = pharmacyRepository.findById(deletedPharmacyData.getId());
		Pharmacy p = returnedObject.get();
		pharmacyRepository.delete(p);
		
		PharmacyModel deletedPharmacyModel = new PharmacyModel(p);
		return deletedPharmacyModel;
	}

}

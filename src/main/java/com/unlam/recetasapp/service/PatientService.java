package com.unlam.recetasapp.service;

import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;

import com.unlam.recetasapp.entity.Patient;
import com.unlam.recetasapp.entity.WaitingRoom;
import com.unlam.recetasapp.model.PatientModel;
import com.unlam.recetasapp.model.PatientPostPackage;
import com.unlam.recetasapp.model.WaitingRoomPostPackage;
import com.unlam.recetasapp.model.WaitingRoomResponsePackage;

public interface PatientService {

	
	public Patient getPatientByDni(Integer dni);
	
	public List<PatientModel> getAllPatients();
	public PatientModel saveNewPatient(PatientPostPackage newPatientData) throws SQLIntegrityConstraintViolationException;
	public PatientModel updatePatient(PatientPostPackage newPatientData);
	public PatientModel deletePatient(PatientPostPackage newPatientData);
	public WaitingRoomResponsePackage addPatientToWaitingRoom(WaitingRoomPostPackage waitingRoomPackage) throws Exception;

	public Boolean patientIsInWaitingRoom(Long dni, Long idHospital);
}

package com.unlam.recetasapp.service;

import java.util.List;

import com.unlam.recetasapp.model.DrugGenericVarietyPackage;
import com.unlam.recetasapp.model.DrugModel;
import com.unlam.recetasapp.model.DrugsPackage;

public interface DrugService {
	public DrugsPackage getAllDrugs();

	public DrugGenericVarietyPackage getDrugGenericVariety(Long idDrugGeneric);

	public List<DrugModel> getAllDrugByPrescriptionId(Long PrescriptionId);
	
	public void addDrugFromExcelRow(String comercialName, String genericName, String strength, String pharmaceuticalForm);
}

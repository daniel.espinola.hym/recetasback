
package com.unlam.recetasapp.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class ChronicDisease {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;
    
    @ManyToOne(fetch = FetchType.EAGER )
	@JoinColumn(name = "diagnosis_id", nullable = false)
    private Diagnosis diagnosis;
    
    @Column(name = "observation", nullable = true)
    private String observation;
    
    @OneToMany(mappedBy = "chronicDisease", fetch = FetchType.LAZY)
    private List<PatientChronicDisease> patientChronicDisease;

    /**
     * No args constructor for use in serialization
     * 
     */
    public ChronicDisease() {
    }

    /**
     * 
     * @param id
     * @param observation
     * @param diagnosisId
     */
    public ChronicDisease(Long id, Diagnosis diagnosisId, String observation) {
        super();
        this.id = id;
        this.diagnosis = diagnosisId;
        this.observation = observation;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("diagnosis_id")
    public Diagnosis getDiagnosis() {
        return diagnosis;
    }

    @JsonProperty("diagnosis_id")
    public void setDiagnosis(Diagnosis diagnosisId) {
        this.diagnosis = diagnosisId;
    }

    @JsonProperty("observation")
    public String getObservation() {
        return observation;
    }

    @JsonProperty("observation")
    public void setObservation(String observation) {
        this.observation = observation;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("diagnosis", diagnosis).append("observation", observation).toString();
    }

}

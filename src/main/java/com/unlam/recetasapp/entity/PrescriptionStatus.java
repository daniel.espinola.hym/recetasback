
package com.unlam.recetasapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class PrescriptionStatus {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;
	
	@Column(name = "description", nullable = true)
    private String description;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PrescriptionStatus() {
    }

    /**
     * 
     * @param id
     * @param description
     */
    public PrescriptionStatus(Long id, String description) {
        super();
        this.id = id;
        this.description = description;
    }
    
    /**
     * 
     * @param id
     */
    public PrescriptionStatus(Long id) {
        super();
        this.id = id;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("description", description).toString();
    }

}

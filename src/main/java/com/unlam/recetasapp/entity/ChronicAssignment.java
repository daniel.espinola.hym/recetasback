
package com.unlam.recetasapp.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;

@Table(name = "chronic_assignment")
@Entity
public class ChronicAssignment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hospital_id", nullable = false)
	private Hospital hospital;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "doctor_id", nullable = false)
	private Doctor doctor;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "patient_chronic_disease_id", nullable = false)
	@JsonBackReference
	private PatientChronicDisease patientChronicDiseaseId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "prescription_id", nullable = false)
	private Prescription prescription;

	@Column(name = "starter_date")
	private Date starterDate;

	@Column(name = "quantity")
	private int quantity;

	@Column(name = "triggered_quantity")
	private int triggeredQuantity;

	@Column(name = "frequency")
	private String frequency;
	
	@Transient
	private String starterDateString;
	@Transient
	private String diagnosis;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public ChronicAssignment() {
	}

	/**
	 * 
	 * @param prescriptionId
	 * @param id
	 * @param patientId
	 * @param doctorId
	 * @param patientChronicDiseaseId
	 * @param hospitalId
	 */
	public ChronicAssignment(Long id, Hospital hospitalId, Doctor doctorId,
			PatientChronicDisease patientChronicDiseaseId, Prescription prescriptionId) {
		super();
		this.id = id;
		this.hospital = hospitalId;
		this.doctor = doctorId;
		this.patientChronicDiseaseId = patientChronicDiseaseId;
		this.prescription = prescriptionId;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("hospital_id")
	public Hospital getHospital() {
		return hospital;
	}

	@JsonProperty("hospital_id")
	public void setHospital(Hospital hospitalId) {
		this.hospital = hospitalId;
	}

	@JsonProperty("doctor_id")
	public Doctor getDoctor() {
		return doctor;
	}

	@JsonProperty("doctor_id")
	public void setDoctor(Doctor doctorId) {
		this.doctor = doctorId;
	}

	@JsonProperty("patient_chronic_disease_id")
	public PatientChronicDisease getPatientChronicDiseaseId() {
		return patientChronicDiseaseId;
	}

	@JsonProperty("patient_chronic_disease_id")
	public void setPatientChronicDiseaseId(PatientChronicDisease patientChronicDiseaseId) {
		this.patientChronicDiseaseId = patientChronicDiseaseId;
	}

	@JsonProperty("prescription_id")
	public Prescription getPrescription() {
		return prescription;
	}

	@JsonProperty("prescription_id")
	public void setPrescription(Prescription prescriptionId) {
		this.prescription = prescriptionId;
	}

	public Date getStarterDate() {
		return starterDate;
	}

	public void setStarterDate(Date starterDate) {
		this.starterDate = starterDate;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getTriggeredQuantity() {
		return triggeredQuantity;
	}

	public void setTriggeredQuantity(int triggeredQuantity) {
		this.triggeredQuantity = triggeredQuantity;
	}

	public String getFrequency() {
		return frequency;
	}

	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}

	public String getStarterDateString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String returnValue="";
		try {
			returnValue = sdf.format(this.starterDate);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return returnValue;
	}

	public String getDiagnosis() {
		if(this.patientChronicDiseaseId!=null) {
			return patientChronicDiseaseId.getChronicDisease().getDiagnosis().getDescription();
		}
		return "";
	}
	
	
}

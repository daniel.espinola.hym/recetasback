
package com.unlam.recetasapp.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class MedicalConsultation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "hospital_id", nullable = false)
	private Hospital hospital;

	@Column(name = "hospital_id", insertable = false, updatable = false)
	private Long hospitalId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "doctor_id", nullable = false)
	private Doctor doctor;

	@Column(name = "doctor_id", insertable = false, updatable = false)
	private Long doctorId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "patient_id", nullable = false)
	private Patient patient;

	@Column(name = "patient_id", insertable = false, updatable = false)
	private Long patientId;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "chronic_assignment_id", nullable = true)
	private ChronicAssignment chronicAssignment;

	@Column(name = "consultation_date", nullable = false)
	private Date consultationDate;

	@Column(name = "observation", nullable = false)
	private String observation;

	@OneToMany(mappedBy = "medicalConsultation", fetch = FetchType.LAZY)
	List<Prescription> prescriptionList;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public MedicalConsultation() {
	}

	/**
	 * 
	 * @param id
	 * @param patientId
	 * @param observation
	 * @param consultationDate
	 * @param doctorId
	 * @param chronicAssignment
	 * @param hospitalId
	 */
	public MedicalConsultation(Long id, Hospital hospitalId, Doctor doctorId, Patient patientId,
			ChronicAssignment chronicAssignment, Date consultationDate, String observation) {
		super();
		this.id = id;
		this.hospital = hospitalId;
		this.doctor = doctorId;
		this.patient = patientId;
		this.chronicAssignment = chronicAssignment;
		this.consultationDate = consultationDate;
		this.observation = observation;
	}

	public MedicalConsultation(Hospital hospitalId, Doctor doctorId, Patient patientId,
			ChronicAssignment chronicAssignment, Date consultationDate, String observation) {
		super();
		this.hospital = hospitalId;
		this.doctor = doctorId;
		this.patient = patientId;
		this.chronicAssignment = chronicAssignment;
		this.consultationDate = consultationDate;
		this.observation = observation;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("hospital_id")
	public void setHospitalId(Hospital hospitalId) {
		this.hospital = hospitalId;
	}

	@JsonProperty("doctor_id")
	public Doctor getDoctor() {
		return doctor;
	}

	@JsonProperty("doctor_id")
	public void setDoctor(Doctor doctorId) {
		this.doctor = doctorId;
	}

	@JsonProperty("patient_id")
	public Patient getPatient() {
		return patient;
	}

	@JsonProperty("patient_id")
	public void setPatient(Patient patientId) {
		this.patient = patientId;
	}

	@JsonProperty("chronic_assignment")
	public ChronicAssignment getChronicAssignment() {
		return chronicAssignment;
	}

	@JsonProperty("chronic_assignment")
	public void setChronicAssignment(ChronicAssignment chronicAssignment) {
		this.chronicAssignment = chronicAssignment;
	}

	@JsonProperty("consultation_date")
	public Date getConsultationDate() {
		return consultationDate;
	}

	@JsonProperty("consultation_date")
	public void setConsultationDate(Date consultationDate) {
		this.consultationDate = consultationDate;
	}

	@JsonProperty("observation")
	public String getObservation() {
		return observation;
	}

	@JsonProperty("observation")
	public void setObservation(String observation) {
		this.observation = observation;
	}

	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	public List<Prescription> getPrescriptionList() {
		return prescriptionList;
	}

	public void setPrescriptionList(List<Prescription> prescriptionList) {
		this.prescriptionList = prescriptionList;
	}

	public Long getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(Long doctorId) {
		this.doctorId = doctorId;
	}

	public Long getPatientId() {
		return patientId;
	}

	public void setPatientId(Long patientId) {
		this.patientId = patientId;
	}

	public void setHospitalId(Long hospitalId) {
		this.hospitalId = hospitalId;
	}

	public Long getHospitalId() {
		return hospitalId;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("hospitalId", hospital).append("doctorId", doctor)
				.append("patientId", patient).append("chronicAssignment", chronicAssignment)
				.append("consultationDate", consultationDate).append("observation", observation).toString();
	}

}

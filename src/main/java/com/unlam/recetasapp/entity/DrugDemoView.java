package com.unlam.recetasapp.entity;



public class DrugDemoView {

	
	
	private Integer drugId;
	private String name;
	private String strength;
	
	public DrugDemoView(Integer drugId, String name, String strength) {
		super();
		this.drugId = drugId;
		this.name = name;
		this.strength = strength;
	}

	public Integer getDrugId() {
		return drugId;
	}

	public void setDrugId(Integer drugId) {
		this.drugId = drugId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}
	
	
}

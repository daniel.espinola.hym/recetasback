package com.unlam.recetasapp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class WaitingRoom {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "dni", nullable = false)
	private Long dni;
	
	@ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "hospital_id", nullable = true)
	private Hospital hospital;
	
	@Column(name = "entrance_date", nullable = false)
	private Date entranceDate;
	
	@Column(name = "exit_date", nullable = true)
	private Date exitDate;
	
	
	public WaitingRoom() {
		super();
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public Long getDni() {
		return dni;
	}
	
	public void setDni(Long dni) {
		this.dni = dni;
	}
	
	public Hospital getHospital() {
		return hospital;
	}
	
	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}
	
	public Date getEntranceDate() {
		return entranceDate;
	}
	
	public void setEntranceDate(Date entranceDate) {
		this.entranceDate = entranceDate;
	}
	
	public Date getExitDate() {
		return exitDate;
	}
	
	public void setExitDate(Date exitDate) {
		this.exitDate = exitDate;
	}
	
	
}

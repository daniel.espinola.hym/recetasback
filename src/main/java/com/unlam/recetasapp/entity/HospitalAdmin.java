package com.unlam.recetasapp.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class HospitalAdmin {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;
	
	@Column(name = "dni", nullable = false)
    private Integer dni;
    
	@Column(name = "dnitype", nullable = false)
    private Integer dnitype;
    
	@Column(name = "name", nullable = false)
    private String name;
    
	@Column(name = "surname", nullable = false)
    private String surname;
    
	@Column(name = "birthdate", nullable = false)
    private Date birthdate;
    
	@Column(name = "insertdate", nullable = false)
    private Date insertdate;
	
	@OneToMany(mappedBy = "hospitalAdmin", fetch = FetchType.LAZY)
	private List<HospitalAdminRelation> adminHospitalList;
	

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("dni")
    public Integer getDni() {
        return dni;
    }

    @JsonProperty("dni")
    public void setDni(Integer dni) {
        this.dni = dni;
    }

    @JsonProperty("dnitype")
    public Integer getDnitype() {
        return dnitype;
    }

    @JsonProperty("dnitype")
    public void setDnitype(Integer dnitype) {
        this.dnitype = dnitype;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("surname")
    public String getSurname() {
        return surname;
    }

    @JsonProperty("surname")
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @JsonProperty("birthdate")
    public Date getBirthdate() {
        return birthdate;
    }

    @JsonProperty("birthdate")
    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    @JsonProperty("insertdate")
    public Date getInsertdate() {
        return insertdate;
    }

    @JsonProperty("insertdate")
    public void setInsertdate(Date insertdate) {
        this.insertdate = insertdate;
    }

	public List<HospitalAdminRelation> getAdminHospitalList() {
		return adminHospitalList;
	}

	public void setAdminHospitalList(List<HospitalAdminRelation> adminHospitalList) {
		this.adminHospitalList = adminHospitalList;
	}

    


}

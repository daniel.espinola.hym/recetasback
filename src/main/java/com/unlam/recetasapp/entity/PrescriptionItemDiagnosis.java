
package com.unlam.recetasapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class PrescriptionItemDiagnosis {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;
    
	@ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "diagnosis_id", nullable = false)
    private Diagnosis diagnosis;
    
	@ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "prescription_item_id", nullable = false)
    private PrescriptionItem prescriptionItem;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PrescriptionItemDiagnosis() {
    }

    /**
     * 
     * @param id
     * @param prescriptionItemId
     * @param diagnosisId
     */
    public PrescriptionItemDiagnosis(Long id, Diagnosis diagnosisId, PrescriptionItem prescriptionItemId) {
        super();
        this.id = id;
        this.diagnosis = diagnosisId;
        this.prescriptionItem = prescriptionItemId;
    }

    public PrescriptionItemDiagnosis(Long diagnosisId) {
		diagnosis = new Diagnosis(diagnosisId);
	}

	@JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("diagnosis_id")
    public Diagnosis getDiagnosis() {
        return diagnosis;
    }

    @JsonProperty("diagnosis_id")
    public void setDiagnosis(Diagnosis diagnosisId) {
        this.diagnosis = diagnosisId;
    }

    @JsonProperty("prescription_item_id")
    public PrescriptionItem getPrescriptionItem() {
        return prescriptionItem;
    }

    @JsonProperty("prescription_item_id")
    public void setPrescriptionItem(PrescriptionItem prescriptionItemId) {
        this.prescriptionItem = prescriptionItemId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("diagnosis", diagnosis).append("prescriptionItem", prescriptionItem).toString();
    }

}


package com.unlam.recetasapp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

@Table(name = "pharmacy_proscription")
@Entity
public class PharmacyPrescription {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;
    
    @ManyToOne
    @JoinColumn(name = "pharmacy_id", nullable = false)
    private Pharmacy pharmacy;

    @ManyToOne
    @JoinColumn(name = "proscription_id", nullable = false)
    private Prescription prescription;
        
    @ManyToOne
    @JoinColumn(name = "pharmacist_id", nullable = false)
    private Pharmacist pharmacist;
    
    @Column(name = "insertdate", nullable = false)
    private Date insertdate;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PharmacyPrescription() {
    }

    /**
     * 
     * @param id
     * @param insertdate
     * @param pharmacistId
     * @param proscriptionId
     * @param pharmacyId
     */
    public PharmacyPrescription(Long id, Pharmacy pharmacyId, Prescription proscriptionId, Pharmacist pharmacistId, Date insertdate) {
        super();
        this.id = id;
        this.pharmacy = pharmacyId;
        this.prescription = proscriptionId;
        this.pharmacist = pharmacistId;
        this.insertdate = insertdate;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("pharmacy_id")
    public Pharmacy getPharmacy() {
        return pharmacy;
    }

    @JsonProperty("pharmacy_id")
    public void setPharmacy(Pharmacy pharmacyId) {
        this.pharmacy = pharmacyId;
    }

    @JsonProperty("proscription_id")
    public Prescription getPrescription() {
        return prescription;
    }

    @JsonProperty("proscription_id")
    public void setPrescription(Prescription proscriptionId) {
        this.prescription = proscriptionId;
    }

    @JsonProperty("pharmacist_id")
    public Pharmacist getPharmacist() {
        return pharmacist;
    }

    @JsonProperty("pharmacist_id")
    public void setPharmacist(Pharmacist pharmacistId) {
        this.pharmacist = pharmacistId;
    }

    @JsonProperty("insertdate")
    public Date getInsertdate() {
        return insertdate;
    }

    @JsonProperty("insertdate")
    public void setInsertdate(Date insertdate) {
        this.insertdate = insertdate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("pharmacyId", pharmacy).append("proscriptionId", prescription).append("pharmacistId", pharmacist).append("insertdate", insertdate).toString();
    }

}

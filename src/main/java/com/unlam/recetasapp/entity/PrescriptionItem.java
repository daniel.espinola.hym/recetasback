
package com.unlam.recetasapp.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class PrescriptionItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;
    
	@Column(name = "observation", nullable = true)
    private String observation;
    
    @ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "prescription_id", nullable = false)
    private Prescription prescription;
    
    @OneToMany(mappedBy = "prescriptionItem", fetch = FetchType.LAZY, cascade =  CascadeType.ALL)
    private List<PrescriptionItemDiagnosis> prescriptionItemDiagnosis;
    
    @OneToMany(mappedBy = "prescriptionItem", fetch = FetchType.LAZY, cascade =  CascadeType.ALL)
    private List<PrescriptionItemDrugComercial> prescriptionItemDrugComercial;
    
    @OneToMany(mappedBy = "prescriptionItem", fetch = FetchType.LAZY, cascade =  CascadeType.ALL)
    private List<PrescriptionItemDrugGeneric> prescriptionItemDrugGeneric;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PrescriptionItem() {
    }

    /**
     * 
     * @param prescriptionId
     * @param id
     * @param observation
     */
    public PrescriptionItem(Long id, String observation, Prescription prescriptionId) {
        super();
        this.id = id;
        this.observation = observation;
        this.prescription = prescriptionId;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("observation")
    public String getObservation() {
        return observation;
    }

    @JsonProperty("observation")
    public void setObservation(String observation) {
        this.observation = observation;
    }

    @JsonProperty("prescription_id")
    public Prescription getPrescription() {
        return prescription;
    }

    @JsonProperty("prescription_id")
    public void setPrescription(Prescription prescriptionId) {
        this.prescription = prescriptionId;
    }
    
   
    public List<PrescriptionItemDiagnosis> getPrescriptionItemDiagnosis() {
		return prescriptionItemDiagnosis;
	}

	public void setPrescriptionItemDiagnosis(List<PrescriptionItemDiagnosis> prescriptionItemDiagnosis) {
		this.prescriptionItemDiagnosis = prescriptionItemDiagnosis;
	}

	public List<PrescriptionItemDrugComercial> getPrescriptionItemDrugComercial() {
		return prescriptionItemDrugComercial;
	}

	public void setPrescriptionItemDrugComercial(List<PrescriptionItemDrugComercial> prescriptionItemDrugComercial) {
		this.prescriptionItemDrugComercial = prescriptionItemDrugComercial;
	}
	
	

	public List<PrescriptionItemDrugGeneric> getPrescriptionItemDrugGeneric() {
		return prescriptionItemDrugGeneric;
	}

	public void setPrescriptionItemDrugGeneric(List<PrescriptionItemDrugGeneric> prescriptionItemDrugGeneric) {
		this.prescriptionItemDrugGeneric = prescriptionItemDrugGeneric;
	}

	@Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("observation", observation).append("prescription", prescription).toString();
    }

}

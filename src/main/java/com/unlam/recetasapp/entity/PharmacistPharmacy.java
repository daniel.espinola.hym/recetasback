
package com.unlam.recetasapp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class PharmacistPharmacy {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;
	
    @ManyToOne
    @JoinColumn(name = "pharmacist_id", nullable = false)
    private Pharmacist pharmacist;
    
    @ManyToOne
    @JoinColumn(name = "pharmacy_id", nullable = false)
    private Pharmacy pharmacy;
    
    @Column(name = "insertdate", nullable = false)
    private Date insertdate;

    
    
    /**
     * No args constructor for use in serialization
     * 
     */
    public PharmacistPharmacy() {
    }

    /**
     * 
     * @param id
     * @param insertdate
     * @param pharmacistId
     * @param pharmacyId
     */
    public PharmacistPharmacy(Long id, Pharmacist pharmacistId, Pharmacy pharmacyId, Date insertdate) {
        super();
        this.id = id;
        this.pharmacist = pharmacistId;
        this.pharmacy = pharmacyId;
        this.insertdate = insertdate;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("pharmacist_id")
    public Pharmacist getPharmacist() {
        return pharmacist;
    }

    @JsonProperty("pharmacist_id")
    public void setPharmacist(Pharmacist pharmacistId) {
        this.pharmacist = pharmacistId;
    }

    @JsonProperty("pharmacy_id")
    public Pharmacy getPharmacy() {
        return pharmacy;
    }

    @JsonProperty("pharmacy_id")
    public void setPharmacy(Pharmacy pharmacyId) {
        this.pharmacy = pharmacyId;
    }

    @JsonProperty("insertdate")
    public Date getInsertdate() {
        return insertdate;
    }

    @JsonProperty("insertdate")
    public void setInsertdate(Date insertdate) {
        this.insertdate = insertdate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("pharmacist", pharmacist).append("pharmacy", pharmacy).append("insertdate", insertdate).toString();
    }

}

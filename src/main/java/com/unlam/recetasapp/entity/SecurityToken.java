package com.unlam.recetasapp.entity;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Transient;

import com.unlam.recetasapp.model.TokenStatus;

@Entity
public class SecurityToken {
	
	@Transient
	static final long ONE_MINUTE_IN_MILLIS = 60000;
	
	@Transient
	static final int DURATION_TOKEN_IN_MINUTES = 3;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "dni", nullable = false)
	private Integer dni;

	@Column(name = "otp_code", nullable = false)
	private Integer otpCode;

	@Column(name = "date_created", nullable = false)
	private Date dateCreated;

	@Column(name = "date_expiration", nullable = false)
	private Date dateExpiration;
	
	@Column(name = "token_status", nullable = false)
	@Enumerated(EnumType.STRING)
	private TokenStatus tokenStatus;

	@OneToMany //(mappedBy = "securityToken", fetch = FetchType.LAZY)
	@JoinColumn(name="security_token_id", referencedColumnName="id")
	private List<Prescription> prescriptions;
	
	
	

	public SecurityToken() {
		super();
	}

	public SecurityToken(Integer dni, Long prescriptionId) {
		this.dni = dni;
		prescriptions = new ArrayList<Prescription>();
		prescriptions.add(new Prescription(prescriptionId));
		otpCode = new Random().nextInt(900000) + 100000;
		dateCreated = new Date();
		tokenStatus = TokenStatus.SIN_UTILIZAR;		
		Calendar date = Calendar.getInstance();
		long t = date.getTimeInMillis();
		Date afterAddingThreeMins=new Date(t + (DURATION_TOKEN_IN_MINUTES * ONE_MINUTE_IN_MILLIS));
		dateExpiration = afterAddingThreeMins;		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getDni() {
		return dni;
	}

	public void setDni(Integer dni) {
		this.dni = dni;
	}

	public Integer getOtpCode() {
		return otpCode;
	}

	public void setOtpCode(Integer otpCode) {
		this.otpCode = otpCode;
	}

	public Date getDateCreated() {
		return dateCreated;
	}

	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}
	
	public Date getDateExpiration() {
		return dateExpiration;
	}

	public void setDateExpiration(Date dateExpiration) {
		this.dateExpiration = dateExpiration;
	}

	public List<Prescription> getPrescriptions() {
		return prescriptions;
	}

	public void setPrescriptions(List<Prescription> prescriptions) {
		this.prescriptions = prescriptions;
	}

	public TokenStatus getTokenStatus() {
		return tokenStatus;
	}

	public void setTokenStatus(TokenStatus tokenStatus) {
		this.tokenStatus = tokenStatus;
	}
	
	
	
}

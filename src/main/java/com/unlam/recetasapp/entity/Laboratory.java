
package com.unlam.recetasapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Laboratory {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;
	
	@Column(name = "name", nullable = false)
    private String name;
    
	@Column(name = "address", nullable = false)
    private String address;
    
	@Column(name = "addressnumber", nullable = false)
    private Integer addressNumber;
    
	@ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "locality_id", nullable = false)
    private Locality locality;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Laboratory() {
    }

    /**
     * 
     * @param id
     * @param address
     * @param addressnumber
     * @param name
     * @param localityId
     */
    public Laboratory(Long id, String name, String address, Integer addressnumber, Locality localityId) {
        super();
        this.id = id;
        this.name = name;
        this.address = address;
        this.addressNumber = addressnumber;
        this.locality = localityId;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("address")
    public String getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(String address) {
        this.address = address;
    }

    @JsonProperty("addressnumber")
    public Integer getAddressNumber() {
        return addressNumber;
    }

    @JsonProperty("addressnumber")
    public void setAddressNumber(Integer addressnumber) {
        this.addressNumber = addressnumber;
    }

    @JsonProperty("locality_id")
    public Locality getLocality() {
        return locality;
    }

    @JsonProperty("locality_id")
    public void setLocality(Locality localityId) {
        this.locality = localityId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("name", name).append("address", address).append("addressnumber", addressNumber).append("localityId", locality).toString();
    }

}

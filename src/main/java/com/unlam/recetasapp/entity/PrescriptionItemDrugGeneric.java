
package com.unlam.recetasapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.unlam.recetasapp.model.DrugView;

@Entity
public class PrescriptionItemDrugGeneric {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "drug_generic_id", nullable = false)
	private DrugGeneric drugGeneric;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "prescription_item_id", nullable = false)
	private PrescriptionItem prescriptionItem;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "pharmaceutical_form_id", nullable = false)
	private PharmaceuticalForm pharmaceuticalForm;

	@JsonProperty("treatment")
	private String treatment;

	@Column(name = "treatment_days")
	private Long treatmentDays;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public PrescriptionItemDrugGeneric() {
	}

	/**
	 * 
	 * @param drugGenericId
	 * @param id
	 * @param prescriptionItemId
	 * @param treatment
	 */

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	public PrescriptionItemDrugGeneric(Long id, DrugGeneric drugGeneric, PrescriptionItem prescriptionItem,
			PharmaceuticalForm pharmaceuticalForm, String treatment, Long days) {
		super();
		this.id = id;
		this.drugGeneric = drugGeneric;
		this.prescriptionItem = prescriptionItem;
		this.pharmaceuticalForm = pharmaceuticalForm;
		this.treatment = treatment;
		this.treatmentDays = days;
	}

	public PrescriptionItemDrugGeneric(DrugView drug) {

	}

	public PrescriptionItemDrugGeneric(DrugGeneric drugGeneric, Long pharmaceuticalFormId, String treatment, Long days) {
		this.drugGeneric = drugGeneric;
		this.pharmaceuticalForm = new PharmaceuticalForm(pharmaceuticalFormId);
		this.treatment = treatment;
		this.treatmentDays = days;
	}

	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("drug_generic_id")
	public DrugGeneric getDrugGeneric() {
		return drugGeneric;
	}

	@JsonProperty("drug_generic_id")
	public void setDrugGeneric(DrugGeneric drugGenericId) {
		this.drugGeneric = drugGenericId;
	}

	@JsonProperty("prescription_item_id")
	public PrescriptionItem getPrescriptionItem() {
		return prescriptionItem;
	}

	@JsonProperty("prescription_item_id")
	public void setPrescriptionItem(PrescriptionItem prescriptionItemId) {
		this.prescriptionItem = prescriptionItemId;
	}

	@JsonProperty("treatment")
	public String getTreatment() {
		return treatment;
	}

	@JsonProperty("treatment")
	public void setTreatment(String treatment) {
		this.treatment = treatment;
	}

	public PharmaceuticalForm getPharmaceuticalForm() {
		return pharmaceuticalForm;
	}

	public void setPharmaceuticalForm(PharmaceuticalForm pharmaceuticalForm) {
		this.pharmaceuticalForm = pharmaceuticalForm;
	}

	public Long getTreatmentDays() {
		return treatmentDays;
	}

	public void setTreatmentDays(Long treatmentDays) {
		this.treatmentDays = treatmentDays;
	}

	@Override
	public String toString() {
		return "PrescriptionItemDrugGeneric [id=" + id + ", drugGeneric=" + drugGeneric + ", prescriptionItem="
				+ prescriptionItem + ", pharmaceuticalForm=" + pharmaceuticalForm + ", treatment=" + treatment + "]";
	}

}

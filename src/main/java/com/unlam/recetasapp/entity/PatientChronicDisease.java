
package com.unlam.recetasapp.entity;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class PatientChronicDisease {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "patient_id", nullable = false)
	@JsonBackReference
	private Patient patient;

	@ManyToOne
	@JoinColumn(name = "chronic_disease_id", nullable = false)
	private ChronicDisease chronicDisease;

	@ManyToOne
	@JoinColumn(name = "doctor_id", nullable = false)
	private Doctor doctor;

	@ManyToOne
	@JoinColumn(name = "hospital_id", nullable = false)
	private Hospital hospital;

	@Column(name = "insertdate", nullable = false)
	private Date insertdate;

	@Transient
	private String insertDateString;
	
	
	public PatientChronicDisease() {
	}

	/**
	 * 
	 * @param id
	 * @param patientId
	 * @param chronicDiseaseId
	 */
	public PatientChronicDisease(Long id, Patient patientId, ChronicDisease chronicDiseaseId) {
		super();
		this.id = id;
		this.patient = patientId;
		this.chronicDisease = chronicDiseaseId;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("patient_id")
	public Patient getPatient() {
		return patient;
	}

	@JsonProperty("patient_id")
	public void setPatient(Patient patientId) {
		this.patient = patientId;
	}

	@JsonProperty("chronic_disease_id")
	public ChronicDisease getChronicDisease() {
		return chronicDisease;
	}

	@JsonProperty("chronic_disease_id")
	public void setChronicDisease(ChronicDisease chronicDiseaseId) {
		this.chronicDisease = chronicDiseaseId;
	}

	public Date getInsertdate() {
		return insertdate;
	}

	public void setInsertdate(Date insertdate) {
		this.insertdate = insertdate;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	public String getInsertDateString() {
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			return sdf.format(this.insertdate);
		}catch (Exception e) {
			return null;
		}
	}	
	

}

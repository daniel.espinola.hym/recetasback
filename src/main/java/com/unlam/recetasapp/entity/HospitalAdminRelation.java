package com.unlam.recetasapp.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class HospitalAdminRelation {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;
	
    @ManyToOne
    @JoinColumn(name = "hospital_admin_id", nullable = false)
    private HospitalAdmin hospitalAdmin;
    
    @ManyToOne
    @JoinColumn(name = "hospital_id", nullable = false)
    private Hospital hospital;
    
    @Column(name = "insertdate", nullable = false)
    private Date insertdate;
    
    
    public HospitalAdminRelation() {
    	
    }

	public HospitalAdminRelation(Long id, HospitalAdmin hospitalAdmin, Hospital hospital, Date insertdate) {
		super();
		this.id = id;
		this.hospitalAdmin = hospitalAdmin;
		this.hospital = hospital;
		this.insertdate = insertdate;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public HospitalAdmin getHospitalAdmin() {
		return hospitalAdmin;
	}

	public void setHospitalAdmin(HospitalAdmin hospitalAdmin) {
		this.hospitalAdmin = hospitalAdmin;
	}

	public Hospital getHospital() {
		return hospital;
	}

	public void setHospital(Hospital hospital) {
		this.hospital = hospital;
	}

	public Date getInsertdate() {
		return insertdate;
	}

	public void setInsertdate(Date insertdate) {
		this.insertdate = insertdate;
	}
    


}

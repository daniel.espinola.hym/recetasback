
package com.unlam.recetasapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Locality {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;
	
	@Column(name = "description", nullable = true)
    private String description;
	
	@ManyToOne(fetch = FetchType.EAGER )
	@JoinColumn(name = "province_id", nullable = false)
    private Province province;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Locality() {
    }

    /**
     * 
     * @param provinceId
     * @param id
     * @param description
     */
    public Locality(Long id, String description, Province provinceId) {
        super();
        this.id = id;
        this.description = description;
        this.province = provinceId;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("description")
    public String getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(String description) {
        this.description = description;
    }

    @JsonProperty("province_id")
    public Province getProvince() {
        return province;
    }

    @JsonProperty("province_id")
    public void setProvince(Province provinceId) {
        this.province = provinceId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("description", description).append("provinceId", province).toString();
    }

}

package com.unlam.recetasapp.entity;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.unlam.recetasapp.model.PharmacistPostPackage;

@Entity
public class Pharmacist {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;
	
	@Column(name = "dni", nullable = false)
    private Integer dni;
    
	@Column(name = "dnitype", nullable = false)
    private Integer dnitype;
    
	@Column(name = "name", nullable = false)
    private String name;
    
	@Column(name = "surname", nullable = false)
    private String surname;
    
	@Column(name = "birthdate", nullable = false)
    private Date birthdate;
    
	@Column(name = "insertdate", nullable = false)
    private Date insertdate;
	
    @OneToMany(mappedBy = "pharmacist")
    private List<PharmacistPharmacy> pharmacistPharmacy;
    
    @OneToMany(mappedBy = "pharmacist")
    private List<PharmacyPrescription> pharmacyPrescription;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Pharmacist() {
    }

    /**
     * 
     * @param id
     * @param insertdate
     * @param dnitype
     * @param name
     * @param birthdate
     * @param surname
     * @param dni
     */
    public Pharmacist(Long id, Integer dni, Integer dnitype, String name, String surname, Date birthdate, Date insertdate) {
        super();
        this.id = id;
        this.dni = dni;
        this.dnitype = dnitype;
        this.name = name;
        this.surname = surname;
        this.birthdate = birthdate;
        this.insertdate = insertdate;
    }
    
    public Pharmacist (PharmacistPostPackage data) {		
		this.dni = data.getDni();
		this.dnitype = data.getDniType();
		this.name = data.getName();
		this.surname = data.getSurname();
		this.insertdate = data.getInsertDate();

		//Si hay algún error al transformar a Date la fecha que recibimos, asignamos por defecto la fecha actual
		try {
			this.birthdate = new SimpleDateFormat("dd/MM/yyyy").parse(data.getBirthDate());
		} catch(Exception ex){
			this.birthdate = new Date();
		}
    	
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("dni")
    public Integer getDni() {
        return dni;
    }

    @JsonProperty("dni")
    public void setDni(Integer dni) {
        this.dni = dni;
    }

    @JsonProperty("dnitype")
    public Integer getDnitype() {
        return dnitype;
    }

    @JsonProperty("dnitype")
    public void setDnitype(Integer dnitype) {
        this.dnitype = dnitype;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("surname")
    public String getSurname() {
        return surname;
    }

    @JsonProperty("surname")
    public void setSurname(String surname) {
        this.surname = surname;
    }

    @JsonProperty("birthdate")
    public Date getBirthdate() {
        return birthdate;
    }

    @JsonProperty("birthdate")
    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    @JsonProperty("insertdate")
    public Date getInsertdate() {
        return insertdate;
    }

    @JsonProperty("insertdate")
    public void setInsertdate(Date insertdate) {
        this.insertdate = insertdate;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("dni", dni).append("dnitype", dnitype).append("name", name).append("surname", surname).append("birthdate", birthdate).append("insertdate", insertdate).toString();
    }

	public List<PharmacistPharmacy> getPharmacistPharmacy() {
		return pharmacistPharmacy;
	}

	public void setPharmacistPharmacy(List<PharmacistPharmacy> pharmacistPharmacy) {
		this.pharmacistPharmacy = pharmacistPharmacy;
	}
    
    

}

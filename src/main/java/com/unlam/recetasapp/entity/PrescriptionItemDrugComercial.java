
package com.unlam.recetasapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.unlam.recetasapp.model.DrugView;

@Entity
public class PrescriptionItemDrugComercial {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;
	
    @ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "drug_comercial_id", nullable = false)
    private DrugComercial drugComercial;
    
    @ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "prescription_item_id", nullable = false)
    private PrescriptionItem prescriptionItem;
    
    @JsonProperty("treatment")
    private String treatment;
    
    @JsonProperty("treatment_days")
    private Long treatmentDays;

    /**
     * No args constructor for use in serialization
     * 
     */
    public PrescriptionItemDrugComercial() {
    }

    /**
     * 
     * @param id
     * @param prescriptionItemId
     * @param treatment
     * @param drugComercialId
     */
    public PrescriptionItemDrugComercial(Long id, DrugComercial drugComercialId, PrescriptionItem prescriptionItemId, String treatment, Long treatmentDays) {
        super();
        this.id = id;
        this.drugComercial = drugComercialId;
        this.prescriptionItem = prescriptionItemId;
        this.treatment = treatment;
        this.treatmentDays = treatmentDays;
    }

    public PrescriptionItemDrugComercial(DrugView drug) {
		drugComercial = new DrugComercial(Long.parseLong(drug.getId().substring(1)));
		treatment = drug.getTreatment();
		treatmentDays = drug.getTreatmentDays();
	}

	@JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("drug_comercial_id")
    public DrugComercial getDrugComercial() {
        return drugComercial;
    }

    @JsonProperty("drug_comercial_id")
    public void setDrugComercial(DrugComercial drugComercialId) {
        this.drugComercial = drugComercialId;
    }

    @JsonProperty("prescription_item_id")
    public PrescriptionItem getPrescriptionItem() {
        return prescriptionItem;
    }

    @JsonProperty("prescription_item_id")
    public void setPrescriptionItem(PrescriptionItem prescriptionItemId) {
        this.prescriptionItem = prescriptionItemId;
    }

    @JsonProperty("treatment")
    public String getTreatment() {
        return treatment;
    }

    @JsonProperty("treatment")
    public void setTreatment(String treatment) {
        this.treatment = treatment;
    }
    

    public Long getTreatmentDays() {
		return treatmentDays;
	}

	public void setTreatmentDays(Long treatmentDays) {
		this.treatmentDays = treatmentDays;
	}

	@Override
	public String toString() {
		return "PrescriptionItemDrugComercial [id=" + id + ", drugComercial=" + drugComercial + ", prescriptionItem="
				+ prescriptionItem + ", treatment=" + treatment + ", treatmentDays=" + treatmentDays + "]";
	}



}

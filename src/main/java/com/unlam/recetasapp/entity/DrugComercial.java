
package com.unlam.recetasapp.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity

public class DrugComercial {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "quantity", nullable = false)
	private Integer quantity;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "presentation_id")
	private PharmaceuticalForm pharmaceuticalForm;

	@Column(name = "presentation_string")
	private String presentationString;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "laboratory_id", nullable = false)
	private Laboratory laboratory;

	@OneToMany(mappedBy = "drugComercial", fetch = FetchType.LAZY)
	private List<DrugComercialCompose> drugComercialCompose;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public DrugComercial() {
	}

	/**
	 * 
	 * @param id
	 * @param laboratoryId
	 * @param name
	 * @param quantity
	 * @param pharmaceuticalForm
	 */
	public DrugComercial(Long id, String name, Integer quantity, PharmaceuticalForm pharmaceuticalForm,
			Laboratory laboratoryId) {
		super();
		this.id = id;
		this.name = name;
		this.quantity = quantity;
		this.pharmaceuticalForm = pharmaceuticalForm;
		this.laboratory = laboratoryId;
	}

	public DrugComercial(Long id) {
		this.id = id;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("quantity")
	public Integer getQuantity() {
		return quantity;
	}

	@JsonProperty("quantity")
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}

	@JsonProperty("presentation_id")
	public PharmaceuticalForm getPharmaceuticalForm() {
		return pharmaceuticalForm;
	}

	@JsonProperty("presentation_id")
	public void setPharmaceuticalForm(PharmaceuticalForm pharmaceuticalForm) {
		this.pharmaceuticalForm = pharmaceuticalForm;
	}

	@JsonProperty("laboratory_id")
	public Laboratory getLaboratory() {
		return laboratory;
	}

	@JsonProperty("laboratory_id")
	public void setLaboratory(Laboratory laboratoryId) {
		this.laboratory = laboratoryId;
	}

	public List<DrugComercialCompose> getDrugComercialCompose() {
		return drugComercialCompose;
	}

	public void setDrugComercialCompose(List<DrugComercialCompose> drugComercialCompose) {
		this.drugComercialCompose = drugComercialCompose;
	}

	public String getPresentationString() {
		return presentationString;
	}

	public void setPresentationString(String presentationString) {
		this.presentationString = presentationString;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name).append("quantity", quantity)
				.append("presentation", pharmaceuticalForm).append("laboratory", laboratory).toString();
	}

}

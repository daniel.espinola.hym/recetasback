
package com.unlam.recetasapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class DoctorHospital {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;
    
	@ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "doctor_id", nullable = true)
	@JsonBackReference
    private Doctor doctor;
    
	@ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "hospital_id", nullable = true)
	@JsonBackReference
    private Hospital hospital;
    
	@Column(name = "doctor_license_number", nullable = false)
    private String doctorLicenseNumber;

    /**
     * No args constructor for use in serialization
     * 
     */
    public DoctorHospital() {
    }

    /**
     * 
     * @param id
     * @param doctorId
     * @param hospitalId
     * @param doctorLicenseNumber
     */
    public DoctorHospital(Long id, Doctor doctorId, Hospital hospitalId, String doctorLicenseNumber) {
        super();
        this.id = id;
        this.doctor = doctorId;
        this.hospital = hospitalId;
        this.doctorLicenseNumber = doctorLicenseNumber;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("doctor_id")
    public Doctor getDoctor() {
        return doctor;
    }

    @JsonProperty("doctor_id")
    public void setDoctor(Doctor doctorId) {
        this.doctor = doctorId;
    }

    @JsonProperty("hospital_id")
    public Hospital getHospital() {
        return hospital;
    }

    @JsonProperty("hospital_id")
    public void setHospital(Hospital hospitalId) {
        this.hospital = hospitalId;
    }

    @JsonProperty("doctor_license_number")
    public String getDoctorLicenseNumber() {
        return doctorLicenseNumber;
    }

    @JsonProperty("doctor_license_number")
    public void setDoctorLicenseNumber(String doctorLicenseNumber) {
        this.doctorLicenseNumber = doctorLicenseNumber;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("doctorId", doctor).append("hospitalId", hospital).append("doctorLicenseNumber", doctorLicenseNumber).toString();
    }

}

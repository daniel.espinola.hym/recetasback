
package com.unlam.recetasapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class Strength {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;
	
	@Column(name = "number", nullable = false)
    private Integer number;
    
	@ManyToOne(fetch = FetchType.EAGER )
	@JoinColumn(name = "unity_id", nullable = false)
    private Unity unityId;

    /**
     * No args constructor for use in serialization
     * 
     */
    public Strength() {
    }

    /**
     * 
     * @param unityId
     * @param id
     * @param number
     */
    public Strength(Long id, Integer number, Unity unityId) {
        super();
        this.id = id;
        this.number = number;
        this.unityId = unityId;
    }

    public Strength(Long strengthId) {
		id = strengthId;
	}

	@JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("number")
    public Integer getNumber() {
        return number;
    }

    @JsonProperty("number")
    public void setNumber(Integer number) {
        this.number = number;
    }

    @JsonProperty("unity_id")
    public Unity getUnityId() {
        return unityId;
    }

    @JsonProperty("unity_id")
    public void setUnityId(Unity unityId) {
        this.unityId = unityId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("number", number).append("unity", unityId).toString();
    }

}

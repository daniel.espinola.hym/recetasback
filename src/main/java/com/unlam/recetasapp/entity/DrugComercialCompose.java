
package com.unlam.recetasapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class DrugComercialCompose {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
    private Long id;
    
    @ManyToOne(fetch = FetchType.EAGER )
	@JoinColumn(name = "drug_id", nullable = false)
    private DrugGeneric drugGeneric;
    
    @ManyToOne(fetch = FetchType.LAZY )
	@JoinColumn(name = "drug_comercial_id", nullable = false)
    private DrugComercial drugComercial;

    /**
     * No args constructor for use in serialization
     * 
     */
    public DrugComercialCompose() {
    }

    /**
     * 
     * @param id
     * @param drugId
     * @param drugComercialId
     */
    public DrugComercialCompose(Long id, DrugGeneric drugId, DrugComercial drugComercialId) {
        super();
        this.id = id;
        this.drugGeneric = drugId;
        this.drugComercial = drugComercialId;
    }

    @JsonProperty("id")
    public Long getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(Long id) {
        this.id = id;
    }

    @JsonProperty("drug_id")
    public DrugGeneric getDrugGeneric() {
        return drugGeneric;
    }

    @JsonProperty("drug_id")
    public void setDrugGeneric(DrugGeneric drugId) {
        this.drugGeneric = drugId;
    }

    @JsonProperty("drug_comercial_id")
    public DrugComercial getDrugComercial() {
        return drugComercial;
    }

    @JsonProperty("drug_comercial_id")
    public void setDrugComercial(DrugComercial drugComercialId) {
        this.drugComercial = drugComercialId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("id", id).append("drugId", drugGeneric).append("drugComercialId", drugComercial).toString();
    }

}

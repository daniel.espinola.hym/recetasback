
package com.unlam.recetasapp.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
public class DrugGeneric {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "strength_string")
	private String strengthString;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "strength_id")
	private Strength strength;

	@OneToMany(mappedBy = "drugGeneric", fetch = FetchType.LAZY)
	private List<DrugComercialCompose> drugComercialCompose;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public DrugGeneric() {
	}

	/**
	 * 
	 * @param id
	 * @param name
	 * @param strengthId
	 */
	public DrugGeneric(Long id, String name, Strength strengthId) {
		super();
		this.id = id;
		this.name = name;
		this.strength = strengthId;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("name")
	public String getName() {
		return name;
	}

	@JsonProperty("name")
	public void setName(String name) {
		this.name = name;
	}

	@JsonProperty("strength_id")
	public Strength getStrength() {
		return strength;
	}

	@JsonProperty("strength_id")
	public void setStrength(Strength strengthId) {
		this.strength = strengthId;
	}

	public List<DrugComercialCompose> getDrugComercialCompose() {
		return drugComercialCompose;
	}

	public void setDrugComercialCompose(List<DrugComercialCompose> drugComercialCompose) {
		this.drugComercialCompose = drugComercialCompose;
	}

	public String getStrengthString() {
		return strengthString;
	}

	public void setStrengthString(String strengthString) {
		this.strengthString = strengthString;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name).append("strengthId", strength)
				.toString();
	}

}

package com.unlam.recetasapp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;
	
	@Column(name = "username", unique = true, nullable = false)
	private String username;
	
	@Column(name = "password", nullable = false)
	private String password;
	
	@Column(name = "role", nullable = false)
	private String role;
	
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "patient_id")
	private Patient patient;
	
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "doctor_id")
	private Doctor doctor;
	
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "pharmacist_id")
	private Pharmacist pharmacist;
    
    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "hospital_admin_id")
	private HospitalAdmin hospitalAdmin;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	public Doctor getDoctor() {
		return doctor;
	}

	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}

	public Pharmacist getPharmacist() {
		return pharmacist;
	}

	public void setPharmacist(Pharmacist pharmacist) {
		this.pharmacist = pharmacist;
	}

	public HospitalAdmin getHospitalAdmin() {
		return hospitalAdmin;
	}

	public void setHospitalAdmin(HospitalAdmin hospitalAdmin) {
		this.hospitalAdmin = hospitalAdmin;
	}
	
	
	
	
}

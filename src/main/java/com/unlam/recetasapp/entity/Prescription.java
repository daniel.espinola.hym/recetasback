package com.unlam.recetasapp.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Type;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.unlam.recetasapp.model.DrugView;
import com.unlam.recetasapp.model.PrescriptionView;

@Entity
public class Prescription {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "prescription_status_id", nullable = false)
	private PrescriptionStatus prescriptionStatus;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "medical_consultation_id")
	@JsonBackReference
	private MedicalConsultation medicalConsultation;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "chronic_assignment_id")
	@JsonBackReference
	private ChronicAssignment chronicAssignment;

	@OneToMany(mappedBy = "prescription", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<PrescriptionItem> prescriptionItem;

//    @ManyToOne(fetch = FetchType.LAZY )
//    @JoinColumn(name = "security_token_id", nullable = false)
	@Column(name = "security_token_id")
	private Long securityToken;

	@Column(name = "file_base64")
	@Type(type = "text")
	private String file;

	/**
	 * No args constructor for use in serialization
	 * 
	 */
	public Prescription() {
	}

	/**
	 * 
	 * @param id
	 * @param prescriptionStatusId
	 * @param medicalConsultationId
	 */
	public Prescription(Long id, PrescriptionStatus prescriptionStatusId, MedicalConsultation medicalConsultationId) {
		super();
		this.id = id;
		this.prescriptionStatus = prescriptionStatusId;
		this.medicalConsultation = medicalConsultationId;
	}

	public Prescription(PrescriptionView prescriptionView, MedicalConsultation medicalConsultation,
			List<PrescriptionItemDrugGeneric> prescriptionItemDrugGenericList) {

		List<DrugView> drugList = prescriptionView.getDrugList();
		List<Long> diagnosisList = prescriptionView.getDiagnosisList();
		PrescriptionItem pItem = new PrescriptionItem();
		List<PrescriptionItemDrugComercial> pItemDrugComercialList = new ArrayList<PrescriptionItemDrugComercial>();
		List<PrescriptionItemDiagnosis> pItemDiagnosisList = new ArrayList<PrescriptionItemDiagnosis>();

		for (Long diagnosisId : diagnosisList) {
			PrescriptionItemDiagnosis pItemDiagnosis = new PrescriptionItemDiagnosis(diagnosisId);
			pItemDiagnosis.setPrescriptionItem(pItem);
			pItemDiagnosisList.add(pItemDiagnosis);
		}

		for (PrescriptionItemDrugGeneric pIDGeneric : prescriptionItemDrugGenericList) {
			pIDGeneric.setPrescriptionItem(pItem);
		}

		for (DrugView drug : drugList) {
			if (drug.getId().startsWith("G")) {
				// Viene como parametro ya que requiere procesamiento y llamadas a repositorio
			} else if (drug.getId().startsWith("C")) {
				PrescriptionItemDrugComercial pItemDrugComercial = new PrescriptionItemDrugComercial(drug);
				pItemDrugComercial.setPrescriptionItem(pItem);
				pItemDrugComercialList.add(pItemDrugComercial);
			}
		}

		pItem.setPrescriptionItemDiagnosis(pItemDiagnosisList);
		pItem.setPrescriptionItemDrugComercial(pItemDrugComercialList);
		pItem.setPrescriptionItemDrugGeneric(prescriptionItemDrugGenericList);
		pItem.setPrescription(this);

		prescriptionItem = new ArrayList<PrescriptionItem>();
		prescriptionItem.add(pItem);

		this.medicalConsultation = medicalConsultation;

	}

	public Prescription(Long prescriptionId) {
		id = prescriptionId;
	}

	@JsonProperty("id")
	public Long getId() {
		return id;
	}

	@JsonProperty("id")
	public void setId(Long id) {
		this.id = id;
	}

	@JsonProperty("prescription_status_id")
	public PrescriptionStatus getPrescriptionStatus() {
		return prescriptionStatus;
	}

	@JsonProperty("prescription_status_id")
	public void setPrescriptionStatus(PrescriptionStatus prescriptionStatusId) {
		this.prescriptionStatus = prescriptionStatusId;
	}

	@JsonProperty("medical_consultation_id")
	public MedicalConsultation getMedicalConsultation() {
		return medicalConsultation;
	}

	@JsonProperty("medical_consultation_id")
	public void setMedicalConsultation(MedicalConsultation medicalConsultationId) {
		this.medicalConsultation = medicalConsultationId;
	}

	public List<PrescriptionItem> getPrescriptionItem() {
		return prescriptionItem;
	}

	public void setPrescriptionItem(List<PrescriptionItem> prescriptionItem) {
		this.prescriptionItem = prescriptionItem;
	}

	public Long getSecurityToken() {
		return securityToken;
	}

	public void setSecurityToken(Long securityToken) {
		this.securityToken = securityToken;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public ChronicAssignment getChronicAssignment() {
		return chronicAssignment;
	}

	public void setChronicAssignment(ChronicAssignment chronicAssignment) {
		this.chronicAssignment = chronicAssignment;
	}

}

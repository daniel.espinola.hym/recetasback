package com.unlam.recetasapp.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.unlam.recetasapp.service.ChronicService;

@Component
public class SchedulerTask {
	private static final Logger logger = LoggerFactory.getLogger(SchedulerTask.class);
    private static final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("HH:mm:ss");

    @Autowired
	private ChronicService chronicService;
    
    @Scheduled(fixedRate = 60000)
    @Transactional
    public void scheduleTaskWithFixedRate() throws Exception {
    	logger.info("Fixed Rate Task :: Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()) );
    	chronicService.testClock();
    	logger.info("Fixed Rate Task :: Ended Execution Time - {}", dateTimeFormatter.format(LocalDateTime.now()) );
    }

    public void scheduleTaskWithFixedDelay() {
    }

    public void scheduleTaskWithInitialDelay() {}

    public void scheduleTaskWithCronExpression() {}
}

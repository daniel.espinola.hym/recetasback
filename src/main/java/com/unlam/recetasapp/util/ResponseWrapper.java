package com.unlam.recetasapp.util;

public class ResponseWrapper {
	private int status;
	private String error;
	private Object response;

	public ResponseWrapper() {
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Object getResponse() {
		return response;
	}

	public void setResponse(Object response) {
		this.response = response;
	}

}

package com.unlam.recetasapp.util;

import java.util.HashMap;

public class AlphabetMap {
	public static HashMap<Integer, String> alphabetMap = new HashMap<Integer, String>();
	
	static {
	    alphabetMap.put(1, "A");
	    alphabetMap.put(2, "B");
	    alphabetMap.put(3, "C");
	    alphabetMap.put(4, "D");
	    alphabetMap.put(5, "E");
	    alphabetMap.put(6, "F");
	    alphabetMap.put(7, "G");
	    alphabetMap.put(8, "H");
	    alphabetMap.put(9, "I");
	}
}
